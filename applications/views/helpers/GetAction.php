<?php

class Zend_View_Helper_GetAction extends Zend_View_Helper_Abstract
{
    function getAction()
    {
		$action = Zend_Controller_Front::getInstance()->getRequest()->getActionName();
		if (!empty($action)) {
			return $action;
		}
		return false;
    }	
}