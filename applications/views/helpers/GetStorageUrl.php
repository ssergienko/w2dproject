<?php

class Zend_View_Helper_GetStorageUrl
{
    function getStorageUrl()
    {
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;

		return $config->storage->url;
    }
}

?>
