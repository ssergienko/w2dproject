<?php

class Zend_View_Helper_GetTravelAuthorId
{
    function getTravelAuthorId($travel_id)
    {
		$travel_table = new Application_Model_DbTable_Travels();
		$travel = $travel_table->getById($travel_id);

		if (!empty($travel['author'])) {
			return $travel['author'];
		}

		return false;
    }
}

?>
