<?php

class Zend_View_Helper_GetSiteDescription extends Zend_View_Helper_Abstract
{
    function getSiteDescription()
    {
		$config = Zend_Registry::getInstance()->config;
		return $config->siteDescription;
    }
}

?>
