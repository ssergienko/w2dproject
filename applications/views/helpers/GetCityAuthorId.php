<?php

class Zend_View_Helper_GetCityAuthorId
{
    function getCityAuthorId($city_id)
    {
		$city_table = new Application_Model_DbTable_City();
		$city = $city_table->getById($city_id);

		if (!empty($city['author'])) {
			return $city['author'];
		}

		return false;
    }
}

?>
