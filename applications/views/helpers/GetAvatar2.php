<?php

class Zend_View_Helper_GetAvatar2 extends Zend_View_Helper_Abstract
{
    function getAvatar2($user_id)
    {
		  $user_table = new Api_Model_DbTable_Users();
      $user = $user_table->getById($user_id);
      if (!empty($user['user_image'])) {
        return $user['user_image'];
      }
      return '/images/default_avatar.png';
    }
}
