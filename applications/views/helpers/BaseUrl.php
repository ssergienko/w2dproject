<?php

class Zend_View_Helper_BaseUrl extends Zend_View_Helper_Abstract
{
    function baseUrl($path = false)
    {
		if(!empty($path)){
			switch($path){
				case 'comments':
					$split = preg_split("/\?/", $_SERVER['REQUEST_URI']);
					$url = $split[0];
					$url = "http://".$_SERVER['HTTP_HOST'].$url;
					break;
				case 'likes':
					if(preg_match('/^(\/[^\/]*\/[\d]*)/i', $_SERVER['REQUEST_URI'], $match)){
						$url = $match[1];
					}
					else{
						$url = $_SERVER['REQUEST_URI'];
					}

					//$url = "http://".$_SERVER['HTTP_HOST'].$url;					
					$url = "http://way2day.ru".$url;					
					break;
				default:
					$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
					break;
			}
			
		}
		else{
			$url = "http://".$_SERVER['HTTP_HOST'];
		}		
        return $url;
    }
}