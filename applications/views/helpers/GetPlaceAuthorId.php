<?php

class Zend_View_Helper_GetPlaceAuthorId
{
    function getPlaceAuthorId($place_id)
    {
		$place_table = new Application_Model_DbTable_Places();
		$place = $place_table->getById($place_id);

		if (!empty($place['author'])) {
			return $place['author'];
		}

		return false;
    }
}

?>
