<?php

class Zend_View_Helper_GetCityAuthorName
{
    function getCityAuthorName($city_id)
    {
		$city_table = new Application_Model_DbTable_City();
		$city = $city_table->getById($city_id);

		if (!empty($city['author'])) {

			$user_table = new Api_Model_DbTable_Users();
			$user = $user_table->getById($city['author']);

			if (!empty($user['uname'])) {
				return $user['uname'];
			}

			return 'Безымянный товарищ';

		}

		return 'Безымянный товарищ';
    }
}

?>
