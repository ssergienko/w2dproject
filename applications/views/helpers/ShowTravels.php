<?php

/*
 * Проверяет флаг в конфиге
 * возвращает тру или фэлс
 */
class Zend_View_Helper_ShowTravels
{
    function showTravels()
    {
        // Получаем конфиг
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        if (!empty($config->travels->show)) {
            return true;
        }
        return false;
    }
}

?>
