<?php

class Zend_View_Helper_GetCityAuthorPhoto
{
    function getCityAuthorPhoto($city_id)
    {
		$city_table = new Application_Model_DbTable_City();
		$city = $city_table->getById($city_id);

		if (!empty($city['author'])) {

			$user_table = new Api_Model_DbTable_Users();
			$user = $user_table->getById($city['author']);

			if (!empty($user['user_image'])) {
				return $user['user_image'];
			}

			return '/images/default_avatar.png';

		}

		return '/images/default_avatar.png';
    }
}

?>
