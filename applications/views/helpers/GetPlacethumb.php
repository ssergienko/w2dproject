<?php

class Zend_View_Helper_GetPlacethumb
{

    function getPlacethumb($place_id, $main_image_url)
    {
		$plim_table = new Api_Model_DbTable_Placeimages();
		$img_url = $plim_table->getThumbnail($place_id, $main_image_url);

		if (!empty($img_url)) {
			return $img_url;
		}

		return false;
    }
}

?>
