<?php

class Zend_View_Helper_GetPath extends Zend_View_Helper_Abstract
{
    function getPath($menu = false)
    {
		$params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		
		$array = array('controller','action');
		
		if(!empty($menu)){
			if(!empty($params[$params['controller']])) 
				$array[] = $params['controller'];
			elseif(!empty($params[substr($params['controller'],0 , strlen($params['controller'])-1)])) 
				$array[] = substr($params['controller'],0 , strlen($params['controller'])-1);
		}
		$path = "";
		$del = "?";
		foreach($params as $key=>$value){
			if(!in_array($key, $array)){
				$path .= $del.$key."=".$value;
				$del = "&";
			}
		}
		return $path;		
    }	
}