<?php

class Zend_View_Helper_BreadCrumbs extends Zend_View_Helper_Abstract
{
	protected $user;
	
    function breadCrumbs($separator = '/')
    {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->user = $auth->check();	
		
		// Получаем параметры
        $params = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		
		//var_dump($params);
		
		$menu_items = array();
		// Кладем в массив ссылок первый элемент
		$menu_items[] = array('title' => 'Куда поехать на выходные?');
		
		// Формируем массив возможных видов ссылок
		$menu_item_types = array('user', 'city', 'travel', 'place', 'hotel', 'room');
		
		foreach ($menu_item_types as $row){
			
			if(!empty($params[$row]) && is_numeric($params[$row])){
				$result_title = 'title';
				switch($row){
					// Если в строке был передан параметр USER
					case 'user': 
						$table = new Application_Model_DbTable_Users(); 
						$result = $table->getById((int)$params[$row]);
						
						$name = "Пользователь";
						$result_title = 'uname';
						break;
					
					// Если в строке был передан параметр CITY
					case 'city': 
						$table = new Application_Model_DbTable_City(); 
						$result = $table->getById((int)$params[$row]);
						
						$name = "Город";
						break;
					
					// Если в строке был передан параметр TRAVEL
					case 'travel': 
						$table = new Application_Model_DbTable_Travels();
						$result = $table->getById((int)$params[$row]);
						if(!empty($params['user']) && $params['user'] == $this->user['id']) {
							$result = $table->getByIdFromUser((int)$params[$row]);
						} else {
							$result = $table->getById((int)$params[$row]);
						}
						
						$name = "Поездка"; 
						break;
						
					// Если в строке был передан параметр PLACE
					case 'place': 
						$table = new Application_Model_DbTable_Places();
						if(!empty($params['user']) && $params['user'] == $this->user['id']) {
							$result = $table->getByIdFromUser((int)$params[$row]);
						} else {
							$result = $table->getById((int)$params[$row]);
						}
						
						$name = "Место";
						break;
						
					// Если в строке был передан параметр HOTEL
					case 'hotel': 
						$table = new Application_Model_DbTable_Hotels();
						$result = $table->getById((int)$params[$row]);
						
						$name = "Отель"; 
						break;
					
					// Если в строке был передан параметр ROOM
					case 'room': 
						$table = new Application_Model_DbTable_Hotelprice();
						$result = $table->getById((int)$params[$row]);
						
						$name = "Номер";
						break;
				}
				
				// Если удалось получить данные по ресурсу
				if(!empty($result)){
					// Выбираем заголовок, а если нет, устанавливаем значение по умолчанию
					$title = !empty($result[$result_title])?$result[$result_title]:$name;
					
					// Выбираем дополнительные параметры из ссылки
					$path_params = "";
					$del_params = "?";
					foreach ($menu_item_types as $type) {
						if($row == $type) {
							break;
						}
						
						if(!empty($params[$type])) {
							$path_params .= $del_params.$type."=".$params[$type];
							$del_params = "&";
						}
					}
										
					// Формируем массив данных для генерации ссылок
					$path = array('title' => $title, 'type' => $row,'value' => $params[$row], 'params' => $path_params);
					$menu_items[$row] = $path;
				}
			}
		}
		
		$add_params = array();
		$return = "";
		// Пробегаем массив данных и генерируем ссылки
		foreach($menu_items as $type=>$path){
			
			$link = "/";
			// Устанавливаем тип ресурса
			$link .= !empty($path['type'])?$path['type'].$separator:"";
			// Устанавливаем ID ресурса
			$link .= !empty($path['value'])?$path['value'].$separator:"";
			if($link != '/') {
				// Если в строке был передан USER и PLACE, дополняем ссылку
				if($type == 'user' && !empty($menu_items['place'])) {
					$link .= "places/";
				}
				// Если в строке был передан USER и TRAVEL, дополняем ссылку
				if($type == 'user' && !empty($menu_items['travel'])) {
					$link .= "travels/";
				}
			}
			
			if(!empty($path['params'])) {
				$link .= $path['params'];
			}
			
			// Если последний ресурс не должен быть жирным
			if($path == end($menu_items)){				
				$return .= "<li><strong><a href='$link'>".$path['title']."</a></strong></li>";
			}
			else{
				$return .= "<li><a href='$link'>".$path['title']."</a></li>";
			}
			// Если последний ресурс должен быть ссылкой
			//$return .= "<li><a href='$link'>".$path['title']."</a></li>";
			
		}	
		$return = "<ul class='breadcrumb'>$return</ul>";
		
		return $return;
    }	
}