<?php

class Zend_View_Helper_GetPlaceAuthorPhoto
{
    function getPlaceAuthorPhoto($place_id)
    {
		$place_table = new Application_Model_DbTable_Places();
		$place = $place_table->getById($place_id);

		if (!empty($place['author'])) {

			$user_table = new Api_Model_DbTable_Users();
			$user = $user_table->getById($place['author']);

			if (!empty($user['user_image'])) {
				return $user['user_image'];
			}

			return '/images/default_avatar.png';

		}

		return '/images/default_avatar.png';
    }
}

?>
