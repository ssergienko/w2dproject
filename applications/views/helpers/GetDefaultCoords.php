<?php

class Zend_View_Helper_GetDefaultCoords
{

	/*
	* Возвращает координаты по умолчанию
	 *
	 * rerun array (lng, lat)
	 *
	*/
    function getDefaultCoords()
    {
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;
		return array($config->coord->lat, $config->coord->lng);
    }
}

?>
