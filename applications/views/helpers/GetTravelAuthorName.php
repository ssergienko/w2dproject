<?php

class Zend_View_Helper_GetTravelAuthorName
{
    function getTravelAuthorName($travel_id)
    {
		$travel_table = new Application_Model_DbTable_Travels();
		$travel = $travel_table->getById($travel_id);

		if (!empty($travel['author'])) {

			$user_table = new Api_Model_DbTable_Users();
			$user = $user_table->getById($travel['author']);

			if (!empty($user['uname'])) {
				return $user['uname'];
			}

			return 'Безымянный товарищ';

		}

		return 'Безымянный товарищ';
    }
}

?>
