<?php


/*
 * Обрезает текст до указанного количества символов и подставляет в конец троеточие
 *
 * @params $text - текст для обрезания
 * @params $count - количество символов
 * если второй параметр не пришел, берем значение из конфига
 *
 * @return cut($text)...
 *
 */
class Zend_View_Helper_CutLongText
{
    function cutLongText($text, $count = false)
    {
		$config = Zend_Registry::getInstance()->config;

		if (empty($count)) {
			$count = $config->count_text_on_list_block;
		}

		if (strlen($text) > $count) {
			$text = mb_substr($text, 0, $count-3, 'utf8').'...';
		}

        return $text;
    }
}

?>
