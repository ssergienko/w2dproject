<?php

class Zend_View_Helper_GetController extends Zend_View_Helper_Abstract
{
    function getController()
    {
		$controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		if (!empty($controller)) {
			return $controller;
		}
		return false;
    }	
}