<?php

/*
 * Получает значение из конфига по ключу
 * 
 * @params $key - должен приходить в виде строки string->string->string
 */
class Zend_View_Helper_GetConfig extends Zend_View_Helper_Abstract
{
    function getConfig($key)
    {
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;
		
		if (!empty($config->$key)) {
			return $config->$key;
		}
		return '';
    }
}