<?php

class Zend_View_Helper_GetTravelAuthorPhoto
{
    function getTravelAuthorPhoto($travel_id)
    {
		$travel_table = new Application_Model_DbTable_Travels();
		$travel = $travel_table->getById($travel_id);

		if (!empty($travel['author'])) {

			$user_table = new Api_Model_DbTable_Users();
			$user = $user_table->getById($travel['author']);

			if (!empty($user['user_image'])) {
				return $user['user_image'];
			}

			return '/images/default_avatar.png';

		}

		return '/images/default_avatar.png';
    }
}

?>
