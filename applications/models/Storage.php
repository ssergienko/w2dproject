<?php

class Storage {
	/*
	 * Получаем контент страницы
	 * @params url (varchar) Адрес сайта
	 * @params post (array) Массив передаваемых данных
	 * @return header (array) Массив получаемых данных
	 */

	public function post_content($url, $post = array()) {

		$uagent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:10.0) Gecko/20100101 Firefox/10.0";

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
		curl_setopt($ch, CURLOPT_HEADER, 0);		   // не возвращает заголовки
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);   // переходит по редиректам
		curl_setopt($ch, CURLOPT_ENCODING, "");		// обрабатывает все кодировки
		curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // таймаут соединения
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);		// таймаут ответа
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);	   // останавливаться после 10-ого редиректа
		if (!empty($post)) {		// отправляем post данные
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		curl_setopt($ch, CURLOPT_COOKIEJAR, "z://coo.txt");
		curl_setopt($ch, CURLOPT_COOKIEFILE, "z://coo.txt");

		$content = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);

		curl_close($ch);

		$header['errno'] = $err;
		$header['errmsg'] = $errmsg;
		$header['content'] = $content;
		return $header;
	}
	
	/*
	 * Метод для изменения размеров изображения
	 * @params file (array) Массив данных о файле
	 * @params options (array) Массив параметров для редактирования
	 * @return filePath (varchar) Ссылка на файл
	 */
	public function imageresize($file, $options = array()){
		// Получаем тип файла
		preg_match_all('/\.([^\.]*)$/', $file['name'], $match);		
		$type = $match[1][0];
		
		// Получаем размеры
		$x = isset($options['x']) && (int)$options['x'] > 1 ? (int)$options['x'] : 400;
		$y = isset($options['y']) && (int)$options['y'] > 1 ? (int)$options['y'] : 300;
		$path = APPLICATION_PATH ."\../public/images/upload/";
		
		// Генерируем случайное имя
		$name = uniqid();
		$file_path = $path.$name.".".$type;
		
		// Помещаем изображение во временное хранилище
		move_uploaded_file($file['tmp_name'], $file_path);
		
		// Получаем данные о изображении
		$info = @getimagesize($file_path);
		$realY = $info[0]; // Реальная ширина
		$realX = $info[1]; // Реальная высота
		
		$mime = $info['mime']; // Тип файла
		
		// Создаем ресурс для изображения
		switch($mime){
			case 'image/png':
				$source = imagecreatefrompng($file_path);
				break;
			case 'image/gif':
				$source = imagecreatefromgif($file_path);
				break;
			case 'image/jpeg':
				$source = imagecreatefromjpeg($file_path);
				break;
			default:
				break;
		}
		// Если удалось загружен файл допустимого расширения
		if(!empty($source)){
			// Определяем максимальное соотношение сжития по высоте и ширине
			$ratioX = (float) ($realX / $x);
			$ratioY = (float) ($realY / $y);
			$ratio = max($ratioX, $ratioY);

			$newX = ceil($realX / $ratio); // новая высота
			$newY = ceil($realY / $ratio); // новая ширина
			
			// Создаем маску для ресурса
			if(!($mask = imagecreatetruecolor($newY, $newX))){
				return;// 'Cannot create a mask';
			}
			// Изменяем размеры изображения
			if (!imagecopyresampled($mask, $source, 0, 0, 0, 0, $newY, $newX, $realY, $realX)) {
				return;// 'Connot resize image';
			}
			//if (!imagejpeg($mask, $file_path, 100)) { // Создаем фото
			if (!imagepng($mask, $file_path)) { // Создаем фото
				return;// 'Connot create a image';
			}
			// Очищаем кэш
			imagedestroy($mask);
			imagedestroy($source);
			
			return $file_path;
		}
		return;// 'Error format file';		
	}
}

?>
