<?php

/**
 * Класс для отправки почты, подготовке сообщений и всем остальным связанным с электронной почтой
 *
 * @author Ssergienko
 */
class Application_Model_Email {



    private function send ($data) {

        $mail = new Zend_Mail('UTF-8');
        $mail->setSubject($data['email_title']);
        $mail->setBodyHtml($data['email_body']);
        $mail->setFrom($data['from']);
        $mail->addTo($data['to']);
        $send = $mail->send();

        if($send){
            return true;
        }
        return false;

    }



/*
	 * Письмо с паролем
	 */
	public function sendPassword ($user) {
		
		if (!empty($user)) {

			// Достаем шаблон
			$html_view = new Zend_View();
			$html_view->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
			
			// Подставляем данные в шаблон			
			$html_view->assign('pass', $user->pass);
			$html_view->assign('user_name', $user->uname);
			
			// Рендерим шаблон
			$html = $html_view->render('rempasswd.phtml');
			
			// Подготавливаем отправку
			$data = array(
				'email_title' => 'Напоминание пароля для Way2Day.ru',
				'email_body' => $html,
				'from' => 'hello@way2day.ru',
				'to' => $user->uemail
			);
			// Отправляем
			$this->send($data);
			
		}
		
	}
	
	
	
	public function sendRegConfirmMessage ($user_id) {
		
		if (!empty($user_id)) {
			
			$config = Zend_Registry::getInstance()->config;
			
			$user_obj = new Application_Model_DbTable_Users();
			$user = $user_obj->getById($user_id);

			// Достаем шаблон
			$html_view = new Zend_View();
			$html_view->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
			
			$str = '{"user":"'.$user['id'].'", "redirect_uri":"'.$user['redirect_uri'].'", "email":"'.$user['uemail'].'"}';
			
			// Подставляем данные в шаблон
			$html_view->assign('url', base64_encode($str));
			$html_view->assign('user_name', $user['uname']);
			$html_view->assign('base_url', $config->url->base);
			
			// Рендерим шаблон
			$html = $html_view->render('confirmregmail.phtml');
			
			// Подготавливаем отправку
			$data = array(
				'email_title' => 'Подтверждение регистрации на Way2Day.ru',
				'email_body' => $html,						
				'from' => 'hello@way2day.ru',
				'to' => $user['uemail']
			);
			// Отправляем
			$this->send($data);
			
		}
		
	}
	
	
	
	/*
	 * Письмо о том, что подтверждение регистрации прошло успешно
	 */
	public function sendSaccessConfirmMessage ($user_id) {
		
		if (!empty($user_id)) {
			
			$user_obj = new Application_Model_DbTable_Users();
			$user = $user_obj->getById($user_id);

			// Достаем шаблон
			$html_view = new Zend_View();
			$html_view->setScriptPath(APPLICATION_PATH . '/views/scripts/emails/');
			
			// Подставляем данные в шаблон
			$html_view->assign('pass', $user['pass']);
			$html_view->assign('user_name', $user['uname']);
			
			// Рендерим шаблон
			$html = $html_view->render('successconfirmregmail.phtml');
			
			// Подготавливаем отправку
			$data = array(
				'email_title' => 'Подтверждение регистрации на Way2Day.ru',
				'email_body' => $html,						
				'from' => 'hello@way2day.ru',
				'to' => $user['uemail']
			);
			// Отправляем
			$this->send($data);
			
		}
		
	}
	
	
	
	
	/*
	 * Формирует заголовки письма
	 */
	public function getEmailHeaders ($from, $type) {				
		if (!empty($from)) {						
			$headers = 'From: '.$from;
			$headers .= " MIME-Version: 1.0\r\n"; 
			$headers .= "Content-type: text/".$type."; charset=utf-8\r\n";
			$headers .="Content-Transfer-Encoding: 8bit";
			
			return $headers;
		}		
		return false;		
	}

	
	
	/*
	 * Подготавливает сообщение
	 * @params page(string) - название страницы откуда вызван метод, взависимоти от этого параметра выбирается сообщение которое посылать
	 * @params options - переменные нужные в тексте письма
	 */
	public function getMessage ($page, $options = false) {
		
		if (!empty($page)) {
			
			switch ($page) {
				// Отель подал заявку на участие (письмо менеджеру)
				case 'addhotel_adm':		
					$message = 'Организация - '.$options['orgname'].", \r\n".
					'в лице представителя с именем '.$options['name'].", \r\n".
					'с телефоном '.$options['phone']."\r\n".
					'и мыльным адресом '.$options['email']. ' хочет учавствовать в проекте way2day.';					
					break;
				
				// Отель подал заявку на участие
				case 'addhotel':		
					$message = '
					<html>
					<body leftmargin="0" topmargin="0" marginheight="0" offset="0" style="font-family:Myriad Pro">
						<table width="100%" border="0">
							<tr>
								<td width="200px"><img src="http://storage.way2day.ru/files/2012-02/2012-02-27/4f4b95701ab22.png" /></td>
								<td><p style="font-size: 1.5em;margin-top:10px;">Здравствуйте, '.$options['name'].'!</p><br /></td>
							</tr>
							<tr>
								<td></td>
								<td><p>Вы подали заявку на участие организации '.$options['orgname'].' в проекте "Куда поехать на выходные - <a href="http://way2day.ru/">way2day.ru</a>".
									Мы рады знакомству с вами. Надеемся на интересное и продуктивное сотрудничество.</p></td></tr><tr><td></td><td><br /><br />
									<p style="font-style:italic;">С уважением, команда разработчиков <a href="http://way2day.ru/">way2day.ru</a><br />
									<br />
								</td>
							</tr>
						</table>
					</body>
					</html>';
					break;
				
			}
			
			return $message;
			
		}
		
		return false;
		
	}
	
}

?>
