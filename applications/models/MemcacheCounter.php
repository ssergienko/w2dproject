<?php
/**
 * Класс увеличивает и уменьшает значение поля рейтинг в memcache
 *
 * @params (string) key
 * 
 * @author ssergienko
 */
class Application_Model_MemcacheCounter {
	
	
	public function increment($key) {
		
		// Подключаем кэширование memcache
		$oCache = Zend_Registry::getInstance()->oCache;
		
		// initialize lock  
		$lock = FALSE;  
  
		// initialize configurable parameters  
		$tries = 0;
		$max_tries = 100;
		$lock_ttl = 10;
  
		$new_count = $oCache->load($key); // fetch older value  
		
		while($lock === FALSE && $tries < $max_tries) {
			if($new_count === FALSE) { $new_count = 0; }
			$new_count = $new_count + 1;
			
			$lock = $oCache->save($key, "lock_".$key);
  
			$tries++;
			usleep(100*($tries%($max_tries/10))); // exponential backoff style of sleep  
		}  
		
		if($lock === FALSE && $tries >= $max_tries) {  
			error_log("Unable to increment key ".$key);  
			return FALSE;  
		}
		else {
			$oCache->save($new_count, $key);  
			error_log("Incremented key ".$key." to ".$new_count);  
			return TRUE;  
		}
  
	}
	
	
	public function decrement($key) {
		
		// Подключаем кэширование memcache
		$oCache = Zend_Registry::getInstance()->oCache;
		
		// initialize lock  
		$lock = FALSE;  
  
		// initialize configurable parameters  
		$tries = 0;
		$max_tries = 100;
		$lock_ttl = 10;
  
		$new_count = $oCache->load($key); // fetch older value  
		
		while($lock === FALSE && $tries < $max_tries) {
			if($new_count === FALSE) { $new_count = 0; }
			$new_count = $new_count - 1;
			
			$lock = $oCache->save($key, "lock_".$key);
			
			$tries++;
			usleep(100*($tries%($max_tries/10))); // exponential backoff style of sleep  
		}  
		
		if($lock === FALSE && $tries >= $max_tries) {  
			error_log("Unable to dencrement key ".$key);  
			return FALSE;  
		}
		else {
			$oCache->save($new_count, $key);  
			error_log("Decremented key ".$key." to ".$new_count);  
			return TRUE;  
		}
  
	}
	
	/*
	 * Метод для динамического увеличения рейтинга
	 * @params type (varchar) тип кэшируемого элемента
	 * @params id (integer) ID кэшируемого элемента
	 */
	public function calcRating($type, $id, $method = 'increment'){
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		if(!empty($type) && !empty($id)){
			
			
			switch($type){
				case'place':
					$places_table = new Application_Model_DbTable_Places();
					$result = $places_table->getById($id);
					if(!empty($result)){
						
						// Увеличиваем рейтинг города, к которому привязано место
						if(!empty($result['city'])){
							if($method == 'increment') {
								$this->increment("city_rating_".$result['city']);
							} elseif($method == 'decrement') {
								$this->decrement("city_rating_".$result['city']);
							}
						}
						// Увеличиваем рейтинг места
						if($method == 'increment') {
							$this->increment("place_rating_".$id);
						} elseif($method == 'decrement') {
							$this->decrement("place_rating_".$id);
						}
						
						/*// Подключаем мемкэш
						$memcache = Zend_Registry::getInstance()->oCache;
						$memcache->delete("city_".$result['city']);
						if(!empty($user)) {
							$memcache->delete("user_".$user['id']);
						}*/
						
					}
					break;
				case'travel':
					// Увеличиваем рейтинг поездки (пока)
					if($method == 'increment') {
						$this->increment("travel_rating_".$id);
					} elseif($method == 'decrement') {
						$this->decrement("travel_rating_".$id);
					}	
					
					break;
			}
			return true;
		}
		return false;
	}
	
	
}

?>
