<?php

/*
 * Класс для поиска
 * 
 * @author sergey.s.sergienko
 * 
 */
class Application_Model_Search {
	
	
	/*
	 * Выполняет поиск
	 */
	public function doSearch ($keyphrase, $find_type) {
		
		$return = array();
		$item_types = array('city', 'place', 'travel', 'hotel');
		
		foreach ($item_types as $item){
			switch($item){
				case 'city':
					$table = new Application_Model_DbTable_City();
					break;
				case 'place':
					$table = new Application_Model_DbTable_Places();
					break;
				case 'travel':
					$table = new Application_Model_DbTable_Travels();
					break;
				case 'hotel':
					$table = new Application_Model_DbTable_Hotels();
					break;
				default: $table = ""; continue;
			}
			if(!empty($table)) {
				if ($find_type == 'filter') {
					$result = $table->searchBySimbol($keyphrase);
				} elseif ($find_type == 'keyphrase') {
					$result = $table->searchByPhrase($keyphrase);
				}
			}
			
			if(!empty($result)){
				$return[$item] = $result;
			}			
		}		
		if(!empty($return)){
			return $return;
		}
		
		return false;
	}
	
}