<?php
/*
 * Auth methods
 * 
 * AVC author
 */

class Application_Model_Auth {
	
    public function check(){
		
		$localhost = array(
			'way2day.local',
			'way2day.dm',
			'local',			
		);
		$site = $_SERVER['SERVER_NAME'];
		
		$users = new Application_Model_DbTable_Users();
        $auth = new Zend_Session_Namespace('auth');
		
		if(in_array($site, $localhost)){
			$uid = 49;
		} else {
			$uid = $auth->uid;
		}

        if(!empty($uid)){
            return $users->getProfile($uid);
        }
        if (!empty($_COOKIE['w2duid'])) {
            return $users->getProfile($_COOKIE['w2duid']);
        }
		return;
	}	
	
}
?>
