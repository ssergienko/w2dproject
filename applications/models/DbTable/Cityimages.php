<?php

class Application_Model_DbTable_Cityimages extends Zend_Db_Table_Abstract {

	protected $_name = 'city_images';

	
	
	/*
	 * Получение больших картинок
	 */
	public function getLargeImages($city_id){
		
		if(!empty($city_id)){
			$select = $this->select()->where('cy_id = '.$city_id);
			$result = $this->fetchAll($select);
			if(!empty($result)){
				$images = array();
				foreach ($result->toArray() as $image) {
					$images[] = array('url' => $image['url']/*, 'image_txt' => $image['image_txt']*/);
				}
				return $images;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Добавление места
	 */
	public function add($data) {
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)) {
				return $result;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получение картинок места
	 */
	public function getImagesByCyId($city_id){
		
		if(!empty($city_id)){
			$select = $this->select()->where('cy_id = '.$city_id);
			$result = $this->fetchAll($select);
			if(!empty($result)){				
				return $result->toArray();
			}
		}
		return false;
	}
	
	
	
	
}
