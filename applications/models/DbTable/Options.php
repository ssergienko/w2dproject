<?php

class Application_Model_DbTable_Options extends Zend_Db_Table_Abstract
{
    protected $_name = 'options';
    
	/*
	 * Получаем список пользователей
	 */
	public function getByUserId($user_id){
		
		if(!empty($user_id)) {
			$select = $this->select()->where('user_id = ?',$user_id);
			$result = $this->fetchRow($select);
			if(!empty($result)) {
				return $result->toArray();
			}
			
		}
		return false;		
	}
	
	
	/*
	 * Обновляем настройки пользователя
	 */
	public function edit($user_id, $data) {
		if(!empty($user_id) && !empty($data)) {
			
			$select = $this->select()->where("user_id = ?",	$user_id);
			$result_select = $this->fetchRow($select);
			
			if(!empty($result_select)) {
				$result = $this->update($data, "user_id = $user_id");
			} else {
				$data['user_id'] = $user_id;
				$result = $this->insert($data);
			}			
			
			if(!empty($result)) {
				return true;
			}
		}
		return false;
	}
	
}
