<?php

class Application_Model_DbTable_Travelplace extends Zend_Db_Table_Abstract {

	protected $_name = 'travelplace';
	
	
	
	/*
	 * Получает места по поездке
	 * @param travel_ids (array) Массив айдишников поездок
	 */
	public function getPlacesByTravel($travel_id) {
		
		// Вытаскиваем идентификаторы из travelplace
		$sql = $this->select()->where('t_id = '.$travel_id);
		$places_ids = $this->fetchAll($sql);
		
		// Подключаем места
		$place_table = new Application_Model_DbTable_Places();
		
		// Выбираем данные мест
		$places = array();
		foreach ($places_ids as $row) {			
			$places[] = $place_table->getById($row['p_id']);
		}
		
		if(!empty($places)){
			return $places;
		}
		
		return false;
		
	}
	
	
	
	/*
	 * Метод для получения всех опубликованных поездок	 
	 */
	public function getList() {
		
		$select = $this->select();//->where('public = 1');
		$result = $this->fetchAll($select);
			
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
	
	
	/*
	 * Возвращает места для данной поездки
	 * @rarams id (integer) ID поездки
	 * @params options (array) Массив условий поиска
	 * @return result (array) Массив данных для записи
	 */	
	public function getById(){
		
		if (!empty($id)) {
			$select = $this->select()->where('id = (?)', $id);
			$result = $this->fetchRow($select);
			
			return $result->toArray();
		}
		
		return false;
	}
	
	/*
	 * Возвращает поездки для данного места
	 * @rarams id (integer) ID места
	 * @params options (array) Массив условий поиска
	 * @return result (array) Массив данных для записи
	 */	
	public function getByPlaceId($id, $options = array()){
		
		$return = array();
		
		if (!empty($id)) {
			// Получаем id как параметр
			$id = (int) $id;
			
			$select = $this->select();
			$select->where('p_id = (?)', $id);
			
			// Используем метод fetchAll для получения всех записей из базы.
			$result = $this->fetchAll($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return;
	}
	
	
	/*
	 * Добавление записи о месте в поездке
	 */
	public function add($data){
		
		if (!empty($data)) {
			$insert = $this->insert($data);
			if(!empty($insert)){
				
				/*// Подключаемся к модели
				$ratinger = new Application_Model_MemcacheCounter();
				// Передаем в метод айдишник записи - это должно быть что то типа user_23 или travel_88 или place_43
				$res = $ratinger->increment('place_'.$data['p_id']);
				$res = $ratinger->increment('travel_'.$data['t_id']);*/
				
				$ratinger = new Application_Model_MemcacheCounter();
				$res = $ratinger->calcRating('place',$data['p_id']);
				$res = $ratinger->calcRating('travel',$data['t_id']);
				
				return $insert;
			}
		}
		return;
	}

	
	/*
	 * Сохранение поездки в свои
	 */
	public function copyTravels($travel_in,$travel_out){
	
		if(!empty($travel_in) && !empty($travel_out)){
			// Получаем id как параметр
			$travel_in = (int) $travel_in;
			$travel_out = (int) $travel_out;

			$select = $this->select();
			$select->where('t_id = (?)',$travel_in);

			// Применяем метод fetchAll для выборки всех записей из таблицы,		
			$result = $this->fetchAll($select);

			if(!empty($result)){
				foreach($result as $row){
					$data = array('t_id'=>$travel_out,'p_id'=>$row['p_id']);
					$insert = $this->insert($data);
				}
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Сохранение места в свои поездки
	 */
	public function addPlaceToMyTravel($travel,$place, $travel_title = false){
		if(isset($travel) && !empty($place)){
			// Получаем id как параметр
			$travel = (int) $travel;
			$place = (int) $place;
			$add = 0;
			
			if(empty($travel) && !empty($travel_title)){
				
				$auth = new Application_Model_Auth();
				$user = $auth->check();

				$travels_table = new Api_Model_DbTable_Travels();

				$data = array(
					'title' => $travel_title,
					'owner' => $user['id'],
					'author' => $user['id']
				);
				
				$travel = $travels_table->add($data);
				if(!empty($travel)){
					$add = 1;
				}
			}
			elseif(!empty($travel)) {				
				$result = $this->check($travel, $place);	
				if(!empty($result)){
					$result['status'] = "already";
					return $result;					
				} else {
					$add = 1;
				}
			}
			
			if($add == 1) {
				$data = array('t_id'=>$travel,'p_id'=>$place);
				
				$insert = $this->add($data);
				if(!empty($insert)){
					$result['status'] = "new";
					return $data;
				}
			}			
		}
		return;
	}
	
	/*
	 * Проверка существования записи
	 */
	public function check($travel,$place){
		if(!empty($travel) && !empty($place)){
			// Получаем id как параметр
			$travel = (int) $travel;
			$place = (int) $place;
			
			$select = $this->select();
			$select->where('t_id = (?)',$travel);
			$select->where('p_id = (?)',$place);
			
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();
			}			
		}
		return false;
	}
	
	
	
	/*
	 * Метод для изменения дня поездки 
	 */
	public function chooseday($travel,$place,$day){
		if(!empty($travel) && !empty($place) && isset($day)){
			$select = $this->select();
			$select->where('t_id = ?', $travel);
			$select->where('p_id = ?', $place);
			
			$save = $this->fetchRow($select);
			$save->day = $day;
			$save->save();
			
			if(isset($save)){
				return true;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Группируем места поездки по дням
	 * @params $travel_id (integer) ID поездки
	 * @params $places (array) массив данных мест
	 */
	public function groupByDate($travel_id, $places){
		$return = array();
		
		if(!empty($travel_id) && !empty($places)) {
			foreach($places as $place){
				$result = $this->check($travel_id, $place['id']);
				if(!empty($result)){
					$day = !empty($result['day'])?$result['day']:0;
					
					$day_list = array();
					if($day == 0) {
						$day_list['active'] = " - - - Выберите день поездки - - - ";
					} else {
						$day_list['list'][0] = "Без дня";
					}
						
					$last = "";
					for($i=1; $i <= $day+3; $i++) {
						if($i==1||$i ==4||$i==5||$i%10==1||$i%10==4||$i%10==5||($i>9&&$i<20)) {
							$day_list['list'][$i] = $last = "$i-ый день поездки";
						}
						elseif($i==3||$i%10==3) {
							$day_list['list'][$i] = $last = "$i-ий день поездки";
						}
						//elseif($i==2||$i==6||$i==7||$i==8||$i%10==2||$i%10==6||$i%10==7||$i%10==8) {
						else {
							$day_list['list'][$i] = $last = "$i-ой день поездки";
						}

						if($i == $day) {
							$day_list['active'] = $last;
						}
					}
					if(!empty($day_list)) {
						$place['day_list'] = $day_list;
					}
					
					$return[$day][] = $place;
				}
			}
		}
		if(!empty($return)) {
			ksort($return);
			
			return $return;
		}
		return false;
	}

	
}
