<?php

class Application_Model_DbTable_Hotels extends Zend_Db_Table_Abstract
{
    
	protected $_name = 'hotels';
	
	
	
	
	/*
	 * Получаем список всех гостиниц по рейтингу 
	 */
	public function getList() {
		$select = $this->select()->where('public < 2')->order('rating DESC');
		
		$result = $this->fetchAll($select);
		
		if(!empty($result)) {
			return $result->toArray();
		}
		return false;
	}
	
	
	
	/*
	 * Получает гостиницы в поездке
	 * 
	 * @params - $places для того чтобы не запрашивать второй раз передаю список мест. Из них можно взять координаты.
	 * 
	 * return array or false
	 */
	public function getInTravels ($travel_id, $places) {
		
		if (!empty($places)) {
			$hotels_list = array();
			
			foreach ($places as $place) {				
				// Взято 1 градус = 60 миль
				if (!empty($place['lat']) && !empty($place['lng'])) {
					$res = $this->getHotelsByCoords($place['lat'], $place['lng']);
					if (!empty($res)) {
						$hotels_list[$res[0]['id']] = $res;						
					}
				}
			}
			// Исключаем дубликаты и формируем массив гостиниц
			if(!empty($hotels_list)) {
				$return = array();
				foreach($hotels_list as $hotels) {
					if(!empty($hotels)) {
						foreach($hotels as $hotel) {
							if(!in_array($hotel, $return)) {
								$sql = "SELECT * FROM hotelprice WHERE h_id = ".$hotel['id'];
								$rooms = $this->getAdapter()->query($sql)->fetchAll();
								if(!empty($rooms)) {
									$hotel['rooms'] = $rooms;
								}
								
								$return[] = $hotel;
							}
						}
					}					
				}
				if (!empty($return)) {
					return $return;
				}
			}
		}
		
		return false;
	}
	
	
	
	/*
	 * Получает отели в городе
	 * 
	 * return array or false
	 */
	public function getInCity ($city_id, $lat, $lng) {
			
		// Взято 1 градус = 60 миль
		if (!empty($lat) && !empty($lng)) {
			$array = $this->getHotelsByCoords($lat, $lng);
			
			if(!empty($array)) {
				$return = array();
				foreach($array as $row) {
					
					$sql = "SELECT * FROM hotelprice WHERE h_id = ".$row['id'];
					$rooms = $this->getAdapter()->query($sql)->fetchAll();

					if(!empty($rooms)) {
						$row['child'] = $rooms;
						// Вычисляем минимальную и максимальную стоимости
						$min_price = 100000000;
						$max_price = 0; // Попробуй найди номер дороже 100 млн руб
						foreach($rooms as $room) {
							if($room['price'] < $min_price) {
								$min_price = $room['price'];
							}
							if($room['price'] > $max_price) {
								$max_price = $room['price'];
							}
						}
						$prices = array();
						if($min_price < 100000000) {
							$prices[] = $min_price;
						}
						if($max_price > 0 && $max_price != $min_price) {
							$prices[] = $max_price;
						}
						if(!empty($prices)) {
							$row['price'] = implode(' - ', $prices);
						}
					}
					$return[] = $row;
				}
				return $return;
			}
			
		}
		
		return false;
	}
	
	
	
	/*
	 * Получает количество мест в городе
	 * 
	 * return integer or 0 if not found
	 */
	public function getCountInCity ($lat, $lng) {
		
		// Взято 1 градус = 60 миль
		if (!empty($lat) && !empty($lng)) {
			$hotels = $this->getHotelsByCoords($lat, $lng);
		}
		
		if(!empty($hotels)) {
			return count($hotels);
		}
		return 0;
	}
	
	
	
	/*
	 * ПОлучает отели у которых назавание похоже на $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchByPhrase ($keyphrase) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."%')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."%')";
		$parts[] = implode(" or ", $parts);		
		
		$select->where(implode(" or ", $parts).' and public = 1')->order('rating desc');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			$array = $result->toArray();
			
			if(!empty($array)) {
				$return = array();
				foreach($array as $row) {
					$sql = "SELECT * FROM hotelprice WHERE h_id = ".$row['id'];
					$rooms = $this->getAdapter()->query($sql)->fetchAll();
					
					if(!empty($rooms)) {
						$row['child'] = $rooms;
						$return[] = $row;
					}
				}
				return $return;
			}
		}
		
		return false;
	}
	
	
	
	/*
	 * ПОлучает отели у которых назавание начиается на $simbol
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchBySimbol ($simbol) {
		
		if(!empty($simbol)) {
			$select = $this->select();

			$parts = array();
			$parts[] = "UPPER(title) LIKE UPPER('".$simbol."%')";
			$parts[] = "UPPER(title) LIKE UPPER('% ".$simbol."%')";

			$select->where(implode(" or ", $parts))->where('public = 1')->order('rating desc');

			$result = $this->fetchAll($select);

			if (!empty($result)) {
				$array = $result->toArray();

				if(!empty($array)) {
					$return = array();
					foreach($array as $row) {
						$sql = "SELECT * FROM hotelprice WHERE h_id = ".$row['id'];
						
						$rooms = $this->getAdapter()->query($sql)->fetchAll();

						if(!empty($rooms)) {
							$row['child'] = $rooms;
							$return[] = $row;
						}
					}
					return $return;
				}
			}
		}
		
		return false;
	}
	
	
	
	/*
	 * 
	 * Получает гостиницы по координатам. 
	 * По умолчанию 60 миль.
	 * 
	 */
	public function getHotelsByCoords($lat, $lng) {

		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;

		// Взято 1 градус = 60 миль
		if (!empty($lat) && !empty($lng) && (is_float($lat) || !is_numeric($lat)) && (is_float($lng) || is_numeric($lng))) {
			$select = $this->getAdapter()->query('
				SELECT id, title, long_text, lng, lat, image_url, city, stars, 
				(6371 * acos(cos(radians('.$lat.')) * cos(radians(lat)) * cos(radians(lng) - radians('.$lng.')) + sin(radians('.$lat.')) * sin(radians(lat)))) AS distance
				FROM ' . $this->_name . ' HAVING distance < '.$config->hotels->radius.' ORDER BY distance');
			$result = $select->fetchAll();
			
			if (!empty($result)) {
				return $result;
			}
		}		
		return false;
		
	}	
	
		
	
	/*
	 * Получает лучшие гостиницы на главной странице
	 * @param offset (array) - смещение	 
	 */
	public function getMain($offset){
		
		$select = $this->select()->where('public = 1')->limit(4, $offset)->order('rating desc');
		$result = $this->fetchAll($select);
			
		if(!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
	
	
	
	/*
	 * Метод для получения гостиницы
	 * @params id (integer) ID гостиницы	 
	 */
	public function getById ($id) {
		
		if (!empty($id)) {
			$select = $this->select()->where('id = (?)', $id);
			$result = $this->fetchRow($select);
			
			if(!empty($result)) {
				return $result->toArray();
			}
		}
		
		return false;
	}
	
	
	
	/*
	 * Метод для получения номеров гостиницы
	 * @params id (integer) ID гостиницы	 
	 */
	public function getNumbers ($id) {
		
		if (!empty($id)) {
			
			$select = $this->select()->where('id = (?)', $id);
			$result = $this->fetchAll($select);
			
			if(!empty($result)) {
				return $result->toArray();
			}
		}
		
		return false;
	}
	
	
	
}
