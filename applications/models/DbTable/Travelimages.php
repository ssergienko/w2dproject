<?php

class Application_Model_DbTable_Travelimages extends Zend_Db_Table_Abstract {

	protected $_name = 'travel_images';

	
	
	/*
	 * Добавление поездки
	 */
	public function add($data){
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)){
				return $result;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получение картинок места
	 */
	public function getImagesByTrId($travel_id){
		
		if(!empty($travel_id)){
			$select = $this->select()->where('tr_id = '.$travel_id)->order('image_idx');
			$result = $this->fetchAll($select);
			if(!empty($result)){				
				return $result;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получение больших картинок
	 */
	public function getLargeImages($travel_id){
		
		if(!empty($travel_id)){
			$select = $this->select()->where('tr_id = '.$travel_id);
			$result = $this->fetchAll($select);
			if(!empty($result)){
				$images = array();
				foreach ($result->toArray() as $image) {
					$images[] = array('url' => $image['url'], 'image_txt' => $image['image_txt']);
				}
				return $images;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Проверяем есть ли запись с таким урлом
	 */
	public function checkImage ($travel_id, $image_url, $size) {
		$select = $this->select()->where('plid = '.$travel_id.' and img_url = '.$travel_url.' and size = '.$size);
		$res = $this->fetchAll($select);
		
		if ($res->count()) {
			return true;			
		}		
		return false;
	}
	
	
	
	
	
}
