<?php

class Application_Model_DbTable_Hotelprice extends Zend_Db_Table_Abstract {

	protected $_name = 'hotelprice';

	
	
	
	/*
	 * Возвращает номер по Id
	 * @rarams id (integer) ID номера
	 * @return result (array) Массив данных для номера
	 */	
	public function check($hotel_id, $room_id){
		
		if (!empty($hotel_id) && !empty($room_id)) {
			$return = array();
			
			// Получаем id как параметр
			$hotel_id = (int) $hotel_id;
			$room_id = (int) $room_id;
			
			$select = $this->select()->where('id = ?', $room_id)->where('h_id = ?', $hotel_id);
			
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return false;
	}
	

	
	
	
	/*
	 * Возвращает номер по Id
	 * @rarams id (integer) ID номера
	 * @return result (array) Массив данных для номера
	 */	
	public function getById($room_id){
		
		if (!empty($room_id)) {
			$return = array();
			
			// Получаем id как параметр
			$room_id = (int) $room_id;
			
			$select = $this->select()->where('id = ?', $room_id);
			
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return false;
	}
	
	
	
	
	
	
	/*
	 * Возвращает массив номеров для данной гостиницы
	 * @rarams id (integer) ID гостиницы
	 * @return result (array) Массив данных для номеров
	 */	
	public function getByHotelId($hotel_id){
		
		if (!empty($hotel_id)) {
			$return = array();
			
			// Получаем id как параметр
			$hotel_id = (int) $hotel_id;
			
			$select = $this->select()->where('h_id = ?', $hotel_id);
			
			$result = $this->fetchAll($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return false;
	}
}
