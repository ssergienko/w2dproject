<?php

class Application_Model_DbTable_Travels extends Zend_Db_Table_Abstract {

	protected $_name = 'travels';

	
	
	
	/*
	 * Получает черновик места. Т.е. если пользователь начал создавать поездку и не закончил, ему отобразятся данные которые от добавлял в прошлый раз
	 * 
	 * @params $user_id - user id (integer)
	 * 
	 */
	public function getDraft ($user_id) {
		
		if (!empty($user_id)) {
			$select = $this->select()->where('author = '.$user_id.' and confirm = 0');
			$res = $this->fetchRow($select);
			if (!empty($res)) {
				return $res;
			}
			return false;
		}
		
	}
	
	
	
	
	/*
	 * Редактируем поездку
	 * @params $place_id (integer) ID места
	 * @params $data (array) массив изменений
	 */
	public function edit($travel_id, $data){
		if(!empty($travel_id) && !empty($data))  {
			$travel_id = (int) $travel_id;
			
			$result = $this->update($data, "id = $travel_id");
			if(!empty($result)) {
				return true;
			}
		}
		return false;
	}
	
	
	
	
	
	/*
	 * Получаем место по Id для редактирования
	 */
	public function getByIdForEdit($travel_id) {
		// ПОлучаем места
		if(!empty($travel_id)) {
			$travel_id = (int) $travel_id;
			
			$select = $this->select()->where('id = ?',$travel_id);
			$result = $this->fetchRow($select);
			if (!empty($result)) {
				$result = $result->toArray();
				
				$travelplace_table = new Application_Model_DbTable_Travelplace();
				$travelplace = $travelplace_table->getPlacesByTravel($travel_id);
				
				if(!empty($travelplace)) {
					$result['places'] = $travelplace;
				}				
				
				return $result;
			}			
		}
		return false;
	}
	
	
	
		
	/*
	 * Возвращает мои поездки
	 */
	public function getMyTravels ($user_id) {
		
		// Получаем и возвращаем поездки пользователя
		if(!empty($user_id)){
			$select = $this->select()->where('owner = ?', $user_id)->order('id desc');
			
			$travels = $this->fetchAll($select);
			if(!empty($travels)) {
				$travels = $travels->toArray();
				
				
				$travelLike = new Application_Model_DbTable_TravelLike();
				$return = array();
				foreach($travels as $travel) {
					$sql = "SELECT * FROM places WHERE id IN (SELECT p_id FROM travelplace WHERE t_id = ".$travel['id'].")";
					$places = $this->getAdapter()->query($sql)->fetchAll();
					
					if(!empty($places)) {
						$travel['places'] = $places;
					}
					
					// Получаем лайки
					$count_travelLike = count($travelLike->getByPlaceId($travel['id']));
					$travel['travelLike'] = !empty($count_travelLike)?$count_travelLike:0;
					
					$return[] = $travel;
				}
				return $return;
			}
		}
		return false;
	}
	
	
	/*
	 * Возвращает поездки пользователя
	 */
	public function getByUser ($user_id) {
		
		// Получаем и возвращаем поездки пользователя
		if(!empty($user_id)){
			$select = $this->select()->where('owner = ?', $user_id)->where('public < 2');
			
			$result = $this->fetchAll($select);
			if(!empty($result)) {
				$result = $result->toArray();
				
				$return = array();
				foreach($result as $row) {
					$sql = "SELECT * FROM places WHERE id IN (SELECT p_id FROM travelplace WHERE t_id = ".$row['id'].") AND public < 2";
					$places = $this->getAdapter()->query($sql)->fetchAll();
					
					if(!empty($places)) {
						$row['places'] = $places;
					}
					$return[] = $row;
				}
				return $return;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Метод для получения данных поездки
	 * @params id (integer) Id поездки	 
	 * @return return (array) Массив данных поездки по всем вкладкам
	 */
	public function getById($travel_id) {
		
		if(!empty($travel_id)) {
			// ПОлучаем данные города
			$select = $this->select()->where('id = ?', $travel_id)->where('public < 2');
			$travel = $this->fetchRow($select);

			// Подключаем места
			$places_table = new Application_Model_DbTable_Places();
			// Подключаем отели
			$hotels_table = new Application_Model_DbTable_Hotels();

			$result = array();
			if (!empty($travel)) {	
				$travel = $travel->toArray();
				// Получаем места города
				$travel['places'] = $places_table->getInTravels($travel['id']);
//				$travel['hotels'] = $hotels_table->getInTravels($travel['id'], $travel['places']);

				return $travel;
			}
		}
		
	}
	
	
	
	/*
	 * Метод для получения данных поездки
	 * @params id (integer) Id поездки	 
	 * @return return (array) Массив данных поездки по всем вкладкам
	 */
	public function getByIdFromUser($travel_id) {
		
		if(!empty($travel_id)) {
			// ПОлучаем данные города
			$select = $this->select()->where('id = ?', $travel_id);
			$travel = $this->fetchRow($select)->toArray();

			// Подключаем места
			$places_table = new Application_Model_DbTable_Places();
			// Подключаем отели
			$hotels_table = new Application_Model_DbTable_Hotels();

			$result = array();
			if (count($travel)) {			
				// Получаем места города
				$travel['places'] = $places_table->getInTravels($travel['id']);			
				$travel['hotels'] = $hotels_table->getInTravels($travel['id'], $travel['places']);

				return $travel;
			}
		}
		
	}
	
	
	
	/*
	 * Получает поездки в городе
	 * 
	 * return array or false
	 */
	public function getInCity ($city_id) {
		
		// Выбираем поездки
		$travels = $this->getAdapter()->query(
				'SELECT travels.* FROM travels WHERE public < 2 AND id IN (
					SELECT t_id FROM travelplace WHERE p_id IN (
						SELECT id FROM places WHERE public < 2 and city = '.$city_id.'
					)
				) ORDER BY rating DESC'
		)->fetchAll();
		
		if (!empty($travels)) {
			$return = array();
			// Получаем список мест прикрепленных к поездке
			$travelLike = new Application_Model_DbTable_TravelLike();
			foreach($travels as $travel) {
				$sql = "SELECT * FROM places WHERE id IN (SELECT p_id FROM travelplace WHERE t_id = ".$travel['id'].") ORDER BY rating DESC";
				$places = $this->getAdapter()->query($sql)->fetchAll();
				if(!empty($places)){
					$travel['places'] = $places;
				}
				// Получаем лайки
				$count_travelLike = count($travelLike->getByPlaceId($travel['id']));
				$travel['travelLike'] = !empty($count_travelLike)?$count_travelLike:0;
				
				$return[] = $travel;
			}
			if(!empty($return)) {
				return $return;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получает количество поездок в городе
	 * 
	 * return integer or 0 if not found
	 */
	public function getCountInCity ($city_id) {
				
		$count_travels = $this->getAdapter()->query(
				'SELECT COUNT(id) FROM travels WHERE public < 2 AND id IN (
					SELECT t_id FROM travelplace WHERE p_id IN (
						SELECT id FROM places WHERE public < 2 and city = '.$city_id.'
					)
				)'
		)->fetchColumn();
		
		if (!empty($count_travels)) {
			return $count_travels;
		}
		return 0;
	}
	
	
	
	/*
	 * Получает самые популярные поездки на главной странице
	 */
	public function getMain ($offset = false) {
		
		$offset = !empty($offset) ? 'offset '.$offset : '';
		
		// Выбираем поездки
		$travels = $this->getAdapter()->query(
				'SELECT travels.* FROM travels WHERE public < 2 AND id IN (
					SELECT t_id FROM travelplace WHERE p_id IN (
						SELECT id FROM places WHERE public < 2
					)
				) order by rating desc limit 4'.$offset
		)->fetchAll();
		
		if (count($travels)) {

			// Получаем данные по местам для выбранных поездок
			$places_table = new Application_Model_DbTable_Travelplace();			
			$result = array();
			foreach ($travels as $travel) {
				// Получаем данные мест по поездкам
				$travel['places'] = $places_table->getPlacesByTravel($travel['id']);				
				$result[] = $travel;				
			}
			
			return $result;
		}
		
		
	}
	
	
		
	/*
	 * ПОлучает поездки у которых назавание похоже на $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchByPhrase ($keyphrase) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."%')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."%')";
		$parts[] = implode(" or ", $parts);		
		
		$select->where(implode(" or ", $parts).' and public < 2')->order('rating desc');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			$array = $result->toArray();
			
			if(!empty($array)) {
				$return = array();
				foreach($array as $row) {
					$sql = "SELECT * FROM places WHERE id IN (SELECT p_id FROM travelplace WHERE t_id = ".$row['id'].")";
					$places = $this->getAdapter()->query($sql)->fetchAll();
					
					if(!empty($places)) {
						$row['child'] = $places;
						$return[] = $row;
					}
				}
				return $return;
			}
		}
		
		return false;
	}
	
	
	
	/*
	 * ПОлучает поездки у которых назавание начинается с $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) citys
	 * 
	 */
	public function searchBySimbol ($simbol) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$simbol."%')";
		$parts[] = "UPPER(title) LIKE UPPER('% ".$simbol."%')";
		
		$select->where(implode(" or ", $parts))->order('rating desc');
		$select->where('public < 2');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			$array = $result->toArray();
			
			if(!empty($array)) {
				$return = array();
				foreach($array as $row) {
					$sql = "SELECT * FROM places WHERE id IN (SELECT p_id FROM travelplace WHERE t_id = ".$row['id'].")";
					$places = $this->getAdapter()->query($sql)->fetchAll();
					
					if(!empty($places)) {
						$row['child'] = $places;
						$return[] = $row;
					}
				}
				return $return;
			}
		}
		
		return false;
				
	}
	
	
	
	/*
	 * Метод для получения всех опубликованных мест
	 * @param limit - если передан, возвращаем указанное количество
	 */
	public function getList($limit = false) {
		
		$select = $this->select()->where('public < 2 and confirm = 1')->order('rating desc');
		// Если задан лимит
		if (!empty($limit)) {
			$select->limit($limit);
		}		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
	
		
	/*
	 * Метод для добавления поездки
	 * @params data (array) Массив полей
	 * @return insert (integer) Id поездки
	 */
	public function add($data){
		if(!empty($data)){
			
			if(!empty($data['owner']) && !empty($data['author'])){
				
				$data['create_date'] = date('Y-m-d H:i:s');
				
				$insert = $this->insert($data);

				if(!empty($insert)){
					return $insert;
				}
			}
			
		}
		return;
	}
	
	
	
	/*
	 * Сохраняет название поездки
	 * 
	 */
	public function savetitle($options) {
		
		if (!empty($options['id']) && !empty($options['title'])) {
			
			$title = !empty($options['title']) ? (string)$options['title'] : '';
			$data = array('title' => $title);
			
			$res = $this->update($data, 'id = '.$options['id']);
			
			return $res;
		}
		
		return false;
		
	}
	
	
	
	
	/*
	 * Обновление рейтинга
	 */
	public function updateRating($id, $value, $options = false){
		if(!empty($id) && !empty($value)){
			$id = (int) $id;
			$value = (int) $value;
			
			if(!empty($options['refresh'])){
				$sql = "UPDATE $this->_name SET rating = $value WHERE id = $id";
			}
			else{	
				$sql = "UPDATE $this->_name SET rating = rating + ($value) WHERE id = $id";
			}
			
			
			$result = $this->getAdapter()->query($sql);
			//$result = $this->update(array('rating' => "'rating' + $value"), "id = $id");
			if(!empty($result)){
				return true;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получаем информацию о днях в поездке
	 * @params places (array) Массив мест в поездке
	 * @return return (array) Массив результатов
	 */
	public function infoDate($places){
		$return = array();
		
		$return['places_day'] = 0;
		foreach($places as $place){
			if($place['day'] > $return['places_day']){
				$return['places_day'] = $place['day'];
			}
		}
		if($return['places_day'] == 1 || $return['places_day']%10 == 1){
			$return['places_day_text'] = 'день';
		}
		elseif(($return['places_day'] > 1 && $return['places_day'] < 5) || ($return['places_day']%10 > 1 && $return['places_day']%10 < 5)){
			$return['places_day_text'] = 'дня';
		}
		else{
			$return['places_day_text'] = 'дней';
		}
		return $return;
	}
		
	
	
	/*
	 * Получаем информацию о днях в поездке
	 * @params places (integer) Колличество мест в поездке
	 * @return return (array) Массив результатов
	 */
	public function infoPlace($count_places){
		$return = array();
		
		$return['places_count'] = $count_places;
		if($return['places_count'] == 1 || $return['places_count']%10 == 1){
			$return['places_count_text'] = 'место';
		}
		elseif(($return['places_count'] > 1 && $return['places_count'] < 5) || ($return['places_count']%10 > 1 && $return['places_count']%10 < 5)){
			$return['places_count_text'] = 'места';
		}
		else{
			$return['places_count_text'] = 'мест';
		}
		return $return;
	}	
	
	
	
	
	
	
	
}
