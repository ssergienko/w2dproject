<?php

class Application_Model_DbTable_City extends Zend_Db_Table_Abstract {

	protected $_name = 'city';


    /*
	 * Метод для получения id города по имени
	 * @params $city_title (varchar) название города
	 * @return return (integer) Id города
	 */
	public function getByName($city_title) {
		
		if(!empty($city_title)) {
			$select = $this->select()->where('UPPER(title) = UPPER(?)', $city_title);	
			$result = $this->fetchRow($select);
			
			if(!empty($result)) {
				return $result->toArray();				
			}
		}
		return false;
	}


    /*
     * Возвращает мои города
     */
    public function getMyCities ($user_id) {
        // Получаем и возвращаем города пользователя
        if(!empty($user_id)){
            $select = $this->select()->where('author = '.$user_id.' and confirm = 1')->order('id desc');
            $cities = $this->fetchAll($select);
            if(!empty($cities)) {
                return $cities->toArray();
            }
        }
        return false;
    }



    /*
	 * Получаем место по Id для редактирования
	 */
    public function getByIdForEdit($city_id) {
        // ПОлучаем места
        if(!empty($city_id)) {
            $city_id = (int) $city_id;

            $select = $this->select()->where('id = ?', $city_id);
            $result = $this->fetchRow($select);
            if (!empty($result)) {
                return $result->toArray();
            }
        }
        return false;
    }



    /*
     * Возвращает места пользователя
     */
    public function getByUser ($user_id) {

        // Получаем и возвращаем поездки пользователя
        if(!empty($user_id)){
            $select = $this->select()->where('author = ?', $user_id)->where('public < 2 and confirm = 1');

            $result = $this->fetchAll($select);
            if(!empty($result)) {
                return $result->toArray();
            }
        }
        return false;
    }


    /*
     * Метод для получения id города по имени
     * @params $city_title (varchar) название города
     * @return return (integer) Id города
     */
	public function getNameById($city_id) {
		
		if(!empty($city_id)) {
			$select = $this->select()->where('id = (?)', $city_id);	
			$result = $this->fetchRow($select)->toArray();
			
			if(!empty($result)) {
				return $result['title'];
			}
		}
		return false;
	}
	
	
	
	
	/*
	 * Получает самые популярные города на главной странице
	 * 
	 * return $result - array() - список городов c кол-вом мест, поездок и гостиниц
	 */
	public function getMain ($offset = false, $lat, $lng) {
		
		$offset = !empty($offset) ? ' offset '.$offset : '';
		
		if (empty($lat) || empty($lng)) {
			$lat = '55.741443';
			$lng = '37.630121';
		}
		
		// Получаем города
		$citys = $this->getAdapter()->query(
			'SELECT *, (6371 * acos(cos(radians('.$lat.')) * cos(radians(lat)) * cos(radians(lng) - radians('.$lng.')) + sin(radians('.$lat.')) * sin(radians(lat)))) AS distance 
			FROM city WHERE public = 1 order by distance asc, rating desc'.$offset.' limit 8');
		
		return $citys->fetchAll();

	}
	
	
	
	
	/*
	 * Получаем список всех городов по рейтингу 
	 */
	public function getList() {
		$select = $this->select()->where('public = 1')->order('rating DESC');
		
		$result = $this->fetchAll($select);
		
		if(!empty($result)) {
			return $result->toArray();
		}
		return false;
	}
	
		
	
	/*
	 * ПОлучает города у которых назавание начинается с $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) citys
	 * 
	 */
	public function searchBySimbol ($simbol) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$simbol."%')";
		$parts[] = "UPPER(title) LIKE UPPER('% ".$simbol."%')";	
		
		$select->where(implode(" or ", $parts))->order('rating desc');
		$select->where('public = 1');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
				
	}
	
	
	
	
	/*
	 * ПОлучает города у которых назавание похоже на $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchByPhrase ($keyphrase) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."%')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."%')";
		$parts[] = implode(" or ", $parts);		
		
		$select->where(implode(" or ", $parts))->order('rating desc');
		$select->where('public = 1');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
		
	
	
	/*
	 * Метод для получения данных города
	 * @params id (integer) Id города	 
	 * @return return (array) Массив данных города по всем вкладкам
	 */
	public function getById($city_id) {
		
		// ПОлучаем данные города
		$select = $this->select()->where('id = ?', $city_id);
		$city = $this->fetchRow($select);
		
		if(!empty($city)) {
			$city = $city->toArray();
			// Подключаем места
			$places_table = new Application_Model_DbTable_Places();
			// Подключаем поездки
			$travels_table = new Application_Model_DbTable_Travels();
			// Подключаем отели
			$hotels_table = new Application_Model_DbTable_Hotels();

			$result = array();
			if (count($city)) {
				// Получаем места города
				$city['places'] = $places_table->getInCity($city['id']);
				$city['travels'] = $travels_table->getInCity($city['id']);
				$city['hotels'] = $hotels_table->getInCity($city['id'], $city['lat'], $city['lng']);

				return $city;
			}
		}
		return false;
		
	}

	
	
	
	/*
	 * Метод для получения данных города
	 * @params id (integer) Id города	 
	 * @return return (array) Массив данных города по всем вкладкам
	 */
	public function getForAutocomplete($city_id) {
		
		// ПОлучаем данные города
		$select = $this->select()->where('id = ?', $city_id);
		$res = $this->fetchRow($select);
		if (!empty($res)) {
			return $res;
		}
		
		return false;
		
	}
	
	
	
	
	/*
	 * Метод для добавления города
	 * @param post (array) Массив данных
	 * @return result (integer) ID добавленного города
	 */
	public function add($data){
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)){
				return $result;
			}
		}
		return false;
	}


    /*
	 * Редактируем город
	 * @params $city_id (integer) ID места
	 * @params $data (array) массив изменений
	 */
    public function edit($city_id, $data){
        if(!empty($city_id) && !empty($data))  {
            $city_id = (int) $city_id;

            $result = $this->update($data, "id = $city_id");

            if(!empty($result)) {
                return true;
            }
        }
        return false;
    }

	
	/*
	 * Обновление рейтинга
	 */
	public function updateRating($id, $value, $options = false){
		if(!empty($id) && !empty($value)){
			$id = (int) $id;
			$value = (int) $value;
			
			if(!empty($options['refresh'])){
				$sql = "UPDATE $this->_name SET rating = $value WHERE id = $id";
			}
			else{	
				$sql = "UPDATE $this->_name SET rating = rating + ($value) WHERE id = $id";
			}
			
			
			$result = $this->getAdapter()->query($sql);
			//$result = $this->update(array('rating' => "'rating' + $value"), "id = $id");
			if(!empty($result)){
				return true;
			}
		}
		return false;
	}



    /*
	 * Получает черновик города. Т.е. если пользователь начал создавать поездку и не закончил, ему отобразятся данные которые от добавлял в прошлый раз
	 *
	 * @params $user_id - user id (integer)
	 *
	 */
    public function getDraft ($user_id) {

        if (!empty($user_id)) {
            $select = $this->select()->where('author = '.$user_id.' and confirm = 0');
            $res = $this->fetchRow($select);
            if (!empty($res)) {
                return $res;
            }
            return false;
        }

    }


	
}
