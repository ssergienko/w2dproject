<?php

class Application_Model_DbTable_Promo extends Zend_Db_Table_Abstract
{
    protected $_name = 'promo';
    
	/*
	 * Проверяем существование промопользователя
	 * @params $key (varchar)
	 */
	public function checkForKey($key) {
		if(!empty($key)) {
			$select = $this->select()->where("unic_id = ?", $key);
			
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();
			}
		}
		return false;
	}
	
	
	
	/*
	 * Проверяем существование промопользователя
	 * @params $email (varchar)
	 */
	public function checkForEmail($email) {
		if(!empty($email)) {
			$select = $this->select()->where("email = ?", $email);
			
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();
			}
		}
		return false;
	}
	
	
	public function clear() {
		$select = $this->select();
		
		$result = $this->fetchAll($select);
		if(!empty($result)) {
			$list = array();
			foreach($result as $row) {
				if(!in_array($row['email'], $list)) {
					$list[] = $row['email'];
				} else {
					$sql = "DELETE FROM promo WHERE id = ".$row['id'];
					$this->getAdapter()->query($sql);
				}
			}
			return true;
		}
		return false;
	}
	
	public function setKey($email = false) {
		$select = $this->select();
		
		$result = $this->fetchAll($select);
		if(!empty($result)) {
			$list = array();
			foreach($result as $row) {
				
				if(empty($row['unic_id'])) {
					$rand = uniqid();

					$this->update(array('unic_id' => $rand), "id = ".$row['id']);
				}
			}
			return true;			
		}
		return false;
	}
	
	public function setSingleKey($email) {
		
		if(!empty($email)) {
			$rand = uniqid();
			$this->update(array('unic_id' => $rand), "email = '".$email."'");
			
			return $rand;			
		}
		return false;
	}
	
}
