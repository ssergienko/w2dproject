<?php

class Application_Model_DbTable_TravelFav extends Zend_Db_Table_Abstract {

	protected $_name = 'travel_fav';

	public function getList(){
		$select = $this->select();
		$select->order('id desc');
		
		$result = $this->fetchAll($select);
		if(!empty($result)){
			return $result->toArray();
		}
	}
	
	/*
	 * Проверяем наличие записи о поездке в избранном
	 * @params u_id (integer) Id пользователя
	 * @return result (array) Массив данных
	 */	
	public function getByUser($u_id){
		
		$return = array();
		
		if (!empty($u_id)) {
			$select = $this->select();
			$select->where('u_id = ?',$u_id);
			
			$result = $this->fetchAll($select);
			if(!empty($result)){
				$result = $result->toArray();
				
				$travels_table = new Application_Model_DbTable_Travels();
				
				foreach($result as $row){
					$travelFav = $row;
					
					$result_travel = $travels_table->getById($row['t_id']);
					if(!empty($result_travel)) {
						$travelFav['travel'] = $result_travel;
						$return[] = $travelFav;
					}
				}
				
				if(!empty($return)){
					$result = $return;
				}
				return $result;
			}
		}
		return;
	}

	/*
	 * Проверяем наличие записи о поездке в избранном
	 * @params t_id (integer) Id поездки
	 * @params u_id (integer) Id пользователя
	 * @return result (array) Массив данных
	 */	
	public function check($t_id, $u_id){
		
		if (!empty($t_id) && !empty($u_id)) {
			$select = $this->select();
			
			$select->where('t_id = ?',$t_id);
			$select->where('u_id = ?',$u_id);
			
			$result = $this->fetchRow($select);
			if(!empty($result)){
				return $result->toArray();
			}
		}
		return;
	}
	
	/*
	 * Добавляем поездку в избранное
	 * @params data (array) Массив данных
	 * @return insert (integer) Id записи
	 */	
	public function add($data){
		
		if (!empty($data)) {
			$insert = $this->insert($data);
			if(!empty($insert)){
				
				// Увеличиваем рэйтинг на 1
				$ratinger = new Application_Model_MemcacheCounter();
				$res = $ratinger->calcRating('travel',$data['t_id']);
				
				return $insert;
			}
		}
		return;
	}
	
	
	
	
	/*
	 * Удаляем запись по данным
	 * @params $data (array) Массив данных
	 */
	public function deleteByData($data) {
		if (!empty($data)) {
			$sql = "DELETE FROM $this->_name WHERE t_id = ".$data['t_id']." AND u_id = ".$data['u_id'];
			$result = $this->getAdapter()->query($sql);
			
			if(!empty($result)){
				// Уменьшаем рэйтинг на 1
				$ratinger = new Application_Model_MemcacheCounter();
				$res = $ratinger->calcRating('travel',$data['t_id'], 'decrement');
				return true;
			}
		}
		return false;
	}
}
