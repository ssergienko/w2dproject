<?php

class Application_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';
    
	/*
	 * Получаем пользователя
	 */
	public function getById($id){
		
		if (!empty($id)) {
			// Получаем id как параметр
			$id = (int) $id;
			$select = $this->select()->where('id = ?', $id);
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}		
	}
	
	
	/*
	 * Получает пользователя по email
	 */
	public function getByEmail ($email) {
		if (!empty($email)) {
			$select = $this->select()->where('uemail = ?', $email);
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result;				
			}
			return false;
		}
	}
	
	
    
	/*
	 * Получаем список пользователей
	 */
	public function getList(){
		
		$select = $this->select();
		$result = $this->fetchAll($select);
		
		if(!empty($result)){
            return $result->toArray();
        }
		return;
	}
	
	
	
    /*
	 *  Метод для получения данных пользователя по fb идентификатору
	 */
    public function getFbSocialId($id)
    {
		if (!empty($id)) {
			// Получаем id как параметр
			$id = (int) $id;
			$select = $this->select()->where('fb_id = ?', $id);
			//$select = $this->select()->where('id = ?', $id);
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return;		
    }
	
	
	
	/*
	 *  Метод для получения данных пользователя по vk идентификатору
	 */
    public function getVkSocialId($id)
    {
		if (!empty($id)) {
			// Получаем id как параметр
			$id = (int) $id;
			$select = $this->select()->where('vk_id = ?', $id);
			//$select = $this->select()->where('id = ?', $id);
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				return $result->toArray();				
			}
		}
		return;		
    }
	
	
	
	/*
	 * Метод для получения данных пользователя по id
	 * @params (integer) ID пользователя
	 * @return (array) Массив данных пользователя
	 */
    public function getProfile($id)
    {
		if(!empty($id)){
			$id = (int)$id;
			
			$select = $this->select()->where('id = ?', $id);
			$result = $this->fetchRow($select);
			
			if(!empty($result)){
				$result = $result->toArray();
				
				$auth = new Zend_Session_Namespace('auth');
				$result['provider'] = $auth->provider;
				
				return $result;
			}
		}
		return;        
    }
		
	
	/*
	 * Поучение профиля
	 */
	public function getVkProfile ($uid) {
		
		$auth = new Zend_Session_Namespace('auth');		
		$connection_data = json_decode(file_get_contents($url = $auth->api_url.'method/getProfiles?uid='.$auth->social_id.'&access_token='.$auth->access_token));
		
		$user = $this->getProfile($uid);
		
		if ($connection_data) {
			return array(			
				'api_url' => 'https://api.vkontakte.ru/',
				'access_token' => $auth->access_token,
				'social_id' => $connection_data->response[0]->uid,
				'uname' => $user['uname'],
				'vk_first_name' => $connection_data->response[0]->first_name,
				'vk_last_name' => $connection_data->response[0]->last_name,
				'provider' => $auth->provider
			);
		}
		
	}
	
	
	
	/*
	 * Редактируем пользователя
	 */
	public function edit($id, $data) {
		if(!empty($id) && !empty($data)) {
			$result = $this->update($data, "id = $id");
			if(!empty($result)) {
				return true;
			}
		}
		return false;
	}
	
	
	/*
	 * Проверяем существование по полю
	 * @params $field_name (varchar) Название поля в таблице
	 * @params $field_value (varchar) Значение поля в таблице
	 */
	public function checkByField($field_name, $field_value) {
		if(!empty($field_name) && !empty($field_value) ) {
			$select = $this->select()->where("$field_name = ?", $field_value);
			$result = $this->fetchAll($select)->toArray();
			if(!empty($result)){
				return true;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Авторизация для промо пользователей
	 */
	public function addPromo($data) {
		if(!empty($data)) {
			
			$result = $this->insert($data);
			if(!empty($result)){
				return $result;
			}
		}
	}
	
}
