<?php

class Application_Model_DbTable_Placeimages extends Zend_Db_Table_Abstract {

	protected $_name = 'place_images';

	
	
	/*
	 * Добавление места
	 */
	public function add($data) {
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)) {
				return $result;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получение картинок места
	 */
	public function getImagesByPlId($place_id){
		
		if(!empty($place_id)){
			$select = $this->select()->where('pl_id = '.$place_id)->order('idx');
			$result = $this->fetchAll($select);
			if(!empty($result)){				
				return $result;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Получение больших картинок
	 */
	public function getLargeImages($place_id){
		
		if(!empty($place_id)){
			$select = $this->select()->where('pl_id = '.$place_id)->order('idx');
			$result = $this->fetchAll($select);
			if(!empty($result)){
				$images = array();
				foreach ($result->toArray() as $image) {
					$images[] = array('url' => $image['url'], 'image_txt' => $image['image_txt']);
				}
				return $images;
			}
		}
		return false;
	}
	
	
	
	/*
	 * Проверяем есть ли запись с таким урлом
	 */
	public function checkImage ($place_id, $image_url, $size) {
		$select = $this->select()->where('plid = '.$place_id.' and img_url = '.$image_url.' and size = '.$size);
		$res = $this->fetchAll($select);
		
		if ($res->count()) {
			return true;			
		}		
		return false;
	}
	
	
	
	
}
