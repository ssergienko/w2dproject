<?php

class Application_Model_DbTable_Places extends Zend_Db_Table_Abstract {

	protected $_name = 'places';

	
	
	/*
	 * 
	 * Получает места по координатам. 
	 * По умолчанию 60 миль.
	 * 
	 */
	public function getPlacesByCoords($lat, $lng) {

		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;

		// Взято 1 градус = 60 миль
		if (!empty($lat) && !empty($lng)) {
			
			$select = $this->getAdapter()->query('
				SELECT id, title, long_text, lng, lat, image_url, 
				(6371 * acos(cos(radians('.$lat.')) * cos(radians(lat)) * cos(radians(lng) - radians('.$lng.')) + sin(radians('.$lat.')) * sin(radians(lat)))) AS distance
				FROM ' . $this->_name . ' ORDER BY distance');

            /*old version. shows places around selected rarius in km
             * $select = $this->getAdapter()->query('
				SELECT id, title, long_text, lng, lat, image_url,
				(6371 * acos(cos(radians('.$lat.')) * cos(radians(lat)) * cos(radians(lng) - radians('.$lng.')) + sin(radians('.$lat.')) * sin(radians(lat)))) AS distance
				FROM ' . $this->_name . ' HAVING distance < '.$config->around->radius.' ORDER BY distance');*/
			
			$result = $select->fetchAll();
			
			if (!empty($result)) {
				return $result;
			}
		}		
		return false;
		
	}
	
	
	
	
	/*
	 * Получает черновик места. Т.е. если пользователь начал создавать поездку и не закончил, ему отобразятся данные которые от добавлял в прошлый раз
	 * 
	 * @params $user_id - user id (integer)
	 * 
	 */
	public function getDraft ($user_id) {
		
		if (!empty($user_id)) {
			$select = $this->select()->where('author = '.$user_id.' and confirm = 0');
			$res = $this->fetchRow($select);
			if (!empty($res)) {
				return $res;
			}
			return false;
		}
		
	}
	
	
	
	/*
	 * Редактируем место
	 * @params $place_id (integer) ID места
	 * @params $data (array) массив изменений
	 */
	public function edit($place_id, $data){
		if(!empty($place_id) && !empty($data))  {
			$place_id = (int) $place_id;
			//return "UPDATE place SET ".serialize($data)." WHERE id = $place_id";
			$result = $this->update($data, "id = $place_id");
			if(!empty($result)) {
				return true;
			}
		}
		return false;
	}
	
	
	
	
	
	/*
	 * Получаем место по Id для редактирования
	 */
	public function getByIdForEdit($place_id) {
		// ПОлучаем места
		if(!empty($place_id)) {
			$place_id = (int) $place_id;
			
			$select = $this->select()->where('id = ?',$place_id);
			$result = $this->fetchRow($select);
			if (!empty($result)) {
				$result = $result->toArray();
				
				
				$city_table = new Application_Model_DbTable_City();
				
				$city = $city_table->getForAutocomplete($result['city']);
				if(!empty($city)) {
					$result['city_data'] = $city;
				}
				
				return $result;
			}			
		}
		return false;
	}
	
	
	
	
	
	/*
	 * Возвращает мои места
	 */
	public function getMyPlaces ($user_id) {
		
		// Получаем и возвращаем поездки пользователя
		if(!empty($user_id)){
			$select = $this->select()->where('owner = '.$user_id.' and confirm = 1')->order('id desc');
			
			$places = $this->fetchAll($select);
			if(!empty($places)) {
				$places = $places->toArray();
				
				$return = array();
			
				$placeLike = new Application_Model_DbTable_PlaceLike();
				foreach($places as $place) {
					// Получаем лайки
					$count_placeLike = count($placeLike->getByPlaceId($place['id']));
					$place['placeLike'] = !empty($count_placeLike)?$count_placeLike:0;				

					$return[] = $place;
				}
				return $return;
			}
		}
		return false;
	}	
	
	
	
	/*
	 * Возвращает места пользователя
	 */
	public function getByUser ($user_id) {
		
		// Получаем и возвращаем поездки пользователя
		if(!empty($user_id)){
			$select = $this->select()->where('owner = ?', $user_id)->where('public < 2 and confirm = 1');
			
			$result = $this->fetchAll($select);
			if(!empty($result)) {
				return $result->toArray();
			}
		}
		return false;
	}	
	
	
	
	/*
	 * Получает места в поездке
	 * 
	 * return array or false
	 */
	public function getInTravels ($travel_id) {
		
		$places = $this->getAdapter()->query('SELECT * FROM places WHERE public < 2 AND id IN (SELECT p_id from travelplace where t_id = '.$travel_id.') ORDER BY RATING DESC')->fetchAll();
		
		if (!empty($places)) {
			$return = array();
			
			$placeLike = new Application_Model_DbTable_PlaceLike();
			foreach($places as $place) {
				// Получаем лайки
				$count_placeLike = count($placeLike->getByPlaceId($place['id']));
				$place['placeLike'] = !empty($count_placeLike)?$count_placeLike:0;				
				
				$return[] = $place;
			}
			return $return;
		}
		return false;
	}
	
	
	
	/*
	 * Получает места в городе
	 * 
	 * return array or false
	 */
	public function getInCity ($city_id) {
		
		$places = $this->getAdapter()->query('SELECT * FROM places WHERE public < 2 AND city = '.$city_id.' ORDER BY RATING DESC')->fetchAll();
		
		if (!empty($places)) {
			$return = array();
			
			$placeLike = new Application_Model_DbTable_PlaceLike();
			foreach($places as $place) {
				// Получаем лайки
				$count_placeLike = count($placeLike->getByPlaceId($place['id']));
				$place['placeLike'] = !empty($count_placeLike)?$count_placeLike:0;				
				
				$return[] = $place;
			}
			
			return $return;
		}
		return false;
	}
	
	
	
	/*
	 * Получает количество мест в городе
	 * 
	 * return integer or 0 if not found
	 */
	public function getCountInCity ($city_id) {
		
		$count_places = $this->getAdapter()->query('SELECT COUNT(id) FROM places WHERE public < 2 AND city = '.$city_id.' ORDER BY RATING DESC')->fetchColumn();
		
		if (!empty($count_places)) {
			return $count_places;
		}
		return 0;
	}
	
	
	/*
	 * Получает места для главной страницы
	 */
	public function getMain ($offset = false, $lat, $lng) {

        $offset = !empty($offset) ? ' offset '.$offset : '';
        if (empty($lat) || empty($lng)) {
            $lat = '55.741443';
            $lng = '37.630121';
        }

        $result = $this->getAdapter()->query('SELECT * , ( 6371 * ACOS( COS( RADIANS('.$lat.') ) * COS( RADIANS( lat ) ) * COS( RADIANS( lng ) - RADIANS('.$lng.') ) + SIN( RADIANS('.$lat.') ) * SIN( RADIANS( lat ) ) ) ) AS distance
        FROM places
        WHERE public < 2
        ORDER BY distance ASC , rating DESC
        LIMIT 8');

        if (!empty($result)) {
            return $result;
        }

		return false;
	}
	
		
	
	/*
	 * Метод для получения всех опубликованных мест
	 * @param limit - если передан, возвращаем указанное количество
	 */
	public function getList($limit = false) {

		$select = $this->select()->where('public < 2 and confirm = 1')->order('rating desc');
		// Если задан лимит
		if (!empty($limit)) {
			$select->limit($limit);
		}		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
		
	
	/*
	 * ПОлучает места у которых назавание похоже на $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchByPhrase ($keyphrase) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."')";
		$parts[] = "UPPER(title) LIKE UPPER('".$keyphrase."%')";
		$parts[] = "UPPER(title) LIKE UPPER('%".$keyphrase."%')";
		$parts[] = implode(" or ", $parts);		
		
		$select->where(implode(" or ", $parts))
				->where('public < 2')
				->where('confirm = 1')
				->order('rating desc');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
	
	
	/*
	 * ПОлучает места у которых назавание начинается с $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) citys
	 * 
	 */
	public function searchBySimbol ($simbol) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(title) LIKE UPPER('".$simbol."%')";
		$parts[] = "UPPER(title) LIKE UPPER('% ".$simbol."%')";
		
		$select->where(implode(" or ", $parts))->order('rating desc');
		$select->where('public < 2');
		$select->where('confirm = 1');
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
				
	}
	
	
	
	/*
	 * Метод для получения места
	 * @params id (integer) ID места
	 * @return result (array) Массив данных места
	 */
	public function getById($id) {	
		// ПОлучаем места
		if(!empty($id)) {
			$id = (int) $id;
			
			$select = $this->select()->where('id = ?',$id)->where('public < 2 and confirm = 1');
			$result = $this->fetchRow($select);
			if (!empty($result)) {
				return $result->toArray();
			}			
		}
		return false;
	}
	
	
	/*
	 * Метод для получения места
	 * @params id (integer) ID места
	 * @return result (array) Массив данных места
	 */
	public function getByIdFromUser($id) {	
		// ПОлучаем места
		if(!empty($id)) {
			$id = (int) $id;
			
			$select = $this->select()->where('id = ?',$id);
			$result = $this->fetchRow($select);
			if (!empty($result)) {
				return $result->toArray();
			}			
		}
		return false;
	}
	
	
	
	/*
	 * Сохраняет описание места
	 * 
	 */
	public function savedescr($options) {
		
		if (!empty($options['id']) && !empty($options['descr'])) {
			
			$descr = !empty($options['descr']) ? (string)$options['descr'] : '';
			$data = array('long_text' => $descr);
			
			$res = $this->update($data, 'id = '.$options['id']);			
			
			return $res;
		}
		
		return false;
		
	}
	
	
	
	/*
	 * Сохраняет название места
	 * 
	 */
	public function savetitle($options) {
		
		if (!empty($options['id']) && !empty($options['title'])) {
			
			$title = !empty($options['title']) ? (string)$options['title'] : '';
			$data = array('title' => $title);
			
			$res = $this->update($data, 'id = '.$options['id']);
						
			return $res;
		}
		
		return false;
		
	}
	
	/*
	 * Выбираем список городов 
	 */
	public function getCity($id) {
		if (!empty($id)) {
			// Получаем id как параметр
			$id = (int) $id;
			
			$select = "SELECT city FROM places WHERE id = $id";
			
			$sql = $this->getAdapter()->query($select);
			$result = $sql->fetchColumn();
			
			if(!empty($result)){
				return $result;
			}			
		}
		return;
	}
	
	/*
	 * Обновление рейтинга
	 */
	public function updateRating($id, $value, $options = false){
		if(!empty($id) && !empty($value)){
			$id = (int) $id;
			$value = (int) $value;
			
			if(!empty($options['refresh'])){
				$sql = "UPDATE $this->_name SET rating = $value WHERE id = $id";
			}
			else{	
				$sql = "UPDATE $this->_name SET rating = rating + ($value) WHERE id = $id";
			}
			
			$result = $this->getAdapter()->query($sql);
			if(!empty($result)){
				return true;
			}
		}
		return false;
	}
	
	
	
	public function setLogo($place_id, $logo_url) {		
		
		if (!empty($logo_url) && $place_id) {
			return $this->update(array('image_url' => $logo_url), 'id = '.$place_id);
		}
		return false;
	}
	
	
	
}
