<?php

/*
 * Класс работы с путешествияем 
 */

/**
 * Description of Travel
 *
 * @author ssergienko
 */


class Travelslist extends Zend_Db_Table_Abstract {
	
	protected $_name = 'travels';
	
	public function getTravels() {
		
		// Применяем метод fetchAll для выборки всех записей из таблицы,
	    // и передаём их в view, через следующую запись
		$select = $this->select()->order('id desc');
	    $travels = $this->fetchAll($select);
		
		return $travels;
		
	}
	
}

?>
