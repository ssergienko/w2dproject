<?php
/**
 * Description of users
 *
 * @author mr.debugg
 */

class user extends Zend_Db_Table {
    protected $_name = 'users';

    public function isUnique($email)
    {
        $select = $this->_db->select()->from($this->_name)->where('uemail = ?', $email);
        $result = $this->getAdapter()->fetchOne($select);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;

        }
    }

    public function createUserDir ($uid)
    {
        $config = Zend_Registry::get('config');
        mkdir($config->path->uploads.'/'.$uid, 0777);
    }

    public function getUserData ($uid)
    {
        $select = $this->_db->select()->from($this->_name)->where('uid = ?', $uid);
        $result = $this->getAdapter()->fetchAll($select);
        if (!$result) {
            return false;
        }
        return $result;
    }

    public function getUidName ()
    {
        $sql = "select uid, ulogin from users";
        $result = $this->getAdapter()->fetchAll($sql);
        if (!$result) {
            return false;
        }
        
        foreach ($result as $val) {
            $uids_names[$val['uid']] = $val['ulogin'];
        }
        return $uids_names;
    }

    public function updateUserData ($user_data)
    {
        $sql = "UPDATE users SET
                    ulogin = '".$user_data['ulogin']."',
                    upass = '".$user_data['upass']."',
                    ureg_date = '".$user_data['ureg_date']."',
                    uemail = '".$user_data['uemail']."',
                    usity = '".$user_data['usity']."',                    
                    uhobby = '".$user_data['uhobby']."'
                WHERE uid = '".$user_data['uid']."'";
        $this->_db->query($sql);
    }

    public function selectUsersAvatarsThumbs ()
    {
        $sql = "SELECT uid, avatar, thumbs FROM users
                join avatars using (uid)
                WHERE avatar <> '' and thumbs <> ''
                ORDER BY RANDOM()
                LIMIT 8";
        $result = $this->getAdapter()->fetchAll($sql);        
        if (!empty($result)) {
            return $result;
        }
        return false;
    }

    public function confirmRegistration ($confirm_key)
    {
        $sql = "select uemail, upass from users where confirm_key = '".$confirm_key."'";
        $result = $this->getAdapter()->fetchAll($sql);
        if (!empty($result)) {
            $sql1 = "update users set is_confirm = true where confirm_key = '".$confirm_key."'";
            $this->_db->query($sql1);
            return true;
        }
        return false;
    }

    public function checkConfirm ($uemail,$upass)
    {
        $sql = "select is_confirm from users where uemail = '".$uemail."' and upass = '".md5($upass)."'";
        $result = $this->getAdapter()->fetchAll($sql);        
        if (!empty($result[0]['is_confirm'])) {
            return true;
        }
        return false;
    }
}
?>
