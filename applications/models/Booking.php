<?php
/*
 * Booking methods
 */

class Application_Model_Getcontent {
	public function getChainTypes($parameters);
	public function getChains($parameters); 
	public function getChangedHotels($parameters);
	public function getCities($parameters); 
	public function getCountries($parameters);
	public function getCreditcardTypes($parameters);
	public function getCreditcards($parameters);
	public function getDistricts($parameters);
	public function getDistrictHotels($parameters);
	public function getFacilityTypes($parameters);
	public function getHotelDescriptionPhotos($parameters);
	public function getHotelDescriptionTranslations($parameters);
	public function getHotelDescriptionTypes($parameters);
	public function getHotelFacilities($parameters);
	public function getHotelFacilityTypes($parameters);
	public function getHotelLogoPhotos($parameters);
	public function getHotelPhotos($parameters);
	public function getHotelTranslations($parameters);
	public function getHotelTypes($parameters);
	public function getHotels($parameters);
	public function getRegions($parameters); 
	public function getRegionHotels($parameters);
	public function getRoomFacilities($parameters);
	public function getRoomFacilityTypes($parameters);
	public function getRoomPhotos($parameters);
	public function getRoomTranslations($parameters);
	public function getRoomTypes($parameters);
	public function getRooms($parameters);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
