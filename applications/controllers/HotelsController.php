<?php

/**
 *  @filesource
 *  Класс IndexController
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class HotelsController extends Zend_Controller_Action {

	
	protected $user;
	protected $_options;
	protected $_booking;
	
	
	
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $this->user = $auth->check();
		
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
		
		$this->view->affiliate_id = Zend_Registry::get('config')->booking->affiliate;
	}

	
	
	
	/* Страница списка гостиницы */
	public function indexAction(){
		$hotels_table = new Application_Model_DbTable_Hotels();
		
		$this->view->hotels = $hotels_table->getList();
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');	
	}

	
	
	
	/* Страница описания гостиницы */
	public function descriptionAction() {
		
		// Инициализируем объект гостиниц
		$hotels_table = new Application_Model_DbTable_Hotels();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->hotel_id = $hotel_id = $params['hotel'];
		
		if(!empty($hotel_id)){
			
			// Получаем гостиницу
			$this->view->hotel = $hotels_table->getById($hotel_id);
		}
	}
	
	
	
	
	/* Страница списка номеров гостинцы */
	public function priceAction() {
		
		$params = $this->getRequest()->getParams();
		$this->view->hotel_id = $hotel_id = $params['hotel'];
		
		// Инициализируем объект гостиниц
		$hotels_table = new Application_Model_DbTable_Hotels();

		// Инициализируем объект номеров гостиниц
		$hotelprice_table = new Application_Model_DbTable_Hotelprice();
		
		if(!empty($hotel_id)){
			
			if(!empty($params['travel'])) {
				$this->view->travel_in_url = "&travel=".$params['travel'];
			}
			
			// Получаем гостиницу
			$this->view->hotel = $hotels_table->getById($hotel_id);
			
			// Получаем номера гостиницы
			$this->view->rooms = $hotelprice_table->getByHotelId($hotel_id);		
		}
	}
	
	
	
	
	/* Страница карты гостинцы */
	public function mapAction() {
		
		$params = $this->getRequest()->getParams();
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;
		
		$this->view->hotel_id = $params['hotel'];
		
		if(!empty($params['hotel'])){
		
			// Инициализируем объект гостиниц
			$hotels_table = new Application_Model_DbTable_Hotels();
			
			$this->view->zoom = 15;
			$this->view->map_type = "yandex#map";
			
			// Получаем гостинцы
			$hotel = $hotels_table->getById($params['hotel']);			
			if (!empty($hotel)) {				
				$this->view->hotel = $hotel;
				$this->view->title = $hotel['title'];
				$this->view->lat = !empty($hotel['lat'])?$hotel['lat']:$config->coords->lat;
				$this->view->lng = !empty($hotel['lng'])?$hotel['lng']:$config->coords->lng;
			}
		}
	}
	
	
	
	/*
	 * Форма подачи заявки на участие гостиницы в проекте
	 */
	public function zayavkaAction() {
		
		
		if ($this->_request->isPost()) {
			
			$formData = $this->_request->getPost();
						
			$this->view->empty_orgname = empty($formData['orgname']) ? true : false;
			$this->view->empty_name = empty($formData['name']) ? true : false;
			$this->view->empty_phone = empty($formData['phone']) ? true : false;
			$this->view->empty_email = empty($formData['email']) ? true : false;
			$this->view->bad_email = !empty($formData['email']) && !preg_match("/@/", $formData['email']) ? true : false;
			
			if (!empty($formData['orgname']) && !empty($formData['name']) && !empty($formData['phone']) && !empty($formData['email']) && empty($this->view->bad_email)) {
			
				// Вытаскиваем конфиг из реестра
				$config = Zend_Registry::getInstance()->config;
				
				$data = array(
					'orgname' => $formData['orgname'],
					'name' => $formData['name'],
					'phone' => $formData['phone'],
					'email' => $formData['email']
				);

				$orgs_table = new Application_Model_DbTable_Orgs();
				// Вставляем данные
				$new_org_id = $orgs_table->insert($data);

				// Подключаем объект работы с почтой
				$email_obj = new Application_Model_Email();
				
				// Получаем заголовки
				$headers = $email_obj->getEmailHeaders('manager@way2day.ru', 'plain');
				
				// Подготавливаем сообщение для отправики себе, чтобы знать что ктото добавился
				$message = $email_obj->getMessage('addhotel_adm', $data);
				if ($headers && $message) {
					mail($config->emails->admin.', '.$config->emails->manager, 'Приглашение на way2day.ru', $message, $headers);
				}
			
				// Получаем заголовки
				$headers = $email_obj->getEmailHeaders('manager@way2day.ru', 'html');
				
				// Подготавливаем сообщение пользователю				
				$message = $email_obj->getMessage('addhotel', $data);		
				// Отправляем
				mail($formData['email'], 'Заявка на участие в проекте way2day.ru', $message, $headers);
				
				$this->view->result_message = "Ваша заявка принята. В ближайшее время наш менеджер свяжется с вами.";
				
			} else {
				// Возвращаем параметры, чтобы не вводить заново
				$this->view->orgname = $formData['orgname'];
				$this->view->name = $formData['name'];
				$this->view->phone = $formData['phone'];
				$this->view->email = $formData['email'];				
			}
			
		}
		
	}
	
	
	/*
	 * Страница комнаты
	 */
	public function roomAction() {
		
		$params = $this->getRequest()->getParams();
		
		if(!empty($params['room']) && !empty($params['hotel'])){
			
			// Инициализируем объект номеров гостиниц
			$hotelprice_table = new Application_Model_DbTable_Hotelprice();

			// Инициализируем объект гостиниц
			$hotels_table = new Application_Model_DbTable_Hotels();
			
			$this->view->hotel = $hotels_table->getById($params['hotel']);
			
			$this->view->room_id = $room_id = $params['room'];
			
			$this->view->room = $hotelprice_table->check($params['hotel'], $room_id);
		}
	}
	
	
	
	/*
	 * Страница комнаты
	 */
	public function bookingAction() {
		
		//$this->_redirect('/');
		
		$autoloader = Zend_Loader_Autoloader::getInstance();
		$autoloader->registerNamespace('Booking_');
		
		$this->_options = Zend_Registry::get('config')->booking->toArray();
		$this->_booking = new Booking_Service_Booking($this->_options);		
	}
	
	

}
