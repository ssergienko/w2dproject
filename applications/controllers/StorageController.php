<?php
/**
*  @filesource
*  Класс IndexController для модуля
* 
*  
*  @author Sergey S. Sergienko
*/

if (!class_exists('S3')) require_once 'S3.php';

class StorageController extends Zend_Controller_Action
{  
	
	// @todo вынести в bootstrap
	public function init () {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		// AWS access info
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAIXVQKNLVQCTSRWYA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		
		// Подключаем s3 библиотеку
		$this->s3 = new S3(awsAccessKey, awsSecretKey);		
		
	}
	
	
	
	/*
	 * Получает картинки для места
	 */
	public function getplimagesAction()
    {
		
		$params = $this->getRequest()->getParams();		
		
		if (!empty($params['user_id']) && !empty($params['place_id'])) {
		
			$image_table = new Api_Model_DbTable_Placeimages();
			$res = $image_table->getImagesByPlId($params['place_id']);
			
			$file = array();
			$files = array();
			foreach ($res as $row) {
				
				$file['id'] = $row->id;
				$file['name'] = $row->name;
				$file['files_url'] = $row->url;
				$file['thumbnail_url'] = $row->thumbnail_url;
				$file['middle_url'] = $row->middle_url;
				$file['delete_url'] = $row->delete_url;
				$file['image_txt'] = $row->image_txt;
				$file['idx'] = $row->idx;
				
				$files[] = $file;
				
			}
			
			echo json_encode($files);
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
    }
	
	
	
	/*
	 * Получает картинки для поездки
	 */
	public function gettrimagesAction()
    {
		
		$params = $this->getRequest()->getParams();		
		
		if (!empty($params['user_id']) && !empty($params['travel_id'])) {
		
			$image_table = new Api_Model_DbTable_Travelimages();
			$res = $image_table->getImagesByTrId($params['travel_id']);
			
			$file = array();
			$files = array();
			foreach ($res as $row) {
				
				$file['id'] = $row->id;
				$file['name'] = $row->name;
				$file['files_url'] = $row->url;
				$file['thumbnail_url'] = $row->thumbnail_url;
				$file['middle_url'] = $row->middle_url;
				$file['delete_url'] = $row->delete_url;
				$file['image_txt'] = $row->image_txt;
				$file['idx'] = $row->image_idx;
				
				$files[] = $file;
				
			}
			
			echo json_encode($files);
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
    }
	
	
	
	/*
	 * Получает картинки для города
	 */
	public function getcityimagesAction()
    {
		
		$params = $this->getRequest()->getParams();		
		
		if (!empty($params['city_id'])) {
		
			$image_table = new Api_Model_DbTable_Cityimages();
			
			$select = $image_table->select()->where('cy_id = '.$params['city_id']);
			$res = $image_table->fetchAll($select);
			
			$file = array();
			$files = array();
			foreach ($res as $row) {

                $file['id'] = $row->id;
                $file['name'] = $row->name;
                $file['files_url'] = $row->url;
                $file['thumbnail_url'] = $row->thumbnail_url;
                $file['middle_url'] = $row->middle_url;
                $file['delete_url'] = $row->delete_url;
                $file['image_txt'] = $row->image_txt;
                $file['idx'] = $row->idx;
				
				$files[] = $file;
				
			}
			
			echo json_encode($files);
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
    }
	
	
	
	
	/*
	 * Удаляет записи из tmp_images
	 */
    public function deletetmpAction()
    {
		
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['user_id']) && !empty($params['img_action'])) {
		
			$tmp_table = new Api_Model_DbTable_Tmpimages();
			$tmp_table->delete('user_id = '.$params['user_id'].' and action = \''.$params['img_action']."'");
			
			echo json_encode(array('success' => 'tmp row deleted'));
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
	}
	
	
	
}
