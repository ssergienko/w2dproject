<?php

/**
 *  @filesource
 *  Класс отвечающий за регистрацию
 *  
 * 
 */
class RegController extends Zend_Controller_Action {

	/*
	 * На эту страницу попадаем из письма подтверждения регистрации
	 */
	public function confirmAction() {
		
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['data'])) {
			
			$data = json_decode(base64_decode($params['data']), true);
			
			$users_table = new Application_Model_DbTable_Users();
			$user = $users_table->getById($data['user']);
			
			// Если такой пользователь есть
			if (!empty($user) && $data['email'] == $user['uemail']) {
				
				// Подтверждаем
				$users_table->update(array('confirm' => 1), 'id = '.$user['id']);
				
				// Отправляем письмо для подтверждения
				$mailer = new Application_Model_Email();
				$mailer->sendSaccessConfirmMessage($user['id']);
				
				// Авторизуем
				$auth = new Zend_Session_Namespace('auth');
				$auth->uid = $user['id'];
				
				// Возвращаем где был уже авторизованным
				$this->_redirect($data['redirect_uri']);
			}
		}
		
	}
	
	
	
	
	/*
	 * На эту страницу попадаем если успешно зарегались
	 */
	public function successAction() {
		
		$params = $this->getRequest()->getParams();

		if (!empty($params['uid'])) {
			$users_table = new Application_Model_DbTable_Users();
			$user = $users_table->getById($params['uid']);
			
			$this->view->email = !empty($user['uemail'])?$user['uemail']:'';
		}
		
	}
	
	
	

}

?>
