<?php

class ErrorController extends Zend_Controller_Action
{
	
	public function errorAction()
    {
		//$this->_helper->layout()->setLayout('error');
		
		//var_dump(APPLICATION_ENV);
		Zend_Layout::getMvcInstance()->assign('title', 'Ошибка');
		
        $errors = $this->_getParam('error_handler');
        
        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);

                $priority = Zend_Log::NOTICE;
				$this->view->title =  'Страница не найдена';
                $this->view->message = 'К сожалению, страница, которую вы ищете, не найдена.';
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
				$this->view->title =  'Ошибка приложения';
				$message = 'На сайте произошла ошибка.';

				if($this->getResponse()->getExceptionByCode(999)){
					$message .= '<br/><br/> Причина ошибки: ' . $errors->exception->getMessage();
				}else{
					$message .= ' Приносим свои извинения за доставленные неудобства. Мы уже работаем над устранением неполадки';
				}

                $this->view->message = $message;

                break;
        }
        
        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }
        
        $this->view->request   = $errors->request;
    }
	
}