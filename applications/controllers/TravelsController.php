<?php

/**
 *  @filesource
 *  Класс IndexController
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class TravelsController extends Zend_Controller_Action {

	protected $user;
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $this->user = $auth->check();
		
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
	}
	
	/* Страница списка поездок */
	public function indexAction() {
		
		$travels_table = new Application_Model_DbTable_Travels();
		
		$this->view->travels = $travels = $travels_table->getList();
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');		
	}
	
	/* Страница описания поездки */
	public function descriptionAction() {
		
		$params = $this->getRequest()->getParams();
		
		$this->view->travel_id = $travel_id = $params['travel'];
		
		// Пока не будет реализовано описание поездки
		$redirect = "";
		$city_in_url = "";
		if(!empty($params['city'])){
			$city_in_url = "&city=".$params['city'];
			
			$redirect .= empty($redirect)?'?':'&';
			$redirect .= "city=".$params['city'];			
		}
		$this->view->city_in_url = $city_in_url;
		
		if(!empty($params['user'])) {
			$this->view->user_in_url = $params['user'];
			
			$redirect .= empty($redirect)?'?':'&';
			$redirect .= "user=".$params['user'];			
		}
		
		// Инициализируем объект путешествий
		$travels_table = new Application_Model_DbTable_Travels();
		
		// Подключаем мемкэш
//		$memcache = Zend_Registry::getInstance()->oCache;
		
		if(!empty($travel_id)){
			
			// Проверяем добавлена ли поездка в избранные
			if(!empty($this->user)) {
				$travelFav_table = new Application_Model_DbTable_TravelFav();
				$this->view->travel_fav = $travelFav_table->check($travel_id, $this->user['id']);
				
				$travelLike_table = new Application_Model_DbTable_TravelLike();
				$this->view->travel_like = $travelLike_table->check($travel_id, $this->user['id']);
			}
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];				
			}
			
			//if(!($this->view->travel = $travel = $memcache->load('travel_'.$travel_id.$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$travel = $travels_table->getByIdFromUser($travel_id);				
				} else {
					$travel = $travels_table->getById($travel_id);
				}
				$this->view->travel = $travel;
				//$memcache->save($this->view->travel, 'travel_'.$travel_id.$mem_name);
			//}
			if($travel['author'] == $this->user['id']) {
				$this->view->my_travel = $travel['author'];
			}
			
			$this->view->headTitle("Поездка ".$travel['title'],'PREPEND');
			
			// Получаем изображения и текста для описания
			$images_table = new Application_Model_DbTable_Travelimages();
			$this->view->images = $images_table->getImagesByTrId($travel_id);
			
		}
		else{
			$this->_redirect('');
		}
	}
	
	/* Страница списка мест у поездки */
	public function planAction() {
		
		$params = $this->getRequest()->getParams();

		// Инициализируем объект путешествий
		$travels_table = new Application_Model_DbTable_Travels();
		// Инициализируем объект путешествий
		$travelplace_table = new Application_Model_DbTable_Travelplace();
		
		$this->view->travel_id = $travel_id = $params['travel'];
		
		$city_in_url = "";
		if(!empty($params['city'])){
			$city_in_url = "&city=".$params['city'];
		}
		$this->view->city_in_url = $city_in_url;
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		if(!empty($travel_id)){
			// Проверяем добавлена ли поездка в избранные
			if(!empty($this->user)) {
				$travelFav_table = new Application_Model_DbTable_TravelFav();
				$this->view->travel_fav = $travelFav_table->check($travel_id, $this->user['id']);
				
				$travelLike_table = new Application_Model_DbTable_TravelLike();
				$this->view->travel_like = $travelLike_table->check($travel_id, $this->user['id']);
			}
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];
				$this->view->my_page = 1;
			}
			
			//if(!($this->view->travel = $travel = $memcache->load('travel_'.$travel_id."_plan".$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$travel = $travels_table->getByIdFromUser($travel_id);				
				} else {
					$travel = $travels_table->getById($travel_id);
				}
				if(!empty($travel['places'])) {
					$travel['places'] = $travelplace_table->groupByDate($travel_id, $travel['places']);
				}
				//$memcache->save($travel, 'travel_'.$travel_id."_plan".$mem_name);
				$this->view->travel = $travel;
			//}
				
			if($travel['author'] == $this->user['id']) {
				$this->view->my_travel = $travel['author'];
			}
			
			$this->view->headTitle("Что посмотреть в поездке ".$travel['title'],'PREPEND');
			
		}
		else{
			$this->_redirect('');
		}
	}
	
	/* Страница карты поездки */
	public function hotelsAction() {
		
		$params = $this->getRequest()->getParams();

		$this->view->travel_id = $travel_id = $params['travel'];
		
		$city_in_url = "";
		if(!empty($params['city'])){
			$city_in_url = "&city=".$params['city'];
		}
		$this->view->city_in_url = $city_in_url;
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект путешествий
		$travels_table = new Application_Model_DbTable_Travels();
				
		if( !empty($travel_id)){	
			// Проверяем добавлена ли поездка в избранные
			if(!empty($this->user)) {
				$travelFav_table = new Application_Model_DbTable_TravelFav();
				$this->view->travel_fav = $travelFav_table->check($travel_id, $this->user['id']);
				
				$travelLike_table = new Application_Model_DbTable_TravelLike();
				$this->view->travel_like = $travelLike_table->check($travel_id, $this->user['id']);
			}
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];
			}
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->travel = $travel = $memcache->load('travel_'.$travel_id."_hotels".$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$travel = $travels_table->getByIdFromUser($travel_id);				
				} else {
					$travel = $travels_table->getById($travel_id);
				}
				//$memcache->save($travel, 'travel_'.$travel_id."_hotels".$mem_name);
				$this->view->travel = $travel;
			//}		
			if($travel['author'] == $this->user['id']) {
				$this->view->my_travel = $travel['author'];
			}
			$this->view->headTitle("Город ".$travel['title'] ,'PREPEND');			
		}
		else{
			$this->_redirect('');
		}
	}
	
	/* Страница карты поездки */
	public function mapAction() {
		
		$params = $this->getRequest()->getParams();

		$this->view->travel_id = $travel_id = $params['travel'];
		
		$city_in_url = "";
		if(!empty($params['city'])){
			$city_in_url = "&city=".$params['city'];
		}
		$this->view->city_in_url = $city_in_url;
		
		// Инициализируем объект путешествий
		$travels_table = new Application_Model_DbTable_Travels();
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Подключаем мемкэш
		$config = Zend_Registry::getInstance()->config;
		
		if(!empty($travel_id)) {
			
			$this->view->zoom = 10;
			$this->view->map_type = "yandex#map";
			
			// Проверяем добавлена ли поездка в избранные
			if(!empty($this->user)) {
				$travelFav_table = new Application_Model_DbTable_TravelFav();
				$this->view->travel_fav = $travelFav_table->check($travel_id, $this->user['id']);
				
				$travelLike_table = new Application_Model_DbTable_TravelLike();
				$this->view->travel_like = $travelLike_table->check($travel_id, $this->user['id']);
			}
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];
			}
			
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->travel = $travel = $memcache->load('travel_'.$travel_id."_map".$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$travel = $travels_table->getByIdFromUser($travel_id);				
				} else {
					$travel = $travels_table->getById($travel_id);
				}
				$this->view->travel = $travel;
				//$memcache->save($travel, 'travel_'.$travel_id."_map".$mem_name);
			//}
			if($travel['author'] == $this->user['id']) {
				$this->view->my_travel = $travel['author'];
			}
			$this->view->headTitle("Как добраться до мест в поездке ".$travel['title'],'PREPEND');
			
			// Получаем координаты для отображения мест на карте
			$coords = array();
			foreach ($travel['places'] as $place) {
				if (!empty($place['lat']) && !empty($place['lng'])) {
					$coords[] = '['.$place['lat'].', '.$place['lng'].']';
					$coords2[] = array($place['lat'], $place['lng']);
				}
			}
			
			$this->view->lat = !empty($coords2[0][0])?$coords2[0][0]:$config->coord->lat;
			$this->view->lng = !empty($coords2[0][1])?$coords2[0][1]:$config->coord->lng;
			
			$this->view->coords = !empty($coords) ? '['.implode(", ", $coords[0]).']' : false;
			
		}
		else{
			$this->_redirect('');
		}
	}
	
	/* Страница добавления поездки */
	public function addAction() {
		
		if(!empty($this->user['id'])) {
			
			$this->view->user_id = $this->user['id'];
			
			$params = $this->getRequest()->getParams();
			
			// Как только входим
			if (!$this->_request->isPost()) {
				
				$travels_table = new Application_Model_DbTable_Travels();
				// Получаем черновик
				$draft = $travels_table->getDraft($this->user['id']);
				// Если уже есть черновик (начинал создавать поездку)
				if (!empty($draft)) {
					
					// Имя					
					$this->view->title = (!empty($draft->title)) ? $draft->title : '';
					// Описание
					$this->view->description = (!empty($draft->description)) ? $draft->description : '';
					// Id
					$this->view->travel_id = $draft->id;
				} else {
					// Иначе сразу создаем поездку
					$this->view->travel_id = $travel_id = $travels_table->insert(array('confirm' => 0, 'author' => $this->user['id']));
				}
				
			} else {
				
				// Инициализируем объект мест
				$travels_table = new Application_Model_DbTable_Travels();
				
				// travel_id будет в параметрах только если нажали кнопку сохранить
				$this->view->travel_id = $travel_id = (!empty($params['travel_id'])) ? $params['travel_id'] : false;
				
				$error = array();
				
				// Проверяем имя
				if(empty($params['title'])) {
					$error['title'] = "Необходимо ввести название поездки";
				} else {
					$this->view->title = $params['title'];
				}
				
				if (!empty($params['long_text'])) {
					$this->view->long_text = $params['long_text'];
				}
				
				if(empty($error)) {
					
					$data = array(
						'title' => $params['title'],
						'long_text' => !empty($params['long_text']) ? $params['long_text'] : '',
						'author' => $this->user['id'],
						'owner' => $this->user['id']
					);
					
					// Сохраняем данные
					$travels_table->update($data, 'id = '.$travel_id);
					
					// Если все ок, подтверждаем сохранение
					if (!empty($params['title'])) {
						$travels_table->update(array('confirm' => 1), 'id = '.$travel_id);
						$this->_redirect('/travel/'.$travel_id.'?user='.$this->user['id']);
					}
					
				}
				
				if(!empty($error)) {
					$this->view->error = $error;
				}				
			}
			
		} else {
			
			// TODO переводить на страницу авторизации, после авторизации возвращать сюда
			$this->_redirect('');
		}
		
	}	
	
	
	
	/*
	 * Удаление места из поездки
	 */
	public function deleteplaceAction () {
		
		$params = $this->getRequest()->getParams();
		
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		// Проверяем авторозацию
		if (!empty($user['id']) && $user['id'] === $params['user']) {
			
			if (!empty($params['travel_id']) && !empty($params['place_id']) && !empty($params['user_id'])) {
				$travelplace_table = new Application_Model_DbTable_Travelplace();
				$travelplace_table->delete("t_id = ".$params['travel_id']." and p_id = ".$params['place_id']);

				$this->_redirect('/travel/'.$params['travel_id'].'/plan?user='.$params['user_id']);
			}
			
		}

		// TODO перенаправлять на страницу с описанием ошибки
		$this->_redirect('');
		
	}
	
	
	
	/*
	 * Удаление путешествия
	 */
	public function deleteAction () {
		
		$params = $this->getRequest()->getParams();
		
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		// Проверяем авторозацию
		if (!empty($user['id']) && $user['id'] === $params['user']) {
		
			if (!empty($params['id'])) {			
				$travels_table = new Application_Model_DbTable_Travels();
				$travels_table->delete("id = ".$params['id']);

				$this->_redirect('/user/'.$params['user'].'/travels');

			}
		}
		
		$this->_redirect('');
		
	}
	
	
	
	/*
	 * Редактирование лписания поездки
	 */
	public function editstoryAction() {
		
		$params = $this->getRequest()->getParams();
		
		$this->view->active = 'story';
		
		if(!empty($this->user['id'])) {
			
			$this->view->user_id = $this->user['id'];
			$this->view->travel_id = $travel_id = $params['travel_id'];
			
			$travels_table = new Application_Model_DbTable_Travels();
			$this->view->travel = $travels_table->getByIdForEdit($params['travel_id']);
			
		}
		
	}
	
	
	
	/*
	 * Редактирование лписания поездки
	 */
	public function editplanAction() {
		
		$params = $this->getRequest()->getParams();
		
		$this->view->active = 'plan';
		
		if(!empty($this->user['id'])) {
			
			$this->view->user_id = $this->user['id'];
			$this->view->travel_id = $travel_id = $params['travel_id'];		
			
			$travels_table = new Application_Model_DbTable_Travels();
			$travelplace_table = new Application_Model_DbTable_Travelplace();
			
			$this->view->travel = $travel = $travels_table->getByIdForEdit($params['travel_id']);

			if($travel['author'] == $this->user['id']) {
				
				$this->view->user_in_url = "&user=".$this->user['id'];
				
				if(!empty($travel['places'])) {
					$travel['places'] = $travelplace_table->groupByDate($travel_id, $travel['places']);
				}
				
				$this->view->travel = $travel;
			}
			
		}
		
	}
	
	
	
	/*
	 * Редактирование поездки
	 */
	public function editAction() {
		
		$params = $this->getRequest()->getParams();
		
		if(!empty($this->user['id'])) {
			
			$this->view->user_id = $this->user['id'];
			$this->view->travel_id = $travel_id = $params['travel'];
			
			$travels_table = new Application_Model_DbTable_Travels();
			$travelplace_table = new Application_Model_DbTable_Travelplace();
			
			$this->view->travel = $travel = $travels_table->getByIdForEdit($travel_id);
			
			if ($this->_request->isPost()) {
				// Инициализируем объект мест
				
				$success = array();
				$error = array();
				if(empty($params['title'])) {
					$error['title'] = "Введите название поездки";
				}
				
				if(empty($error)) {
					
					$data = array();
					
					if($params['title'] != $travel['title']) {
						$data['title'] = $params['title'];						
					}
					if($params['long_text'] != $travel['long_text']) {
						$data['long_text'] = $params['long_text'];
					}
					
					if(!empty($data)) {
						$result = $travels_table->edit($travel_id, $data);

						if(!empty($result)) {
							foreach($data as $key=>$value) {
								$success[$key] = "Данные успешно обновлены";
								$travel[$key] = $value;
							}
						}
					}
				}
				
				if(!empty($error)) {
					$this->view->error = $error;
				}
				if(!empty($success)) {
					$this->view->success = $success;
				}
				
				$this->_redirect('travel/'.$travel_id);
				
			}
		}
	}
	
	
	
}
