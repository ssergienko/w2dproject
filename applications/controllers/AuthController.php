<?php

/**
 *  @filesource
 *  Класс AuthController выполняющий аутентификацию
 *   
 *  Этот класс запускается в том случае если пользователь не авторизован, то есть 
 *  Zend_Auth::getInstance()->getIdentity() = false 
 *  это проверяется в файле IndexController.php в indexAction
 * 
 */
class AuthController extends Zend_Controller_Action {

	protected $session;
	protected $user;

	public function init() {

	}

	public function indexAction() {
		$this->_redirect('auth/login');
	}
	
	public function vkAction() {

		$auth = new Zend_Session_Namespace('auth');
		
		$params = $this->getRequest()->getParams();
		if (!empty($params['w2d_redirect_uri'])) {
			$auth->w2d_redirect_uri = $params['w2d_redirect_uri'];
		}

		// Если ошибка придет - error=access_denied&error_reason=user_denied&error_description=User+denied+your+request
		$error = $this->_getParam('error');
		if (!empty($error)) {
			throw new Exception($error);
			die();
		}

		$config = Zend_Registry::getInstance()->config;
		
		// Формирование данных для запроса
		$client_id = $config->vk->app_id;
		$redirect_uri = $config->vk->redirect_uri;
		$client_secret = $config->vk->secret_key;
		
		$code = $this->_getParam('code');
		
		// Если не залогинен
		if (empty($code) && empty($auth->uid)) {
			// Формируем урл для получения кода
			$dialog_url = "http://oauth.vk.com/authorize";
			$dialog_url .= "?client_id=".$client_id;
			$dialog_url .= "&redirect_uri=" . $redirect_uri;
			$dialog_url .= "&scope=friends";
			$dialog_url .= "&display=popup";
			$dialog_url .= "&response_type=code";
			// Окно авторизации
			echo("<script type=\"text/javascript\"> top.location.href='" . $dialog_url . "'</script>");
		} else {
			
			// Получаем токен
			$token_url = "https://oauth.vk.com/access_token";
			$token_url .= "?client_id=".$client_id;
			$token_url .= "&client_secret=".$client_secret;
			$token_url .= "&code=".$code;
			$token_url .= "&redirect_uri=".$redirect_uri;
			
			$token = json_decode(file_get_contents($token_url));
			
			if (!empty($token)) {
				// Получаем имя пользователя
				$url = 'https://api.vk.com/method/users.get?uids='.$token->user_id.'&fields=uid,photo,screen_name,sex,bdate,city&access_token='.$token->access_token;
				$user_data = json_decode(file_get_contents($url));
			}
			
			// Проверяем, есть ли пользователь с таким id в наших зарегистрированных
			$users_table = new Application_Model_DbTable_Users();
			$user = $users_table->getVkSocialId($user_data->response[0]->uid);

            ////// data from vk //////
            // object(stdClass)#125 (1) { ["response"]=> array(1) { [0]=> object(stdClass)#137 (8) { ["uid"]=> int(982186) ["first_name"]=> string(12) "Сергей" ["last_name"]=> string(18) "Сергиенко" ["sex"]=> int(2) ["screen_name"]=> string(11) "sssergienko" ["bdate"]=> string(10) "27.10.1983" ["city"]=> int(2) ["photo"]=> string(53) "http://cs310822.vk.me/v310822186/2fa6/PwV4sts8Z4k.jpg" } } }

			$auth->social_id = $user_data->response[0]->uid;
			$auth->provider = 'vk';
			$auth->uname = $user_data->response[0]->first_name.' '.$user_data->response[0]->last_name;
			$auth->photo = $user_data->response[0]->photo;
			
			// Если такой пользователь есть, перенаправляем на страницу где он был
			if (!empty($user)) {
				$auth->uid = $user['id'];
                // Save uid in cookie for auth
                setcookie("w2duid", $user['id'], strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);
				// Получаем страницу на которой находился пользователь перед началом авторизации
				$w2d_user_callback_url = !empty($auth->w2d_redirect_uri)?$auth->w2d_redirect_uri:'/';
				// Перенаправляем на логин
				$this->_redirect($w2d_user_callback_url);
			}
			
			// Если нет такого пользователя перенаправляем на регистрацию
			$this->_redirect('auth/signin');
		}
	}

	
	
	public function fbAction() {

		// Отключаем отображение
		$this->_helper->viewRenderer->setNoRender(true);
		
		$auth = new Zend_Session_Namespace('auth');
		$auth->provider = 'fb';
		
		$params = $this->getRequest()->getParams();
		if (!empty($params['w2d_redirect_uri'])) {
			$auth->w2d_redirect_uri = $params['w2d_redirect_uri'];
		}
		
		// Если ошибка придет - error=access_denied&error_reason=user_denied&error_description=User+denied+your+request		
		if (!empty($params['error'])) {
			throw new Exception($params['error']);
		}

		$config = Zend_Registry::getInstance()->config;
		
		// Формирование данных для запроса
		$client_id = $config->fb->app_id;
		$redirect_uri = $config->fb->redirect_uri;
		$client_secret = $config->fb->secret_key;
		
		// Если все ок придет code
		$code = !empty($params['code'])?$params['code']:'';

        if (empty($code)) {
			
			$_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
			
			$dialog_url = "https://www.facebook.com/dialog/oauth";
			$dialog_url .= "?client_id=".$client_id;
			$dialog_url .= "&redirect_uri=".$redirect_uri;
			$dialog_url .= "&state=".$_SESSION['state'];

			echo("<script> top.location.href='" . $dialog_url . "'</script>");
		}

        //var_dump($_REQUEST['state'], $_SESSION['state'], isset($_REQUEST['code'])); die();

        if (isset($_REQUEST['code'])) {

            if ($_SESSION['state'] !== null && isset($_REQUEST['state']) && $_REQUEST['state'] == $_SESSION['state']) {

                $token_url = "https://graph.facebook.com/oauth/access_token";
                $token_url .= "?client_id=".$client_id;
                $token_url .= "&redirect_uri=".$redirect_uri;
                $token_url .= "&client_secret=".$client_secret;
                $token_url .= "&code=".$code;

                $response = file_get_contents($token_url);

                $params = null;
                parse_str($response, $params);

                $graph_url = "https://graph.facebook.com/me";
                $graph_url .= "?access_token=" . $params['access_token'];

                // Получаем данные пользователя
                $user_data = json_decode(file_get_contents($graph_url));

                /////// data from fb /////////
                //object(stdClass)#124 (10) { ["id"]=> string(15) "100007897650053" ["name"]=> string(57) "Donna Amghigfekec Martinazzimansensteinskysonwitzescuberg" ["first_name"]=> string(5) "Donna" ["middle_name"]=> string(11) "Amghigfekec" ["last_name"]=> string(39) "Martinazzimansensteinskysonwitzescuberg" ["link"]=> string(55) "https://www.facebook.com/profile.php?id=100007897650053" ["gender"]=> string(6) "female" ["timezone"]=> int(0) ["locale"]=> string(5) "ru_RU" ["updated_time"]=> string(24) "2014-02-25T15:15:01+0000" }

                $auth->social_id = !empty($user_data->id) ? $user_data->id : '';
                $auth->uname = !empty($user_data->name) ? $user_data->name : '';
                $auth->uemail = !empty($user_data->email) ? $user_data->email : '';
                $auth->photo = !empty($user_data->photo) ? $user_data->photo : '';

                // Проверяем, есть ли пользователь с таким id в наших зарегистрированных
                $users_table = new Application_Model_DbTable_Users();
                $user = $users_table->getFbSocialId($user_data->id);

                //$auth->access_token = $user_data->access_token;
                $social_id = $user_data->id;

                // Если такой пользователь есть
                if (!empty($user)) {
                    $auth->uid = $user['id'];
                    // Save uid in cookie for auth
                    setcookie("w2duid", $user['id'], strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);
                    // Получаем страницу на которой находился пользователь перед началом авторизации и редиректим
                    $this->_redirect(!empty($auth->w2d_redirect_uri) ? $auth->w2d_redirect_uri :'/');
                }

                // Смотрим нет ли пользователя с таким email
                $user = $users_table->getByEmail($user_data->email);

                // Если такого мыла нет
                if (empty($user)) {
                    // Если удалось получить email
                    if (!empty($user_data->email)) {
                        // Записываем в базу нового пользователя
                        $options = array(
                            'uname' => $user_data->name,
                            'uemail' => $user_data->email,
                            'user_image' => 'https://graph.facebook.com/'.$user_data->username.'/picture',
                            'fb_id' => $social_id
                        );
                        $user_id = $users_table->insert($options);
                   } else {
                        // Если нет такого пользователя и не удалось получить email из соцсети, перенаправляем на регистрацию
                        $this->_redirect('auth/signin');
                    }
                } else {
                    // Обновляем данные
                    $options = array(
                        'uname' => $user_data->name,
                        'fb_id' => $social_id
                    );
                    // Если не установлена картинка, устанавливаем
                    if (empty($user->user_image)) {
                        $options['user_image'] = 'https://graph.facebook.com/'.$user_data->username.'/picture';
                    }

                    $users_table->update($options, 'id = '.$user->id);
                    $user_id = $user->id;
                }

                // Запоминаем пользователя
                $auth->uid = $user_id;

                // Save uid in cookie for auth
                setcookie("w2duid", $user['id'], strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);

                // Получаем страницу на которой находился пользователь перед началом авторизации и редиректим
                $this->_redirect(!empty($auth->w2d_redirect_uri) ? $auth->w2d_redirect_uri :'/');

            } else {
                echo("The state does not match. You may be a victim of CSRF.");
            }
        }
	}
	
	public function logoutAction() {

		// Очищаем
		$auth = new Zend_Session_Namespace('auth');
		
		$auth->access_token = '';
		$auth->social_id = '';
		$auth->uid = '';
		$auth->provider = '';
		$auth->w2d_redirect_uri = '';
		$auth->uname = '';

        // Unset uid in cookie for auth
        setcookie("w2duid", "", strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);

		$this->_redirect('/');
	}

	/*
	 * Регистрация нового пользователя	 
	 */
	public function signinAction() {

		$auth = new Zend_Session_Namespace('auth');
		$social_id = $auth->social_id;

		if(empty($social_id)) {
			$this->_redirect('/');
		}
		
		$params = $this->getRequest()->getParams();
		
		$this->view->uname = !empty($auth->uname) ? $auth->uname:'';
		$this->view->photo = !empty($auth->photo) ? $auth->photo:'';
        $this->view->uemail = !empty($auth->uemail) ? $auth->uemail:'';
		
		if(!empty($params['submit'])) {
			
			$this->view->uemail = $params['uemail'];
			$this->view->photo = $params['photo'];
			
			$error = array();
			// Проверка имени
			if(empty($params['uname'])) {
				$error['uname'] = "Укажите имя";
			}
			// Проверка почты
			if(empty($params['uemail'])) {
				$error['uemail'] = "Укажите email";
			} elseif(!preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i',$params['uemail'])) {
				$error['uemail'] = "Что то не так с email";
			}
			
			if(!empty($error)) {
				$this->view->error = $error;
			} else {
				
				$users_table = new Application_Model_DbTable_Users();
				// Смотрим нет ли пользователя с таким email
				$user = $users_table->getByEmail($params['uemail']);
				
				if ($auth->provider == 'vk') {
					$soc_field = 'vk_id';
				} elseif ($auth->provider == 'fb') {
					$soc_field = 'fb_id';
				}
				
				// Если такого мыла нет
				if (empty($user)) {
					// Записываем в базу нового пользователя
					$options = array(
						'uname' => $params['uname'],
						'uemail' => $params['uemail'],
						'user_image' => $params['photo'],
						$soc_field => $social_id
					);
					$user_id = $users_table->insert($options);
				} else {
					// Обновляем данные для пользователя
					$options = array(
						'uname' => $params['uname'],
						$soc_field => $social_id
					);
					// Если нет аватарки, устанавливаем
					if (empty($user->user_image)) {
						$options['user_image'] = $params['photo'];
					}
					$users_table->update($options, 'id = '.$user->id);
					$user_id = $user->id;
				}

				// Запоминаем пользователя
				$auth->uid = $user_id;
				$auth->social_id = $social_id;

                // Save uid in cookie for auth
                setcookie("w2duid", $user_id, strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);

				// Получаем страницу на которой находился пользователь перед началом авторизации
                if(!empty($params['w2d_redirect_uri'])) {
                    $w2d_user_callback_url = $params['w2d_redirect_uri'];
                } else {
                    $w2d_user_callback_url = !empty($auth->w2d_redirect_uri)?$auth->w2d_redirect_uri:'/';
                }

				// Перенаправляем на логин
				$this->_redirect($w2d_user_callback_url);
			}
		}
		
	}
	
	
	
	/*
	 * Регистрация нового пользователя родная
	 */
	public function signinativeAction() {

		$params = $this->getRequest()->getParams();
		
		if(!empty($params['submit'])) {
			
			$this->view->uname = $params['uname'];
			$this->view->uemail = $params['uemail'];
			$this->view->pass1 = $params['pass1'];
			$this->view->redirect_uri = $params['redirect_uri'];
			
			$error = array();
			// Проверка имени
			if(empty($params['uname'])) {
				$error['uname'] = "Укажите имя";
			}
			// Проверка почты
			if(empty($params['uemail'])) {
				$error['uemail'] = "Укажите email";
			} elseif(!preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i',$params['uemail'])) {
				$error['uemail'] = "Что то не так с email";
			}

			// Проверка пароля
			if (empty($params['pass1'])) {
				$error['pass'] = "Укажите пароль";
			} elseif (!preg_match('/^[a-z0-9_\-\[\]\<\>]*$/i',$params['pass1'])) {
				$error['pass'] = "Недопустимые символы в пароле";
			} elseif (strlen($params['pass1']) > 20) {
				$error['pass'] = "Слишком длинный пароль";
			} elseif (strlen($params['pass1']) < 3) {
				$error['pass'] = "Слишком короткий пароль";
			}
			
			if(!empty($error)) {
				$this->view->error = $error;
			} else {
				
				$users_table = new Application_Model_DbTable_Users();
				
				// Проверяем, если уже зарегистрирован такой email
				$alreadyreg_user = $users_table->getByEmail($params['uemail']);
				// Если есть
				if (!empty($alreadyreg_user)) {
				
					// Запоминаем новые данные для существующего пользователя
					$options = array(
						'uname' => $params['uname'],
						'pass' => $params['pass1'],
						'redirect_uri' => $params['redirect_uri']
					);
					$users_table->update($options, 'uemail = \''.$params['uemail'].'\'');
					
					$user_id = $alreadyreg_user->id;
				}
				// Иначе вставляем нового пользователя
				else {
					
					$options = array(
						'uname' => $params['uname'],
						'uemail' => $params['uemail'],
						'pass' => $params['pass1'],
						'redirect_uri' => $params['redirect_uri']
					);
					// Записываем в базу нового пользователя
					$user_id = $users_table->insert($options);
					
				}

				if (!empty($user_id)) {

                    $auth = new Zend_Session_Namespace('auth');
                    $auth->uid = $user_id;

                    // Save uid in cookie for auth
                    setcookie("w2duid", $user_id, strtotime('+30 days'), '/', $_SERVER['HTTP_HOST']);

                    // Отправляем письмо для подтверждения
                    $mailer = new Application_Model_Email();
                    $mailer->sendRegConfirmMessage($user_id);

                    // Получаем страницу на которой находился пользователь перед началом авторизации
                    if(!empty($params['redirect_uri'])) {
                        $w2d_user_callback_url = $params['redirect_uri'];
                    } else {
                        $w2d_user_callback_url = !empty($auth->w2d_redirect_uri)?$auth->w2d_redirect_uri:'/';
                    }

                    // Перенаправляем на логин
                    $this->_redirect($w2d_user_callback_url);
					
				}
				
			}
		}
	}


    /*
     * Login by id
     * */
	public function loginbyidAction () {
        $params = $this->getRequest()->getParams();
        $auth = new Zend_Session_Namespace('auth');
        if (empty($auth->uid)) {
            if (!empty($params['userid'])) {
                $auth->uid = $params['userid'];
                // Перенаправляем на логин
                $this->_redirect('/');
            }
        }
    }
	

}

?>
