<?php

/**
 *  @filesource
 *  Класс IndexController
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class CityController extends Zend_Controller_Action {

	protected $user;
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $this->user = $auth->check();	
		
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
	}
	
	/*
	 * Список городов
	 */
	public function indexAction() {
		$citys_table = new Application_Model_DbTable_City();
		
		$this->view->city = $citys_table->getList();
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');
	}

	/*
	 * Описание города
	 */
	public function descriptionAction() {

		// Получаем параметры
		$params = $this->getRequest()->getParams();
		
		$this->view->city_id = $city_id = $params['city'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();
		
		if(!empty($city_id)){
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->city = $memcache->load('city_'.$city_id))) {
				$this->view->city = $city = $city_table->getById($city_id);
				
				// Инициализируем объект городов
				$city_images_table = new Application_Model_DbTable_Cityimages();
				$this->view->city_images = $city_images_table->getImagesByCyId($city_id);
				
			//	$memcache->save($this->view->city, 'city_'.$city_id);
			//}

            // Проверяем является ли пользователь автором
            if($city['author'] == $this->user['id']) {
                $this->view->my_city = $city['author'];
            }

			$this->view->headTitle($city['title'] ,'PREPEND');
			
			$this->view->large_images = $city_images_table->getLargeImages($city_id);
		}
		else{
			$this->_redirect('');
		}		
	}
	/*
	 * Места города
	 */
	public function placesAction() {

		$params = $this->getRequest()->getParams();
		
		$this->view->city_id = $city_id = $params['city'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();		
		
		if(!empty($city_id)){			
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->city = $memcache->load('city_'.$city_id))) {
				$this->view->city = $city = $city_table->getById($city_id);
				//$memcache->save($this->view->city, 'city_'.$city_id);
			//}
			$this->view->headTitle("Что посмотреть ".$city['titleplaces'] ,'PREPEND');			
		}
		else{
			$this->_redirect('');
		}		
	}
	/*
	 * Поездки города
	 */
	public function travelsAction() {

		$params = $this->getRequest()->getParams();
		
		$this->view->city_id = $city_id = $params['city'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();
		
		if(!empty($city_id)){			
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->city = $memcache->load('city_'.$city_id))) {
				$this->view->city = $city = $city_table->getById($city_id);
				//$memcache->save($this->view->city, 'city_'.$city_id);
			//}
			$this->view->headTitle("Поехать в ".$city['title']);
		}
		else{
			$this->_redirect('');
		}
		
	}
	/*
	 * Гостиницы города
	 */
	public function hotelsAction() {

		$params = $this->getRequest()->getParams();
		
		$this->view->city_id = $city_id = $params['city'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();
		
		if(!empty($city_id)){			
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->city = $memcache->load('city_'.$city_id))) {
				$this->view->city = $city = $city_table->getById($city_id);
				//$memcache->save($this->view->city, 'city_'.$city_id);
			//}			
			$this->view->headTitle("Где остановиться ".$city['titlestay'] ,'PREPEND');
		}
		else{
			$this->_redirect('');
		}
		
	}
	/*
	 * Карта города
	 */
	public function mapAction() {

		$params = $this->getRequest()->getParams();
		
		$this->view->city_id = $city_id = $params['city'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();
		
		if(!empty($city_id)){	
			
			$this->view->zoom = 12;
			$this->view->map_type = "yandex#map";
			
			// Получаем всю возможную информацию по всем вкладкам
			//if(!($this->view->city = $memcache->load('city_'.$city_id))) {
				$this->view->city = $city = $city_table->getById($city_id);
				//$memcache->save($this->view->city, 'city_'.$city_id);
			//}
			$this->view->headTitle("Как добраться ".$city['titleway'] ,'PREPEND');
			
			// Формируем координаты полученных мест			
			$tmp_arr = array();
			
			foreach ($city['places'] as $place) {			
				$tmp_arr[] = $place['id'];
			}
			$this->view->places_ids = '['.implode(",", $tmp_arr).']';
			
			$this->view->lat = !empty($city['lat']) ? $city['lat'] : $config->coord->lat;
			$this->view->lng = !empty($city['lng']) ? $city['lng'] : $config->coord->lng;
			
		}
		else{
			$this->_redirect('');
		}
		
	}


    /*
	 * Страница добавления места
	 *
	 *
	 *
	 */
    public function addAction() {

        if(!empty($this->user['id'])) {

            // Вытаскиваем конфиг из реестра
            $config = Zend_Registry::getInstance()->config;

            $this->view->user_id = $this->user['id'];
            $this->view->zoom = 10;
            $this->view->map_type = "yandex#map";

            $params = $this->getRequest()->getParams();

            $cities_table = new Application_Model_DbTable_City();

            // Как только входим
            if (!$this->_request->isPost()) {
                // Получаем черновик
                $draft = $cities_table->getDraft($this->user['id']);

                // Если уже есть черновик (начинал создавать место)
                if (!empty($draft)) {
                    // Город
                    $this->view->city_title = (!empty($draft->city_title)) ? $draft->city_title : '';
                    // Описание
                    $this->view->description = (!empty($draft->description)) ? $draft->description : '';
                    // Id
                    $this->view->city_id = $draft->id;

                    // ПОлучаем координаты
                    if (!empty($draft->lat) && !empty($draft->lng)) {
                        $this->view->lat = $draft->lat;
                        $this->view->lng = $draft->lng;
                    }
                } else {
                    // Иначе сразу создаем место
                    $this->view->city_id = $city_id = $cities_table->insert(array('confirm' => 0, 'author' => $this->user['id']));
                }

            } else {

                // Получаем координаты
                if (!empty($params['lat']) && !empty($params['lng'])) {
                    $this->view->lat = $params['lat'];
                    $this->view->lng = $params['lng'];
                }

                // place_id будет в параметрах только если нажали кнопку сохранить
                $this->view->city_id = $city_id = (!empty($params['city_id'])) ? $params['city_id'] : false;

                $error = array();

                // Проверяем координаты
                if(empty($params['lat']) || (!is_float($params['lat']) && !is_numeric($params['lat']))) {
                    $error['lat'] = "Необходимо ввести координаты";
                } else {
                    $this->view->lat = $params['lat'];
                }

                // Проверяем координаты
                if(empty($params['lng']) || (!is_float($params['lng']) && !is_numeric($params['lng']))) {
                    $error['lng'] = "Необходимо ввести координаты";
                } else {
                    $this->view->lng = $params['lng'];
                }

                // Проверяем город
                if(empty($params['title'])) {
                    $error['title'] = "Необходимо ввести название населенного пунка у места";
                } else {
                    $this->view->city_title = $params['title'];
                }

                // Проверяем город на повторение
                $city_check_id = $cities_table->getByName($params['title']);

                if(!empty($city_check_id)) {
                    $error['title'] = "Кто то уже добавил этот город. Хотите посмотреть?";
                    $this->view->city_exist = $city_check_id['id'];
                }

                // Проверяем описание
                if(!empty($params['description'])) {
                    $this->view->description = $params['description'];
                }

                if(empty($error)) {

                    // Выбираем Id города по его имени
                    $city = $cities_table->getByName($params['title']);

                    $data = array(
                        'title' => $params['title'],
                        'long_text' => $params['description'],
                        'author' => $this->user['id'],
                        'lat' => $params['lat'],
                        'lng' => $params['lng']
                    );

                    // Если пользователь не выбрал главную картинку, ставим первую загруженную
                    if (empty($params['main_picture'])) {

                        $city_images = new Application_Model_DbTable_Cityimages();
                        $select = $city_images->select()->where('cy_id = (?)', $city_id)->limit(1)->order('id desc');

                        $res = $city_images->fetchRow($select);

                        if (!empty($res->middle_url)) {
                            $data['image_url'] = $res->middle_url;
                        }
                    }

                    $cities_table->update($data, 'id = '.$city_id);

                    if (!empty($params['title']) && $params['title'] != '') {
                        $cities_table->update(array('confirm' => 1), 'id = '.$city_id);
                    }

                    $this->_redirect('/city/'.$city_id.'?user='.$this->user['id']);

                }

                if(!empty($error)) {
                    $this->view->error = $error;
                }
            }

        } else {
            // TODO переводить на страницу авторизации, после авторизации возвращать сюда
            $this->_redirect('');
        }
    }



    /*
	 * Удаление города
	 */
    public function deleteAction () {

        $this->_helper->viewRenderer->setNoRender(true);

        $params = $this->getRequest()->getParams();

        // Пытаемся получить пользователя
        $auth = new Application_Model_Auth();
        $user = $auth->check();

        // Проверяем авторозацию
        if (!empty($user['id']) && $user['id'] === $params['user']) {

            if (!empty($params['id'])) {
                /////// Удаляем запись о городе //////
                $city_table = new Application_Model_DbTable_City();
                $city_table->delete("id = ".$params['id']);

                ////// Удаляем картинки места из s3 хранилища ///////
                $cityimages_table = new Application_Model_DbTable_Cityimages();
                $images = $cityimages_table->getImagesByCyId($params['id']);

                foreach ($images as $row) {

                    if (!empty($row['img_url'])) {
                        // ПОлучаем имя файла по урлу
                        $slice = preg_split("/\//", $row['img_url']);

                        // Корзины
                        $backets = array('w2d-files', 'w2d-middle', 'w2d-thumbnail');
                        // Удаляем из всех хранилищ
                        foreach ($backets as $backet) {
                            if ($this->s3->deleteObject($backet, baseName($slice[count($slice)]-1))) {
                                echo json_encode(array('success' => 'deleted'));
                            } else {
                                echo json_encode(array('error' => 'not deleted'));
                            }
                        }

                    }
                }

                ///// Удаляем из временной таблицы ///////////
                $cityimages_table->delete("cy_id = ".$params['id']);

                $this->_redirect('/user/'.$params['user'].'/cities');

            }

        }

        $this->_redirect('');

    }


    /*
	 * Редактируем поездку
	 */
    public function editAction () {

        $params = $this->getRequest()->getParams();

        // Вытаскиваем конфиг из реестра
        $config = Zend_Registry::getInstance()->config;

        $this->view->user_id = $this->user['id'];

        if(!empty($params['city'])) {
            $city_table = new Application_Model_DbTable_City();

            if ($this->_request->isPost()) {

                $error = array();
                $success = array();
                // Проверяем имя
                if(empty($params['title'])) {
                    $error['title'] = "Необходимо ввести название города";
                }

                // Проверяем город
                if(empty($params['lat']) || empty($params['lng']) || (!is_float($params['lat']) && !is_numeric($params['lat'])) && (!is_float($params['lng']) && !is_numeric($params['lng']))) {
                    $error['lat'] = "Необходимо ввести долготу";
                    $error['lng'] = "Необходимо ввести широту";
                }

                if(empty($error)) {

                    $city = $city_table->getByIdForEdit($params['city']);

                    $data = array();
                    $data['title'] = ($city['title'] != $params['title']) ? $params['title'] : $city['title'];
                    $data['long_text'] = ($city['long_text'] != $params['long_text']) ? $params['long_text'] : $city['long_text'];
                    $data['lat'] = $this->view->lat = $params['lat'];
                    $data['lng'] = $this->view->lng = $params['lng'];

                    if(!empty($data)) {
                        $result = $city_table->update($data, 'id = ' . $params['city']);
                    }

                }

                if(!empty($error)) {
                    $this->view->error = $error;
                } else {
                    $this->_redirect('/user/'.$this->user['id'].'/cities');
                }
            }

            $city = $city_table->getByIdForEdit($params['city']);

            // ПОлучаем координаты
            if (!empty($city['lat']) && !empty($city['lng'])) {
                $this->view->lat = $city['lat'];
                $this->view->lng = $city['lng'];
            } else {
                $this->view->lat = $config->coord->lat;
                $this->view->lng = $config->coord->lng;
            }

            $this->view->zoom = 10;
            $this->view->map_type = "yandex#map";

            if($city['author'] == $this->user['id']) {
                $this->view->city = $city;
            }
        }
    }


}
