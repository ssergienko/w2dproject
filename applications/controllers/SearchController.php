<?php

/*
 * Класс организующий работу поиска
 * 
 * @author sergey.s.sergienko@gmail.com
 * 
 */
class SearchController extends Zend_Controller_Action {
	
	public $tr = array(
		"А"=>"a","Б"=>"B","В"=>"V","Г"=>"G","Д"=>"D",
		"Е"=>"E","Ё"=>"YO","Ж"=>"ZH","З"=>"Z","И"=>"I","Й"=>"Y",
		"К"=>"K","Л"=>"L","М"=>"M","Н"=>"N","О"=>"O",
		"П"=>"P","Р"=>"R","С"=>"S","Т"=>"T","У"=>"U",
		"Ф"=>"F","Х"=>"H","Ц"=>"C","Ч"=>"CH","Ш"=>"SH",
		"Щ"=>"SH","Ъ"=>"aa","Ы"=>"aa","Ь"=>"ss","Э"=>"dd",
		"Ю"=>"YU","Я"=>"YA",

		"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d",
		"е"=>"e","ё"=>"yo","ж"=>"zh","з"=>"z","и"=>"i","й"=>"y",
		"к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o",
		"п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u",
		"ф"=>"f","х"=>"h","ц"=>"c","ч"=>"ch","ш"=>"sh",
		"щ"=>"sh","ъ"=>"ff","ы"=>"gg","ь"=>"mm","э"=>"ee",
		"ю"=>"yu","я"=>"ya",

		"№"=>"N","."=>"POINT"," "=>""

	);
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $auth->check();
		
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
	}
	
	/*
	 * Поиск по фразе
	 */
	public function indexAction () {
		
		$find_type = '';
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');
		
		$params = $this->getRequest()->getParams();
		
		// TODO сделать чтобы принимался один параметр, а в нем разные значения
		// ппц тупо написано		
		/* Переделать так:
		 * 
		 * передается один параметр - search_type
		 * в условии проверяется что в нем
		 * 
		 */
		if(!empty($params['keyphrase'])){
			$find_type = 'keyphrase';
		}
		if(!empty($params['filter'])){
			$find_type = 'filter';
		}
		
		$this->view->keyphrase = !empty($params['keyphrase']) ? $params['keyphrase'] : '';
		
		if(!empty($find_type)) {
			$this->view->find = $find = $params[$find_type];
			$this->view->find_type = $find_type;
			
			$type = !empty($params['type']) ? $params['type'] : 'city';
			
			// Подрубаем модель
			$search = new Application_Model_Search();
			
			// ПОдрубаем мемкеш
			$memcache = Zend_Registry::getInstance()->oCache;
			
			// Идентификатор для кэша
			$sCacheId = $this->translit($find);			
			// Пытаемся получить результаты поиска из мемкеша
			//if(!($search_result = $memcache->load($sCacheId))) {
				$search_result = $search->doSearch($find, $find_type);
				//$memcache->save($search_result, $sCacheId);
			//}
			
			if(!empty($search_result)){
				
				$this->view->result = $search_result;
				
				$active = "";
				$count = array();
				if(!empty($search_result['city'])){
					if($type == 'city' || empty($active)){
						$active = 'city';
					}
					$count['city'] = count($search_result['city']);
				}
				if(!empty($search_result['place'])){
					if($type == 'place' || empty($active)){
						$active = 'place';
					}
					$count['place'] = count($search_result['place']);
				}
				if(!empty($search_result['travel'])){
					if($type == 'travel' || empty($active)){
						$active = 'travel';
					}
					$count['travel'] = count($search_result['travel']);
				}
				if(!empty($search_result['hotel'])){
					if($type == 'hotel' || empty($active)){
						$active = 'hotel';
					}
					$count['hotel'] = count($search_result['hotel']);
				}
				$this->view->type = $active;
				$this->view->count = $count;
			}			
		}		
	}

	
	
	/**
	 * Делает из строки запроса айдишник для кэша
	 */
	public function translit($cyr_str) {
		
		$str = strtr($cyr_str, $this->tr);
		
		// Удаляем пробельные символы с начала и с конца строки
		$str = trim($str);
		// Убираем все табы
		$str = str_replace("\t", '', $str);
		
		$str = mb_strtolower($str);		
		$str = preg_replace("/-/", '', $str);
		$str = preg_replace("/\\\/", '', $str);
		$str = preg_replace("/\//", '', $str);
		$str = preg_replace("/\+/", '', $str);
		$str = preg_replace("/–/", '', $str);
		$str = preg_replace("/!/", '', $str);
		$str = preg_replace("/@/", '', $str);
		$str = preg_replace("/#/", '', $str);
		$str = preg_replace("/\$/", '', $str);
		$str = preg_replace("/%/", '', $str);
		$str = preg_replace("/\^/", '', $str);
		$str = preg_replace("/\&/", '', $str);
		$str = preg_replace("/\*/", '', $str);
		$str = preg_replace("/\(/", '', $str);
		$str = preg_replace("/\)/", '', $str);
		$str = preg_replace("/\"/", '', $str);
		$str = preg_replace("/№/", '', $str);
		$str = preg_replace("/;/", '', $str);
		$str = preg_replace("/:/", '', $str);
		$str = preg_replace("/\?/", '', $str);
		$str = preg_replace("/\'/", '', $str);
		$str = preg_replace("/\[/", '', $str);
		$str = preg_replace("/\]/", '', $str);
		$str = preg_replace("/_/", '', $str);
		$str = preg_replace("/=/", '', $str);
		$str = preg_replace("/-/", '', $str);
		$str = preg_replace("/,/", '', $str);
		$str = preg_replace("/\./", '', $str);
		$str = preg_replace("/»/", '', $str);
		$str = preg_replace("/«/", '', $str);
		$str = preg_replace("/\|/", '', $str);
		$str = preg_replace("/`/", '', $str);
		$str = preg_replace("/\</", '', $str);
		$str = preg_replace("/\>/", '', $str);
		$str = preg_replace("/\{/", '', $str);
		$str = preg_replace("/\}/", '', $str);
		$str = preg_replace("/$/", '', $str);
		
		$str = trim($str);

		return $str;
	}
	
	
	
}