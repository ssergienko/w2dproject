<?php

/**
 *  @filesource
 *  Класс для всяких системных штук. Например для компиляции js файлов и css файлов
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class SystemController extends Zend_Controller_Action {

	public function init() {
		$remout_ip = $this->getRequest()->getServer('REMOTE_ADDR');
		$actets = preg_split("/\./", $remout_ip);
		$this->subnet = $actets[0].'.'.$actets[1].'.'.$actets[2];
	}
	
	/*
	 * Очистка кеша
	 */
	public function clearcacheAction () {
	    // Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		// Подключаем кэширование
		$oCache = Zend_Registry::getInstance()->oCache;
		// Очищаем кеш
		$oCache->clean();
	}
	
	public function phpinfoAction () {

		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		echo phpinfo();
		
	}
	
	public function compileAction() {
		
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		// Врубаем ошибки
		error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
		
		// Подключаем сжималку
		require_once '../library/Thirdparty/jsmin.php';
		
		// Переключаемся на текущую директорию, чтобы работать с относительными путями
		//chdir(dirname(__FILE__));

//********* Скрипты *********//
		
		$files = array();		
		$files[] = 'js/Api.js';
		$files[] = 'js/Storage.js';
		
		$files[] = 'js/face/face.js';
		$files[] = 'js/face/face.index.js';
		$files[] = 'js/face/face.header.js';

		$files[] = 'js/face/face.city.js';
        $files[] = 'js/face/face.city.edit.js';
		$files[] = 'js/face/face.place.js';
		$files[] = 'js/face/face.place.edit.js';
		
		$files[] = 'js/face/face.search.js';
		$files[] = 'js/face/face.login.js';
		$files[] = 'js/face/face.travel.map.js';
		$files[] = 'js/face/face.hotel.js';
		$files[] = 'js/face/face.user.js';
		$files[] = 'js/face/face.user.places.js';
		$files[] = 'js/face/face.listener.js';
		$files[] = 'js/face/face.place.around.js';
		
		$files[] = 'js/jquery-ui.js';
		$files[] = 'js/bootstrap.js';		
		$files[] = 'js/cookies.js';
		
		$files[] = 'js/jquery-autocomplete/src/jquery.autocomplete.js';

		$files[] = 'js/plugins/w2dmap.js';
		
		// Список файлов, которые нужно упаковать
		$pack = array();
		$pack[] = 'js/Api.js';
		$pack[] = 'js/Storage.js';
		
		$pack[] = 'js/face/face.js';
		$pack[] = 'js/face/face.header.js';
		$pack[] = 'js/face/face.index.js';
		$pack[] = 'js/face/face.city.js';
        $pack[] = 'js/face/face.city.edit.js';
		$pack[] = 'js/face/face.place.js';
		$pack[] = 'js/face/face.place.edit.js';

		$pack[] = 'js/face/face.search.js';
		$pack[] = 'js/face/face.login.js';
		$pack[] = 'js/face/face.travel.map.js';
		$pack[] = 'js/face/face.hotel.js';
		$pack[] = 'js/face/face.user.js';
		$pack[] = 'js/face/face.user.places.js';
		$pack[] = 'js/face/face.listener.js';
		$pack[] = 'js/face/face.place.around.js';
		
		$pack[] = 'js/jquery-ui.js';
		$pack[] = 'js/bootstrap.js';
		$pack[] = 'js/cookies.js';
		
		$pack[] = 'js/jquery-autocomplete/src/jquery.autocomplete.js';

		$pack[] = 'js/plugins/w2dmap.js';
		
		// Массив скриптов
		$contents = array();

		// Перебираем все файлы
		foreach($files as $file) {

			// Читаем содержимое файла
			$content = file_get_contents($file);			
			$content = mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content));			
			
			// Если нужно паковать, пакуем
			if(array_search($file, $pack) !== false) {
				//$content = JSMin::minify($content);
			}

			// Помещаем содержимое файла в общий контейнер
			$contents[] = $content;

		}

		// Объединяем скрипты в один файл
		$content = implode(PHP_EOL, $contents);

		// Сжимаем
        // Сжимаем
        $config = Zend_Registry::getInstance()->config;
        if ($config->scripts->minify == 1) {
		    $content = JSMin::minify($content);
        }

		// Сохраняем файл
		file_put_contents('js/all.js', $content);


// ******** Стили ********* //

		$files = array();
		//$files[] = 'css/main.css';
		// Не менять местами, важен порядок 
		$files[] = 'css/w2dstyle.css';
		$files[] = 'css/responsive.css';
		$files[] = 'css/fileuploader.css';
		$files[] = 'css/main.css';
		$files[] = 'css/jquery-ui.css';
		$files[] = 'js/jquery-autocomplete/src/jquery.autocomplete.css';

		// Массив стилей
		$contents = array();

		// Перебираем все файлы
		foreach($files as $file) {

			// Помещаем содержимое файла в общий контейнер
			$contents[] = file_get_contents($file);

		}

		// Объединяем стили в один файл
		$content = implode(PHP_EOL, $contents);

		// Сжимаем
        // ЛОМАЕТ CSS
        //$content = JSMin::minify($content);

		// Сохраняем файл
		file_put_contents('css/all.css', $content);


		// ** End

		echo 'Success.' . PHP_EOL;
		echo 'js/all.js: ' . ceil(filesize('js/all.js') / 1024) . 'K' . PHP_EOL;
		echo 'css/all.css: ' . ceil(filesize('css/all.css') / 1024) . 'K' . PHP_EOL;
		
		echo 'Готово!';
		
	}
	
	
	
	
	/* 
	 * Честный подсчет рэйтинга (выборка из базы)
	 */
	public function ratingAction(){
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$places = array();		
		$travels = array();
		$city = array();
		
		$placeFav_table = new Application_Model_DbTable_PlaceFav();
		$travelFav_table = new Application_Model_DbTable_TravelFav();		
		$travelplace_table = new Application_Model_DbTable_Travelplace();
		
		$places_table = new Application_Model_DbTable_Places();
		$travels_table = new Application_Model_DbTable_Travels();
		$city_table = new Application_Model_DbTable_City();
		
		// Добавление мест в избранные
		$placeFav = $placeFav_table->getList();		
		if(!empty($placeFav)){
			foreach($placeFav as $fav){
				if(isset($places[$fav['p_id']]))$places[$fav['p_id']]++;
				else $places[$fav['p_id']] = 1;
			}
		}	
		// Добавление поездок в избранные
		$travelFav = $travelFav_table->getList();		
		if(!empty($travelFav)){
			foreach($travelFav as $fav){
				if(isset($travels[$fav['t_id']]))$travels[$fav['t_id']]++;
				else $travels[$fav['t_id']] = 1;
			}
		}		
		// Добавление мест в поездки
		$travelplace = $travelplace_table->getList();
		if(!empty($travelplace)){
			foreach($travelplace as $tp){
				if(!empty($places[$tp['p_id']])){
					$places[$tp['p_id']]++;						
				}				
			}
			foreach($travelplace as $tp){
				if(!empty($places[$tp['p_id']])){
					if(!empty($travels[$tp['t_id']])) $travels[$tp['t_id']] +=	$places[$tp['p_id']];
					else $travels[$tp['t_id']] =	$places[$tp['p_id']];
				}				
			}
		}
		// Рейтинг для городов
		foreach($places as $key=>$value){
			$result = $places_table->getCity($key);
			if(!empty($result)){
				if(!empty($city[$result])) $city[$result] += $value;
				else $city[$result] = $value;
			}
		}
		
		// Обновление рейтинга
		if(!empty($places)){
			foreach($places as $key=>$value){
				$places_table->updateRating($key, $value, 'refresh');
			}
		}
		if(!empty($travels)){
			foreach($travels as $key=>$value){
				$travels_table->updateRating($key, $value, 'refresh');
			}
		}
		if(!empty($city)){
			foreach($city as $key=>$value){
				$city_table->updateRating($key, $value, 'refresh');
			}
		}
		// Очищаем КЭШ
		$memcache = new Memcache;
		$memcache->connect('localhost', 11211) or die ("Could not connect to memcached server");
		
		$allSlabs = $memcache->getExtendedStats('slabs');
		foreach($allSlabs as $server => $slabs){
			foreach($slabs AS $slabId => $slabMeta){
				$cdump = $memcache->getExtendedStats('cachedump',(int)$slabId);
				foreach($cdump AS $keys => $arrVal){
					if ($arrVal) {
						foreach($arrVal AS $k => $v){
							if(preg_match('/_rating_(\d*)/',$k)){
								$memcache->delete($k);
							}
						}
					}
				}
			}
		}
		//$memcache->flush();
		$array = array('places', 'travels', 'citys');

		foreach($array as $row){
			$resourse = array();
			switch($row){
				case 'places': $resourse = $places;
				case 'travels': $resourse = $travels;
				case 'citys': $resourse = $city;			
			}
			if(!empty($resourse)){
				
				$title = "==== $row ====<br/>";
				echo $title;

				foreach($resourse as $key=>$value){
					$text = "id [$key] => rating [$value]<br/>";
					echo $text;
				}
				echo "<br/>";
			}
		}
	}	
	
	/*
	 * Обновление рейтинга на основе КЭША
	 */
	public function uploadratingAction(){
		
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		// Вывод всех ключей memcache
		
		$memcache = new Memcache;
		$memcache->connect('localhost', 11211) or die ("Could not connect to memcached server");

		$allSlabs = $memcache->getExtendedStats('slabs');
		//$items = $memcache->getExtendedStats('items');

		foreach($allSlabs as $server => $slabs){
			foreach($slabs AS $slabId => $slabMeta){
				$cdump = $memcache->getExtendedStats('cachedump',(int)$slabId);
				foreach($cdump AS $keys => $arrVal){
					if ($arrVal) {
						foreach($arrVal AS $k => $v){
							//echo $k."<br/>";
							$update = 0;
							if(preg_match('/travel_rating_(\d*)/',$k,$match)){
								$table = new Application_Model_DbTable_Travels();
								$id = $match[1];
								$update = 1;
							}
							elseif(preg_match('/place_rating_(\d*)/',$k,$match)){
								$table = new Application_Model_DbTable_Places();
								$id = $match[1];
								$update = 1;
							}
							elseif(preg_match('/city_rating_(\d*)/',$k,$match)){
								$table = new Application_Model_DbTable_City();
								$id = $match[1];
								$update = 1;
							}

							if(!empty($update)){
								$data = $memcache->get($k);
								preg_match('/i:(\d*)/',$data[0],$match);
								// Если нет значения в КЕШЕ
								if(!empty($match[1])){
									$value = $match[1];
									$result = $table->updateRating($id, $value);
									if($result){
										echo "Success update [$k] + $value<br/>";
									}
									else{
										echo "Error update [$k] + $value <br/>";
									}
								}
								$memcache->delete($k);
							}
						}
					}
				}
			}		
		}
		$memcache->flush();
	}
	
	
	
	private function checkSubnet() {		
		if ($this->subnet == '172.16.0' || $this->subnet == '127.0.0') {
			return true;
		}
		return false;
	}
	
	
	/*
	 * Задаем ключи для промо пользователей
	 */
	public function createpromokeyAction() {
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		
		$promo_table = new Application_Model_DbTable_Promo();
		
		$result = $promo_table->setKey();
		
		if($result === true) {
			echo "Success";
		} else {
			echo "Error";
		}
	}
	
	
	
	/*
	 * Удаляем дубликаты промопользователй
	 */
	public function clearpromoAction() {
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		
		$promo_table = new Application_Model_DbTable_Promo();
		
		$result = $promo_table->clear();
		
		if($result === true) {
			echo "Success";
		} else {
			echo "Error";
		}
	}
	
	/*
	 * Сливаем пользователей из 2 баз
	 */
	public function sex2dbAction() {
		// Если запрос не из нашей подсети
	    if (!$this->checkSubnet()) { $this->_helper->redirector('index', 'index'); }
		
		// Отключаем вьюхи
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$promo_table = new Application_Model_DbTable_Promo();		
		
		$select1 = "select * from way2day3.promo";
		$result1 = $promo_table->getAdapter()->query($select1)->fetchAll();
		
		$user_list = array();
		foreach($result1 as $row) {
			if(!in_array($row['email'], $user_list)) {
				$user_list[] = $row['email'];
				echo $row['email']."<br/>";
			}
		}
		echo "<hr/>";
		//echo "<pre>";print_r($result1);echo "</pre><hr/>";
		
		$select2 = "select * from way2day.emails";
		$result2 = $promo_table->getAdapter()->query($select2)->fetchAll();
		//echo "<pre>";print_r($result2);echo "</pre><hr/>";
		
		foreach($result2 as $row) {
			if(!in_array($row['email'], $user_list)) {
				$data = array(
					'email' => $row['email']
				);
				
				$promo_table->insert($data);
				echo "INSER ".$row['email']."<br/>";
				//$user_list[] = $row['email'];
			}
		}
		
	}
	
}