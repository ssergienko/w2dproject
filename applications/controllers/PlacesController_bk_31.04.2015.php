<?php

/**
 *  @filesource
 *  Класс PlacesController
 * 
 *  
 *  @author Sergey S. Sergienko
 */

if (!class_exists('S3')) require_once 'S3.php';

class PlacesController extends Zend_Controller_Action {

	protected $user;
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $this->user = $auth->check();
		
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
		
		// AWS access info
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAIXVQKNLVQCTSRWYA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		
		// Подключаем s3 библиотеку
		$this->s3 = new S3(awsAccessKey, awsSecretKey);
		
	}
	
	
	
	/* Страница списка мест */
	public function indexAction() {
		$places_table = new Application_Model_DbTable_Places();
		
		$this->view->places = $places_table->getList();
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');		
	}
	
	
	
	/*
	 * Места вокруг
	 */
	public function aroundAction () {

        // Пытаемся получить пользователя
        $auth = new Application_Model_Auth();
        $this->view->user = $this->user = $auth->check();

		$this->view->zoom = 7;
		$this->view->map_type = "yandex#map";
		
	}
	
	
	/*
	 * Правильное определение адреса. Не используется, но может потом пришгодиться.
	 */
	/*private function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}*/
	
	
	
	/* Страница описания места */
	public function descriptionAction() {
		
		// Инициализируем объект мест
		$places_table = new Application_Model_DbTable_Places();
		// Инициализируем объект городов
		//$user_table = new Application_Model_DbTable_Users();
		$travels_table = new Application_Model_DbTable_Travels();
        $city_table = new Application_Model_DbTable_City();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->place_id = $place_id = $params['place'];
		
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		if(!empty($place_id)){
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];				
			}
			
			//if(!($this->view->place = $place = $memcache->load('place_'.$place_id.$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$place = $places_table->getByIdFromUser($place_id);				
				} else {
					$place = $places_table->getById($place_id);
				}
				$this->view->place = $place;
				//$memcache->save($this->view->place, 'place_'.$place_id.$mem_name);
			//}

            // Проверяем привязку к городу. Если есть выводм текст "посмотреть другие места в городе Город"
            if ($place['city']) {
                $city = $city_table->getById($place['city']);
                $this->view->city_name = $city['title'];
                $this->view->city_titleplaces = $city['titleplaces'];
            }

			// Проверяем является ли пользователь автором
			if($place['author'] == $this->user['id']) {
				$this->view->my_place = $place['author'];
			}
			// Проверяем добавлена ли поездка в избранные и в понравившиеся
			elseif(!empty($this->user)) {
				$placeFav_table = new Application_Model_DbTable_PlaceFav();
				$this->view->place_fav = $placeFav_table->check($place_id, $this->user['id']);
				
				$placeLike_table = new Application_Model_DbTable_PlaceLike();
				$this->view->place_like = $placeLike_table->check($place_id, $this->user['id']);
			}
			
			$this->view->headTitle($place['title'],'PREPEND');

			if(!empty($this->user)){				
				$this->view->my_travels = $travels_table->getByUser($this->user['id']);				
			}
			
			// Инициализируем объект картинок мест
			$place_images_table = new Application_Model_DbTable_Placeimages();			
			$this->view->large_images = $place_images_table->getLargeImages($place_id);
			
		} else{
			$this->_redirect('');
		}
	}



	/* Страница карты места */
	public function mapAction() {
		
		$this->view->zoom = 13;
		$this->view->map_type = "yandex#map";
		
		// Инициализируем объект мест
		$places_table = new Application_Model_DbTable_Places();
		$travels_table = new Application_Model_DbTable_Travels();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->place_id = $place_id = $params['place'];
				
		// Подключаем мемкэш
		$memcache = Zend_Registry::getInstance()->oCache;
		
		if(!empty($place_id)) {
			
			// Дополняем именование мемкеша если зашли от пользователя
			$mem_name = "";
			if(!empty($params['user']) && $params['user'] == $this->user['id']) {
				$mem_name = "_user_".$this->user['id'];				
			}
			
			//if(!($this->view->place = $place = $memcache->load('place_'.$place_id.$mem_name))) {
				// Получаем поездку
				if(!empty($params['user']) && $params['user'] == $this->user['id']) {
					$place = $places_table->getByIdFromUser($place_id);				
				} else {
					$place = $places_table->getById($place_id);
				}
				$this->view->place = $place;
			//	$memcache->save($this->view->place, 'place_'.$place_id.$mem_name);
			//}
			
			// Проверяем является ли пользователь автором
			if($place['author'] == $this->user['id']) {
				$this->view->my_place = $place['author'];
			}
			// Проверяем добавлена ли поездка в избранные
			elseif(!empty($this->user)) {
				$placeFav_table = new Application_Model_DbTable_PlaceFav();
				$this->view->place_fav = $placeFav_table->check($place_id, $this->user['id']);
				
				$placeLike_table = new Application_Model_DbTable_PlaceLike();
				$this->view->place_like = $placeLike_table->check($place_id, $this->user['id']);
			}
			
			$this->view->headTitle("Как добраться в ".$place['title'],'PREPEND');
			
			if(!empty($this->user)){
				$this->view->my_travels = $travels_table->getByUser($this->user['id']);				
			}
			
		} else{
			$this->_redirect('');
		}
	}
	
	
	
	/* 
	 * Страница добавления места
	 * 
	 * 
	 * 
	 */
	public function addAction() {
		
		if(!empty($this->user['id'])) {
			
			// Вытаскиваем конфиг из реестра
			$config = Zend_Registry::getInstance()->config;
			
			$this->view->user_id = $this->user['id'];
			
			$this->view->zoom = 10;
			$this->view->map_type = "yandex#map";
			
			$params = $this->getRequest()->getParams();
			
			$city_table = new Application_Model_DbTable_City();

            if (!empty($params['city'])) {
                $city = $city_table->getById($params['city']);
                $this->view->city_title = $city['title'];
                $this->view->lat = $city['lat'];
                $this->view->lng = $city['lng'];
			}

            // Как только входим
			if (!$this->_request->isPost()) {

				$places_table = new Application_Model_DbTable_Places();
				// Получаем черновик
				$draft = $places_table->getDraft($this->user['id']);

				// Если уже есть черновик (начинал создавать место)
				if (!empty($draft)) {
					
					// Имя					
					$this->view->title = (!empty($draft->title)) ? $draft->title : '';
					// Город
                    $city = $city_table->getById($draft->city);
					$this->view->city_title = (!empty($city['title'])) ? $city['title'] : '';
					// Описание
					$this->view->description = (!empty($draft->description)) ? $draft->description : '';
					// Id
					$this->view->place_id = $draft->id;

                    // ПОлучаем координаты
                    if (!empty($draft->lat) && !empty($draft->lng)) {
                        $this->view->lat = $draft->lat;
                        $this->view->lng = $draft->lng;
                    }

				} else {
					// Иначе сразу создаем место
					$this->view->place_id = $place_id = $places_table->insert(array(
                        'lat' => !empty($city) ? $city['lat'] : '',
                        'lng' => !empty($city) ? $city['lng'] : '',
                        'city' => (!empty($city) ? $city['id'] : ''),
                        'confirm' => 0,
                        'author' => $this->user['id']));
				}
				
			} else {
			
				// Инициализируем объект мест
				$places_table = new Api_Model_DbTable_Places();

                if (!empty($params['lat']) && !empty($params['lng'])) {
                    $this->view->lat = $params['lat'];
                    $this->view->lng = $params['lng'];
                }

				// place_id будет в параметрах только если нажали кнопку сохранить
				$this->view->place_id = $place_id = (!empty($params['place_id'])) ? $params['place_id'] : false;
				
				$error = array();
				
				// Проверяем имя
				if(empty($params['title'])) {
					$error['title'] = "Необходимо ввести название места";
				} else {
					$this->view->title = $params['title'];
				}
				
				// Проверяем имя
				if(empty($params['lat']) || (!is_float($params['lat']) && !is_numeric($params['lat']))) {
					$error['lat'] = "Необходимо ввести координаты";
				} else {
					$this->view->lat = $params['lat'];
				}
				
				// Проверяем имя
				if(empty($params['lng']) || (!is_float($params['lng']) && !is_numeric($params['lng']))) {
					$error['lng'] = "Необходимо ввести координаты";
				} else {
					$this->view->lng = $params['lng'];
				}
				
				// Проверяем город
				if(!empty($params['city_title'])) {
                    $this->view->city_title = $params['city_title'];
				}
				
				// Проверяем описание
				if(!empty($params['description'])) {
					$this->view->description = $params['description'];
				}
				
				if(empty($error)) {
					
					// Выбираем Id города по его имени
					$city = $city_table->getByName($params['city_title']);
					
					if(!empty($city)) {
						$city_id = $city['id'];						
					} else {
						$data = array('title' => $params['city_title']);
						$city_id = $city_table->add($data);
					}
					
					$data = array(
						'title' => $params['title'],
						'city' => $city_id,
						'long_text' => $params['description'],
						'author' => $this->user['id'],
						'owner' => $this->user['id'],
						'lat' => $params['lat'],
						'lng' => $params['lng']
					);
					
					// Если пользователь не выбрал главную картинку, ставим первую загруженную
					if (empty($params['main_picture'])) {
						
						$place_images = new Application_Model_DbTable_Placeimages();
						$select = $place_images->select()->where('pl_id = (?)', $place_id)->limit(1)->order('id desc');
						
						$res = $place_images->fetchRow($select);
						
						if (!empty($res->middle_url)) {
							$data['image_url'] = $res->middle_url;
						}
					}
					
					$places_table->update($data, 'id = '.$place_id);
					
					// Если место добавляется в поездку
					if(!empty($params['travel'])) {
						$travel_id = $params['travel'];

						$travels_table = new Application_Model_DbTable_Travels();
						$travel = $travels_table->getById($travel_id);
						
						if(!empty($travel)) {
							if($travel['owner'] == $this->user['id']) {
								$travelplace_table = new Application_Model_DbTable_Travelplace();
								$result_check = $travelplace_table->check($travel_id, $place_id);
								if($result_check === false) {
									$data_travelplace = array(
										't_id' => $travel_id,
										'p_id' => $place_id
									);
									$result_travelplace = $travelplace_table->add($data_travelplace);
									
									if (!empty($params['title']) && $params['title'] != '') {
										$places_table->update(array('confirm' => 1), 'id = '.$place_id);
									}
									
									if(!empty($result_travelplace)) {
										$this->_redirect("/travel/$travel_id/plan?user=".$this->user['id']);
									}
								}

							}
						}
					}

					if (!empty($params['title']) && $params['title'] != '') {
						$places_table->update(array('confirm' => 1), 'id = '.$place_id);
					}
					
					$this->_redirect('/place/'.$place_id.'?user='.$this->user['id']);
					
				}
				
				if(!empty($error)) {
					$this->view->error = $error;
				}				
			}
			
		} else {
			// TODO переводить на страницу авторизации, после авторизации возвращать сюда
			$this->_redirect('');
		}
	}
	
	
	
	/*
	 * Удаление места
	 */
	public function deleteAction () {
		
		$this->_helper->viewRenderer->setNoRender(true);
		
		$params = $this->getRequest()->getParams();
		
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		// Проверяем авторозацию
		if (!empty($user['id']) && $user['id'] === $params['user']) {
			
			if (!empty($params['id'])) {
				/////// Удаляем запись о месте //////
				$places_table = new Application_Model_DbTable_Places();
				$places_table->delete("id = ".$params['id']);

				////// Удаляем картинки места из s3 хранилища ///////
				$placesimages_table = new Application_Model_DbTable_Placeimages();
				$images = $placesimages_table->getImagesByPlId($params['id']);

				foreach ($images as $row) {

					if (!empty($row['img_url'])) {
						// ПОлучаем имя файла по урлу
						$slice = preg_split("/\//", $row['img_url']);

						// Корзины
						$backets = array('w2d-files', 'w2d-middle', 'w2d-thumbnail');
						// Удаляем из всех хранилищ
						foreach ($backets as $backet) {
							if ($this->s3->deleteObject($backet, baseName($slice[count($slice)]-1))) {
								echo json_encode(array('success' => 'deleted'));
							} else {
								echo json_encode(array('error' => 'not deleted'));
							}
						}

					}
				}

				///// Удаляем из временной таблицы ///////////
				$placesimages_table->delete("pl_id = ".$params['id']);

				$this->_redirect('/user/'.$params['user'].'/places');

			}
			
		}

		$this->_redirect('');
		
	}
	
	
	
	
	
	
	/*
	 * Редактируем поездку
	 */
	public function editAction () {
		
		$params = $this->getRequest()->getParams();
		
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;
		
		$this->view->user_id = $this->user['id'];
		
		if(!empty($params['place'])) {
			$places_table = new Application_Model_DbTable_Places();
			
			if ($this->_request->isPost()) {
				
				$error = array();
				$success = array();
				// Проверяем имя
				if(empty($params['title'])) {
					$error['title'] = "Необходимо ввести название места";
				}

				//var_dump($params['lng'], $params['lat']); die();
				
				// Проверяем город
				if(empty($params['lat']) || empty($params['lng']) || (!is_float($params['lat']) && !is_numeric($params['lat'])) && (!is_float($params['lng']) && !is_numeric($params['lng']))) {
					$error['lat'] = "Необходимо ввести долготу";
					$error['lng'] = "Необходимо ввести широту";
				}
				
				if(empty($error)) {
					
					$place = $places_table->getByIdForEdit($params['place']);
					
					$data = array();
					if($place['title'] != $params['title']) {
						$data['title'] = $params['title'];
					}
					if($place['long_text'] != $params['long_text']) {
						$data['long_text'] = $params['long_text'];
					}
					
					$data['lat'] = $this->view->lat = $params['lat'];
					$data['lng'] = $this->view->lng = $params['lng'];
					
					if($place['city']['title'] != $params['city_title']) {
						$city_table = new Application_Model_DbTable_City();
					
						// Выбираем Id города по его имени
						$city = $city_table->getByName($params['city_title']);

						if(!empty($city)) {
							$data['city'] = $city['id'];							
						} else {
							$data_city = array(
								'title' => $params['city_title']
							);
							$data['city'] = $city_table->add($data_city);							
						}
						$success['city_title'] = 'Данные успешно обновлены';
					}
					
					if(!empty($data)) {
						$result = $places_table->edit($params['place'], $data);
					}
					
				}
				
				if(!empty($error)) {
					$this->view->error = $error;
				} else {
					$this->_redirect('/place/'.$params['place'].'?user='.$this->user['id']);
				}
			}
			
			$place = $places_table->getByIdForEdit($params['place']);
			
			// ПОлучаем координаты
			if (!empty($place['lat']) && !empty($place['lng'])) {
				$this->view->lat = $place['lat'];
				$this->view->lng = $place['lng'];
			} else {
				$this->view->lat = $config->coord->lat;
				$this->view->lng = $config->coord->lng;
			}
			
			$this->view->zoom = 10;
			$this->view->map_type = "yandex#map";
			
			if($place['author'] == $this->user['id']) {
				$this->view->place = $place;
			}
		}
	}
	
	

}
