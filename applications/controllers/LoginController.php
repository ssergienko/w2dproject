<?php

/**
 *  @filesource
 *  Класс LoginController
 * 
 *  
 *  @author Chernyshov A moroznoeytpo@gmail.com
 */
class LoginController extends Zend_Controller_Action {	
	
	
	
	public function init() {
		// Переопределяем шаблон
		$this->_helper->layout->setLayout('login');	
	}
	
	
	
	/* Стартовая страница */
	public function indexAction() {
		
		// Очищаем сессию
		//Zend_Session::namespaceUnset('w2dPromo');
		
		$promo_table = new Application_Model_DbTable_Promo();
		$user_table = new Application_Model_DbTable_Users();
		
		$params = $this->getRequest()->getParams();
		
		if(!empty($params['submit'])) {
			$error = array();
			
			if(empty($params['unicId'])) {
				$error['unicId'] = "Необходимо ввести промокод";
				if(!empty($params['key'])) {
					$this->view->unicId = $params['key'];
				}
			} else {
				$this->view->unicId = $params['unicId'];
			}
						
			if(empty($error)) {
				$result = $promo_table->checkForKey($params['unicId']);
				if(!empty($result)) {
					
					$time = 60*60*24*30; // Ауторизация на месяц
					setcookie('w2dPromo_id', $result['id'], time() + $time, '/');
					setcookie('w2dPromo_email', $result['email'], time() + $time, '/');
					setcookie('w2dPromo_unic_id', $result['unic_id'], time() + $time, '/');

					echo $promo_table->update(array('entered' => 1), 'unic_id = \''.$params['unicId'].'\'');
					
					$this->_redirect('/');

				} else {
					$error['unicId'] = "Неверный промокод";					
				}
			}
			
			if(!empty($error)) {
				$this->view->error = $error;
			}			
		} elseif(!empty($params['key'])) {
			$this->view->unicId = $params['key'];
		}
		
	}
	
	
	
	/*
	 * Форма подачи заявки на участие гостиницы в проекте
	 */
	public function hotelsAction() {
		
		if ($this->_request->isPost()) {
			
			$formData = $this->_request->getPost();
						
			$this->view->empty_orgname = empty($formData['orgname']) ? true : false;
			$this->view->empty_name = empty($formData['name']) ? true : false;
			$this->view->empty_phone = empty($formData['phone']) ? true : false;
			$this->view->empty_email = empty($formData['email']) ? true : false;
			$this->view->bad_email = !empty($formData['email']) && !preg_match("/@/", $formData['email']) ? true : false;
			
			if (!empty($formData['orgname']) && !empty($formData['name']) && !empty($formData['phone']) && !empty($formData['email']) && empty($this->view->bad_email)) {
			
				// Вытаскиваем конфиг из реестра
				$config = Zend_Registry::getInstance()->config;
				
				$data = array(
					'orgname' => $formData['orgname'],
					'name' => $formData['name'],
					'phone' => $formData['phone'],
					'email' => $formData['email']
				);

				$orgs_table = new Application_Model_DbTable_Orgs();
				// Вставляем данные
				$new_org_id = $orgs_table->insert($data);

				// Подключаем объект работы с почтой
				$email_obj = new Application_Model_Email();
				
				// Получаем заголовки
				$headers = $email_obj->getEmailHeaders('manager@way2day.ru', 'plain');
				
				// Подготавливаем сообщение для отправики себе, чтобы знать что ктото добавился
				$message = $email_obj->getMessage('addhotel_adm', $data);
				if ($headers && $message) {
					mail($config->emails->admin.', '.$config->emails->manager, 'Приглашение на way2day.ru', $message, $headers);
				}
			
				// Получаем заголовки
				$headers = $email_obj->getEmailHeaders('manager@way2day.ru', 'html');
				
				// Подготавливаем сообщение пользователю				
				$message = $email_obj->getMessage('addhotel', $data);		
				// Отправляем
				mail($formData['email'], 'Заявка на участие в проекте way2day.ru', $message, $headers);
				
				$this->view->result_message = "Ваша заявка принята. В ближайшее время наш менеджер свяжется с вами.";
				
			} else {
				// Возвращаем параметры, чтобы не вводить заново
				$this->view->orgname = $formData['orgname'];
				$this->view->name = $formData['name'];
				$this->view->phone = $formData['phone'];
				$this->view->email = $formData['email'];				
			}
			
		}
		
	}
	
}
