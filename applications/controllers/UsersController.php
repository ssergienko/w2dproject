<?php

/**
 *  @filesource
 *  Класс IndexController
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class UsersController extends Zend_Controller_Action {

	protected $user;
	
	public function init() {
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $this->user = $auth->check();		
	}
	
	public function indexAction() {
		$this->view->headTitle("Пользователь ".$this->user['uname'] ,'PREPEND');	
		
		$user_table = new Application_Model_DbTable_Users();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->user_id = $user_id = $params['user'];
		
		if(!empty($user_id)){
			
			if($user_id == $this->user['id']) {
				$this->view->my_page = 1;
			}
			
			if(!empty($params['submit'])) {
				$error = array();
				$success = array();
				
				$data_change = array();
				// Проверяем имя пользователя
				if($params['uname'] != $this->user['uname']) {
					/*$check = $user_table->checkByField('uname',$params['uname']);
					if($check === false){
						$data_change['uname'] = $params['uname'];						
					} else {
						$error['uname'] = "Пользователь с таким именем уже существует";
					}*/
					$data_change['uname'] = $params['uname'];
				}
				// Проверяем email пользователя
				if($params['uemail'] != $this->user['uemail']) {
					/*$check = $user_table->checkByField('uemail',$params['uemail']);
					if($check === false){
						$data_change['uemail'] = $params['uemail'];						
					} else {
						$error['uemail'] = "Пользователь с таким email уже существует";
					}*/	
					$data_change['uemail'] = $params['uemail'];	
				}
				// Проверяем описание
				if($params['about'] != $this->user['about']) {
					$data_change['about'] = $params['about'];
				}
				
				if(!empty($data_change)) {
					$result = $user_table->edit($user_id, $data_change);
					if($result === true) {
						foreach($data_change as $key=>$value) {
							$success[$key] = "Данные успешно обновлены";
						}
						
						// Заново получаем данные пользователя
						$auth = new Application_Model_Auth();
						$this->view->user = $this->user = $auth->check();
					}
				}
				if(!empty($error)) {
					$this->view->error = $error;
				}
				if(!empty($success)) {
					$this->view->success = $success;
				}
			}
			
			$this->view->user_view = $user_table->getById($user_id);
		}
	}
	
	/*
	 * Поездки пользователя
	 */
	public function travelsAction() {
		$this->view->headTitle("Поездки пользователя ".$this->user['uname'] ,'PREPEND');
		
		$user_table = new Application_Model_DbTable_Users();
		$travels_table = new Application_Model_DbTable_Travels();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->user_id = $user_id = $params['user'];
		
		if(!empty($user_id)){
			
			if($this->user['id'] == $user_id){
				$this->view->my_page = $my_page = 1;
				
				$travels = $travels_table->getMyTravels($user_id);
				
				$travelFav_table = new Application_Model_DbTable_TravelFav();
				$this->view->travelFav = $travelFav_table->getByUser($user_id);
			} else {
				$travels = $travels_table->getByUser($user_id);
			}
			$this->view->travels = $travels;
			
			$this->view->user_view = $user_table->getById($user_id);
		}
	}
	
	
	/*
	 * Места пользователя
	 */
	public function placesAction() {
		$this->view->headTitle("Места пользователя ".$this->user['uname'] ,'PREPEND');
		
		$user_table = new Application_Model_DbTable_Users();
		$places_table = new Application_Model_DbTable_Places();		
		
		$params = $this->getRequest()->getParams();
		
		$this->view->user_id = $user_id = $params['user'];
		
		if(!empty($user_id)){
			
			if($this->user['id'] == $user_id){
				$this->view->my_page = 1;
				
				$places = $places_table->getMyPlaces($user_id);
				
				$placeFav_table = new Application_Model_DbTable_PlaceFav();
				$this->view->placeFav = $placeFav_table->getByUser($user_id);
				
			} else {
				$places = $places_table->getByUser($user_id);
			}
			
			// Получаем место
			$this->view->places = $places;
			
			$this->view->user_view = $user_table->getById($user_id);
		}
	}


    /*
	 * Города пользователя
	 */
    public function citiesAction() {
        $this->view->headTitle("Города пользователя ".$this->user['uname'] ,'PREPEND');

        $user_table = new Application_Model_DbTable_Users();
        $city_table = new Application_Model_DbTable_City();

        $params = $this->getRequest()->getParams();

        $this->view->user_id = $user_id = $params['user'];

        if(!empty($user_id)){

            if($this->user['id'] == $user_id){
                $this->view->my_page = 1;
                $cities = $city_table->getMyCities($user_id);
            } else {
                $cities = $city_table->getByUser($user_id);
            }

            // Получаем место
            $this->view->cities = $cities;

            $this->view->user_view = $user_table->getById($user_id);
        }
    }


	/*
	 * Настройки пользователя
	 */
	public function optionsAction() {
		$this->view->headTitle("Места пользователя ".$this->user['uname'] ,'PREPEND');
		
		$user_table = new Application_Model_DbTable_Users();
		$options_table = new Application_Model_DbTable_Options();
		
		$params = $this->getRequest()->getParams();
		
		$this->view->user_id = $user_id = $params['user'];
		
		if(!empty($user_id)) {
			
			if($this->user['id'] != $user_id) {
				$this->_redirect('/user/'.$user_id);
			} else {
				$this->view->my_page = 1;
			}
			
			$options = $options_table->getByUserId($user_id);
			
			if(!empty($params['submit'])) {
				$error = array();
				$success = array();
				
				// Подписка на новости
				$news_email = !empty($params['news_email'])?1:0;
				if($news_email != $options['news_email'] || empty($options)) {
					$data = array(
						'news_email' => $news_email);
					$result = $options_table->edit($user_id, $data);
					if($result === true) {
						$success['news_email'] = "Данные успешно обновлены";
						// Изменяем подписку на новости
						$options['news_email'] = $news_email;						
					} else {
						$success['news_email'] = $result;
					}
				}
				
				if(!empty($error)) {
					$this->view->error = $error;
				}
				if(!empty($success)) {
					$this->view->success = $success;
				}
			}
			$this->view->options = $options;
			
			$this->view->user_view = $this->user;
		}
	}

}
