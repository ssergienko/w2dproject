<?php
include("SxGeo.php"); 
/**
 *  @filesource
 *  Класс IndexController
 * 
 *  
 *  @author Sergey S. Sergienko
 */
class IndexController extends Zend_Controller_Action {	
	
	
	
	public function init() {
		$this->view->abc = array('А','Б','В','Г','Д','Е','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Э','Ю','Я');
	}
	
	
	
	/* Стартовая страница */
	public function indexAction() {

		$SxGeo = new SxGeo('SxGeoCity.dat');
		
		$city = $SxGeo->getCity($this->getRealIpAddr());
		
		$this->view->headTitle('Куда поехать на выходные?','PREPEND');
		
		$params = $this->getRequest()->getParams();
		
		$hoffset = !empty($params['hoffset']) ? $params['hoffset'] : 0;
		$coffset = !empty($params['coffset']) ? $params['coffset'] : 0;
		$toffset = !empty($params['toffset']) ? $params['toffset'] : 0;
		$poffset = !empty($params['poffset']) ? $params['poffset'] : 0;
		
		// Пытаемся получить пользователя
		$auth = new Application_Model_Auth();
		$this->view->user = $auth->check();

		$memcache = Zend_Registry::getInstance()->oCache;
		
		// Инициализируем объект городов
		$city_table = new Application_Model_DbTable_City();
		// Инициализируем объект поездок
		$travels_table = new Application_Model_DbTable_Travels();
		// Инициализируем объект мест
		$places_table = new Application_Model_DbTable_Places();
		// Инициализируем объект гостиниц
		$hotels_table = new Application_Model_DbTable_Hotels();
		
		// Получаем 2 города		
		//if(!($this->view->city = $memcache->load('main_city'))) {
			$this->view->city = $city_table->getMain($coffset, $city['lat'], $city['lon']);			
		//	$memcache->save($this->view->city, 'main_city');
		//}		
		// Получаем 4 поездки
		//if(!($this->view->travels = $memcache->load('main_travels'))) {
			$this->view->travels = $travels_table->getMain($toffset);			
		//	$memcache->save($this->view->travels, 'main_travels');
		//}
		// Получаем 4 места		
		//if(!($this->view->places = $memcache->load('main_places'))) {
			$this->view->places = $places_table->getMain($poffset, $city['lat'], $city['lon']);
		//	$memcache->save($this->view->places, 'main_places');
		//}
		// Получаем 4 гостиницы
		//if(!($this->view->hotels = $memcache->load('main_hotels'))) {
			$this->view->hotels = $hotels_table->getMain($hoffset);			
		//	$memcache->save($this->view->hotels, 'main_hotels');
		//}

        $this->view->zoom = 7;
        $this->view->map_type = "yandex#map";
		
	}	
	
	
	
	/* Опделяет пользователя и перенаправляет на страницу мест пользователя */
	public function myplacesAction(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		// Если передан ID места
		$params = $this->getRequest()->getParams();
		if(!empty($params['place'])) {
			$url = "/place/".$params['place']."?user=".$user['id'];
		} else {
			$url = "/user/".$user['id']."/places";
		}
		
		$this->_redirect($url);
	}	
	
	
	
	/* Опделяет пользователя и перенаправляет на страницу поездок пользователя */
	public function mytravelsAction(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		// Если передан ID поездки
		$params = $this->getRequest()->getParams();
		if(!empty($params['travel'])) {
			$url = "/travel/".$params['travel']."?user=".$user['id'];
		} else {
			$url = "/user/".$user['id']."/travels";
		}
		
		$this->_redirect($url);		
	}

	
	private function getRealIpAddr()
	{
		
		/*FOR DEBUGG
		 * 
		 * var_dump($_SERVER);
		
		echo 'HTTP_CLIENT_IP<br />';
		var_dump($_SERVER['HTTP_CLIENT_IP']);
		echo '<br />';
		echo 'HTTP_X_FORWARDED_FOR<br />';
		var_dump($_SERVER['HTTP_X_FORWARDED_FOR']);
		echo '<br />';
		echo 'REMOTE_ADDR<br />';
		var_dump($_SERVER['REMOTE_ADDR']);
		echo '<br />';
		echo 'HTTP_X_REAL_IP<br />';
		var_dump($_SERVER['HTTP_X_REAL_IP']);
		
		die();*/
		
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		elseif (!empty($_SERVER['HTTP_X_REAL_IP'])) {
			$ip=$_SERVER['HTTP_X_REAL_IP'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
}
