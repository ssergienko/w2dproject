<?php

require_once 'Zend/Layout.php';


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initAutoload() {
		$moduleLoader = new Zend_Application_Module_Autoloader(array(
					'namespace' => '',
					'basePath' => APPLICATION_PATH));

		return $moduleLoader;
	}
	/**
	 * Задаем локали и кодировки
	 */
	/*protected function _initLocale() {

		// Кодировка для мультибайта
		mb_internal_encoding('utf-8');
	}*/

	protected function _initViewHelpers(){
		$this->bootstrap('view');
		$view = $this->getResource('view');

		$view->headMeta()->appendHttpEquiv('Content-Type', 'text/html;charset=utf-8');
		/*$view->headTitle('Way2Day');
		$view->headTitle()->setSeparator(' :: ');*/
	}

	/**
	 * Помещаем в реестр текущий конфиг
	 */
	protected function _initConfig() {

		// Получаем конфиг
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);

		// Помещаем конфиг в реестр
		Zend_Registry::getInstance()->config = $config;

	}




	/* Подключается плагин проверки авторизации
	 * protected function _initPlugins(){
		if(CRON_START === FALSE) {
			$front = Zend_Controller_Front::getInstance();
			$front->registerPlugin(new Plugin_Acl());

		}
	}*/



	public function _initMemcashed() {

		//настраиваем стратегию backend кэширования
		/*$oBackend = new Zend_Cache_Backend_Memcached(
			array(
				'servers' => array( array(
					'host' => 'localhost',
					'port' => 11211
				) ),
				'compression' => true,
				'compatibility'=>true
		) );

		// настраиваем стратегию frontend кэширования
		$oFrontend = new Zend_Cache_Core(
			array(
				'caching' => true,
				'cache_id_prefix' => '',
				'logging' => false,
				'write_control' => true,
				'automatic_serialization' => true,
				'ignore_user_abort' => true
			)
		);

		// составляем объект кэширования
		Zend_Registry::getInstance()->oCache = Zend_Cache::factory( $oFrontend, $oBackend );*/

	}



	/**
	 * Настройка Zend_Layout
	 */
	protected function _initLayout(){
		if(!defined('RATING_OPERATION')){
			$layout = explode('/', $_SERVER['REQUEST_URI']);

			$layout_dir = APPLICATION_PATH . '/layouts/scripts';

			$options = array(
				'layout'     => 'layout',
				'layoutPath' => $layout_dir
			);

			Zend_Layout::startMvc($options);
		}

	}



	/*protected function _initDatabases()
	{
		$this->bootstrap('multidb'); // line 41
		$resource = $this->getPluginResource('multidb');
		$databases = Zend_Registry::get('config')->resources->multidb;
		foreach ($databases as $name => $adapter)
		{
			$db_adapter = $resource->getDb($name);
			Zend_Registry::set($name, $db_adapter);
		}
	}*/



	protected function _initDbAdaptersToRegistry()
	{
		$this->bootstrap('multidb');
		$resource = $this->getPluginResource('multidb');
		$resource->init();
		$Adapter1 = $resource->getDb('db');
		$Adapter2 = $resource->getDb('db2');
		Zend_Registry::set('Adapter1RegKey', $Adapter1);
		Zend_Registry::set('Adapter2RegKey',$Adapter2);
	}


	/*
     * Инициализируем объект навигатора и передаем его в View
     *
     * @return Zend_Navigation
     */
    protected function _initNavigation()
    {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/navigation.xml');

		$container = new Zend_Navigation($config);

		$view->navigation($container);

	}



	/*protected function _initCityResources()
	{
		return new Zend_Application_Module_Autoloader
		(
			array('namespace'=>'City', 'basePath'=> APPLICATION_PATH . '/modules/city')
		);
	}

	protected function _initUserResources()
	{
		return new Zend_Application_Module_Autoloader
		(
			array('namespace'=>'User', 'basePath'=> APPLICATION_PATH . '/modules/user')
		);
	}*/

	protected function _initOdminResources()
	{
		return new Zend_Application_Module_Autoloader
		(
			array('namespace'=>'Odmin', 'basePath'=> APPLICATION_PATH . '/modules/odmin')
		);
	}

	protected function _initApiResources()
	{
		return new Zend_Application_Module_Autoloader
		(
			array('namespace'=>'Api', 'basePath'=> APPLICATION_PATH . '/modules/api')
		);
	}

	// Подключение xml роутера
	protected function _initXmlRoutes()
	{
		$router = Zend_Controller_Front::getInstance()->getRouter();

// ========================================================================================================================================================================
// ===== Города ===========================================================================================================================================================
// ========================================================================================================================================================================
// ===== Список городов ========================================================
// =============================================================================
		$router->addRoute(
			'citys',
			new Zend_Controller_Router_Route_Regex(
				'citys',
				array(
					'controller'	=> 'city',
					'action'		=> 'index'),
				array(),
				'citys'
			));
// =============================================================================
// ===== Описание города =======================================================
// =============================================================================
		$router->addRoute(
			'city',
			new Zend_Controller_Router_Route_Regex(
				'city/(\d+)',
				array(
					'controller'	=> 'city',
					'action'		=> 'description'),
				array(
					'city' => 1),
				'city/%d'
			));
// =============================================================================
// ===== Список мест в городе ==================================================
// =============================================================================
		$router->addRoute(
			'city_places',
			new Zend_Controller_Router_Route_Regex(
				'city/(\d+)/places',
				array(
					'controller'	=> 'city',
					'action'		=> 'places'),
				array(
					'city' => 1),
				'city/%d/places'
			));
// =============================================================================
// ===== Список поездок в городе ===============================================
// =============================================================================
		$router->addRoute(
			'city_travels',
			new Zend_Controller_Router_Route_Regex(
				'city/(\d+)/travels',
				array(
					'controller'	=> 'city',
					'action'		=> 'travels'),
				array(
					'city' => 1),
				'city/%d/travels'
			));
// =============================================================================
// ===== Список гостиниц в городе ==============================================
// =============================================================================
		$router->addRoute(
			'city_hotels',
			new Zend_Controller_Router_Route_Regex(
				'city/(\d+)/hotels',
				array(
					'controller'	=> 'city',
					'action'		=> 'hotels'),
				array(
					'city' => 1),
				'city/%d/hotels'
			));

// =============================================================================
// ===== Редактирование города ==================================================
// =============================================================================
        $router->addRoute(
            'city_edit',
            new Zend_Controller_Router_Route_Regex(
                'city/(\d+)/edit',
                array(
                    'controller'	=> 'city',
                    'action'		=> 'edit'),
                array(
                    'city' => 1),
                'city/%d/edit'
            ));
// =============================================================================
// ===== Карта города =======================================================
// =============================================================================
		$router->addRoute(
			'city_map',
			new Zend_Controller_Router_Route_Regex(
				'city/(\d+)/map',
				array(
					'controller'	=> 'city',
					'action'		=> 'map'),
				array(
					'city' => 1),
				'city/%d/map'
			));
// =============================================================================
// ===== Места =================================================================
// =============================================================================
// ===== Список мест ===========================================================
// =============================================================================
		$router->addRoute(
			'places',
			new Zend_Controller_Router_Route_Regex(
				'places',
				array(
					'controller'	=> 'places',
					'action'		=> 'index'),
				array(),
				'places'
			));
// =============================================================================
// ===== Описание места ========================================================
// =============================================================================
		$router->addRoute(
			'places_description',
			new Zend_Controller_Router_Route_Regex(
				'place/(\d+)',
				array(
					'controller'	=> 'places',
					'action'		=> 'description'),
				array(
					'place' => 1),
				'place/%d'
			));
// =============================================================================
// ===== Карта места ===========================================================
// =============================================================================
		$router->addRoute(
			'places_map',
			new Zend_Controller_Router_Route_Regex(
				'place/(\d+)/map',
				array(
					'controller'	=> 'places',
					'action'		=> 'map'),
				array(
					'place' => 1),
				'place/%d/map'
			));
// =============================================================================
// ===== Редактирование места ==================================================
// =============================================================================
		$router->addRoute(
			'place_edit',
			new Zend_Controller_Router_Route_Regex(
				'place/(\d+)/edit',
				array(
					'controller'	=> 'places',
					'action'		=> 'edit'),
				array(
					'place' => 1),
				'place/%d/edit'
			));
// =============================================================================
// ===== Поездки ===============================================================
// =============================================================================
// ===== Список поездок ========================================================
// =============================================================================
		$router->addRoute(
			'travels',
			new Zend_Controller_Router_Route_Regex(
				'travels',
				array(
					'controller'	=> 'travels',
					'action'		=> 'index'),
				array(),
				'travels'
			));
// =============================================================================
// ===== Описание поездки ======================================================
// =============================================================================
		$router->addRoute(
			'travel_description',
			new Zend_Controller_Router_Route_Regex(
				'travel/(\d+)',
				array(
					'controller'	=> 'travels',
					'action'		=> 'description'),
				array(
					'travel' => 1),
				'travel/%d'
			));
// =============================================================================
// ===== План поездки у поездки ================================================
// =============================================================================
		$router->addRoute(
			'travel_plan',
			new Zend_Controller_Router_Route_Regex(
				'travel/(\d+)/plan',
				array(
					'controller'	=> 'travels',
					'action'		=> 'plan'),
				array(
					'travel' => 1),
				'travel/%d/plan'
			));
// =============================================================================
// ===== Гостиницы у поездки ===================================================
// =============================================================================
		$router->addRoute(
			'travel_hotels',
			new Zend_Controller_Router_Route_Regex(
				'travel/(\d+)/hotels',
				array(
					'controller'	=> 'travels',
					'action'		=> 'hotels'),
				array(
					'travel' => 1),
				'travel/%d/hotels'
			));
// =============================================================================
// ===== Карта у поездки =======================================================
// =============================================================================
		$router->addRoute(
			'travel_map',
			new Zend_Controller_Router_Route_Regex(
				'travel/(\d+)/map',
				array(
					'controller'	=> 'travels',
					'action'		=> 'map'),
				array(
					'travel' => 1),
				'travel/%d/map'
			));
// =============================================================================
// ===== Редактирование поездки ================================================
// =============================================================================
		$router->addRoute(
			'travel_edit',
			new Zend_Controller_Router_Route_Regex(
				'travel/(\d+)/edit',
				array(
					'controller'	=> 'travels',
					'action'		=> 'edit'),
				array(
					'travel' => 1),
				'travel/%d/edit'
			));
// =============================================================================
// ===== Гостиницы =============================================================
// =============================================================================
// ===== Список гостиниц =======================================================
// =============================================================================
		$router->addRoute(
			'hotels',
			new Zend_Controller_Router_Route_Regex(
				'hotels',
				array(
					'controller'	=> 'hotels',
					'action'		=> 'index'),
				array(),
				'hotels'
			));
// =============================================================================
// ===== Описание гостиницы ====================================================
// =============================================================================
		$router->addRoute(
			'hotel_description',
			new Zend_Controller_Router_Route_Regex(
				'hotel/(\d+)',
				array(
					'controller'	=> 'hotels',
					'action'		=> 'description'),
				array(
					'hotel' => 1),
				'hotel/%d'
			));
// =============================================================================
// ===== Список номеров гостиницы ==============================================
// =============================================================================
		$router->addRoute(
			'hotel_price',
			new Zend_Controller_Router_Route_Regex(
				'hotel/(\d+)/price',
				array(
					'controller'	=> 'hotels',
					'action'		=> 'price'),
				array(
					'hotel' => 1),
				'hotel/%d/price'
			));
// =============================================================================
// ===== Карта гостиницы =======================================================
// =============================================================================
		$router->addRoute(
			'hotel_map',
			new Zend_Controller_Router_Route_Regex(
				'hotel/(\d+)/map',
				array(
					'controller'	=> 'hotels',
					'action'		=> 'map'),
				array(
					'hotel' => 1),
				'hotel/%d/map'
			));
// =============================================================================
// ===== Номера ================================================================
// =============================================================================
// ===== Описание номера =======================================================
// =============================================================================
		$router->addRoute(
			'room_description',
			new Zend_Controller_Router_Route_Regex(
				'room/(\d+)',
				array(
					'controller'	=> 'hotels',
					'action'		=> 'room'),
				array(
					'room' => 1),
				'room/%d'
			));
// ========================================================================================================================================================================
// ===== Пользователь =====================================================================================================================================================
// ========================================================================================================================================================================
// ===== Информация о пользователе =============================================
// =============================================================================
		$router->addRoute(
			'user',
			new Zend_Controller_Router_Route_Regex(
				'user/(\d+)',
				array(
					'controller'	=> 'users',
					'action'		=> 'index'),
				array(
					'user' => 1),
				'user/%d'
			));
// =============================================================================
// ===== Поездки пользователя ====================================================
// =============================================================================
		$router->addRoute(
			'user_travels',
			new Zend_Controller_Router_Route_Regex(
				'user/(\d+)/travels',
				array(
					'controller'	=> 'users',
					'action'		=> 'travels'),
				array(
					'user' => 1),
				'user/%d/travels'
			));
// =============================================================================
// ===== Места пользователя ====================================================
// =============================================================================
		$router->addRoute(
			'user_places',
			new Zend_Controller_Router_Route_Regex(
				'user/(\d+)/places',
				array(
					'controller'	=> 'users',
					'action'		=> 'places'),
				array(
					'user' => 1),
				'user/%d/places'
			));
// =============================================================================
// ===== Города пользователя ====================================================
// =============================================================================
        $router->addRoute(
            'user_cities',
            new Zend_Controller_Router_Route_Regex(
                'user/(\d+)/cities',
                array(
                    'controller'	=> 'users',
                    'action'		=> 'cities'),
                array(
                    'user' => 1),
                'user/%d/cities'
            ));
// =============================================================================
// ===== Настройки пользователя ====================================================
// =============================================================================
		$router->addRoute(
			'user_options',
			new Zend_Controller_Router_Route_Regex(
				'user/(\d+)/options',
				array(
					'controller'	=> 'users',
					'action'		=> 'options'),
				array(
					'user' => 1),
				'user/%d/options'
			));

// ========================================================================================================================================================================
// ===== Одминка ==========================================================================================================================================================
// ========================================================================================================================================================================
// ===== Город =================================================================
// =============================================================================
		$router->addRoute(
			'odmin_city',
			new Zend_Controller_Router_Route_Regex(
				'odmin/city/(\d+)',
				array(
					'module'		=> 'odmin',
					'controller'	=> 'city',
					'action'		=> 'description'),
				array(
					'city' => 1),
				'odmin/city/%d'
			));
// =============================================================================
// ===== Место =================================================================
// =============================================================================
		$router->addRoute(
			'odmin_place',
			new Zend_Controller_Router_Route_Regex(
				'odmin/place/(\d+)',
				array(
					'module'		=> 'odmin',
					'controller'	=> 'places',
					'action'		=> 'description'),
				array(
					'place' => 1),
				'odmin/place/%d'
			));
// =============================================================================
// ===== Поездка ===============================================================
// =============================================================================
		$router->addRoute(
			'odmin_travel',
			new Zend_Controller_Router_Route_Regex(
				'odmin/travel/(\d+)',
				array(
					'module'		=> 'odmin',
					'controller'	=> 'travels',
					'action'		=> 'description'),
				array(
					'travel' => 1),
				'odmin/travel/%d'
			));
// =============================================================================
// ===== Гостиницы =============================================================
// =============================================================================
		$router->addRoute(
			'odmin_hotel',
			new Zend_Controller_Router_Route_Regex(
				'odmin/hotel/(\d+)',
				array(
					'module'		=> 'odmin',
					'controller'	=> 'hotels',
					'action'		=> 'description'),
				array(
					'hotel' => 1),
				'odmin/hotel/%d'
			));



		$router->addRoute(
			'auth_confirm',
			new Zend_Controller_Router_Route_Regex(
				'reg/confirm/(.*)',
				array(
					'controller'	=> 'reg',
					'action'		=> 'confirm'
				),
				array(
					'data' => 1
				),
				'auth/confirm/%d'
			));


	}

}
