<?php

class Api_Model_DbTable_Tmpimages extends Zend_Db_Table_Abstract {
	
    protected $_name = 'tmp_images';
    
	
	/*
	 * Получает запись по урлу
	 */
	public function getByUrl($url) {
		
		$select = $this->select()->where('url = (?)', $url);
		$res = $this->fetchAll($select);
		if ($res->count()) {
			return $res;
		}
		
		return false;
		
	}
	
}

