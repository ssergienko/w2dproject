<?php

class Api_Model_DbTable_Placeimages extends Application_Model_DbTable_Placeimages {

	protected $_name = 'place_images';

	
	/*
	 * Получает thumbnail места
	 */
	public function getThumbnail ($place_id, $main_image_url) {
		if (!empty($main_image_url)) {
			$select = $this->select()->where('pl_id = '.$place_id.' and middle_url = \''.$main_image_url."'");
			$res = $this->fetchRow($select);		
			if (!empty($res)) {
				return $res->thumbnail_url;
			}
		}
		return false;
	}
	
	
}
