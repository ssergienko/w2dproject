<?php

class Api_Model_DbTable_City extends Application_Model_DbTable_City {
	
    protected $_name = 'city';    
	
	
	
	/* 
	 * Добавляем методы 
	 */
	
	/*
	 * Метод для добавления гостиницы
	 * @param post (array) Массив данных
	 * @return result (integer) ID добавленного города
	 */
	public function add($data){
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)){
				return $result;
			}
		}
		return;
	}	
	
	
	/*
	 * Метод для добавления гостиницы
	 * @param post (array) Массив данных
	 * @return result (integer) ID добавленного города
	 */
	public function edit($id, $data){
		
		if(!empty($id) && !empty($data)){
			$where = "id = $id";
			$result = $this->update($data, $where);
			
			if($result){
				return true;
			}
		}
		return false;
	}
	
	
	/*
	 * Метод для удаления гостиницы
	 * @params id (integer) Id места
	 */
    public function delete($id)
    {		
		if(!empty($id)){
			// Получаем id как параметр
			$id = (int)$id;
			
			//$this->delete("id = $id"); // хз не работает
			$sql = "DELETE FROM ".$this->_name." WHERE id = $id";
			$this->getAdapter()->query($sql);
			return true;
		}        
		return false;
    }
}

