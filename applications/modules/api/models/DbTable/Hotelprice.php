<?php

class Api_Model_DbTable_Hotelprice extends Application_Model_DbTable_Hotelprice {

	protected $_name = 'hotelprice';

	/* 
	 * Добавляем методы 
	 */
	
	/*
	 * Добавляем запись номера для гостиницы
	 * @param post (array) Массив данных
	 * @return result (integer) ID добавленного номера
	 */
	public function add($data){
		if(!empty($data)){
			$result = $this->insert($data);
			if(!empty($result)){
				return $result;
			}
		}
		return;
	}
	
	/*
	 * Метод для редактирования номера гостиницы
	 * @param id (array) Id номера
	 * @param data (array) Массив данных
	 * @return (boolean)
	 */
	public function edit($id,$data){
		
		if(!empty($id) && !empty($data)){
			$where = "id = $id";
			$result = $this->update($data, $where);
			if($result){
				return true;
			}
		}
		return;
	}
	
	/*
	 * Метод для удаления номера
	 * @params id (integer) Id номера
	 */
    public function delete($id)
    {		
		if(!empty($id)){
			// Получаем id как параметр
			$id = (int)$id;
			
			//$this->delete("id = $id"); // хз не работает
			$sql = "DELETE FROM ".$this->_name." WHERE id = $id";
			$this->getAdapter()->query($sql);
			return true;
		}        
		return false;
    }
}
