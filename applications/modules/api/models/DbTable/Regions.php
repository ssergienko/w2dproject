<?php

class Api_Model_DbTable_Regions extends Zend_Db_Table_Abstract {
	
    protected $_name = 'regions';
    
	
	
	/*
	 * ПОлучает города у которых назавание похоже на $keyphrase
	 * 
	 * @author sergey.s.sergienko@gmail.com
	 * @params (string) $keyphrase
	 * 
	 * @return (array) travels
	 * 
	 */
	public function searchByPhrase ($keyphrase, $limit) {		
		
		$select = $this->select();
		
		$parts = array();
		$parts[] = "UPPER(name) LIKE UPPER('".$keyphrase."')";
		$parts[] = "UPPER(name) LIKE UPPER('%".$keyphrase."')";
		$parts[] = "UPPER(name) LIKE UPPER('".$keyphrase."%')";
		$parts[] = "UPPER(name) LIKE UPPER('%".$keyphrase."%')";
		$parts[] = implode(" or ", $parts);		
		
		$select->where(implode(" or ", $parts));
		
		$result = $this->fetchAll($select);
		
		if (!empty($result)) {
			return $result->toArray();
		}
		
		return false;
	}
	
	
	
}

