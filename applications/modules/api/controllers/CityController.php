<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_CityController extends Zend_Controller_Action {

	public function preDispatch() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}	
	
	/*
	 * Получает главную картинку места
	 */
	public function getmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['city_id'])) {
			$city_table = new Api_Model_DbTable_City();
			$city = $city_table->getById($params['city_id']);
			
			if (!empty($city['image_url'])) {
				echo $this->_helper->json(array('image_url' => $city['image_url']));
			}
			
			echo $this->_helper->json(array('error' => 'failed to obtain'));
		}
		
		echo $this->_helper->json(array('error' => 'empty params'));
		
	}
	
	
	
	/*
	* Задает главную картинку для вывода в списках
	**/
	public function setmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['city_id']) && !empty($params['middle_url'])) {
			
			$data = array(
				'image_url' => $params['middle_url'],
			);
			
			$city_table = new Api_Model_DbTable_City();
			$city_table->update($data, 'id = '.$params['city_id']);
			
			echo $this->_helper->json($params['city_id']);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}


    /*
	 * Сохраняет текст для картинки
	 */
    public function saveimagetxtAction () {

        // Обрабатка GET данных
        $params = $this->getRequest()->getParams();

        if (!empty($params['city_id']) && !empty($params['image_url'])) {

            $data = array(
                'image_txt' => !empty($params['image_txt']) ? $params['image_txt'] : '',
                'idx' => !empty($params['idx']) ? $params['idx'] : 0
            );

            $placeimages_table = new Api_Model_DbTable_Cityimages();
            $placeimages_table->update($data, 'cy_id = '.$params['city_id'].' and url = \''.$params['image_url'].'\'');

            echo $this->_helper->json(array('success' => 1));

        } else {
            echo $this->_helper->json(array('error' => 'empty params'));
        }

    }

	
	public function addcityimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['city_id']) && !empty($params['url']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Cityimages();

			$data = array(
				'cy_id' => $params['city_id'],
				'name' => $params['name'],
				'url' => $params['url'],
				'middle_url' => !empty($params['middle_url']) ? $params['middle_url'] : '',
				'thumbnail_url' => !empty($params['thumbnail_url']) ? $params['thumbnail_url'] : '',
				'delete_url' => !empty($params['delete_url']) ? $params['delete_url'] : ''
			);
			$tmp_table->insert($data);
			
			echo $this->_helper->json($data);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
	
	/*
	 * Удаляет картинки из города
	 */
    public function deletecityimgAction()
    {
		
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['city_id']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Cityimages();
			$tmp_table->delete('cy_id = '.$params['city_id'].' and delete_url = \''.$params['delete_url']."'");
			
			echo json_encode(array('success' => 'tmp row deleted'));
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
	}
	
	
	
}

?>
