<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_TravelsController extends Zend_Controller_Action {

	
	
	public function preDispatch() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	
	
	
	/*
	 * Получает главную картинку места
	 */
	public function getmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['travel_id'])) {
			$travel_table = new Api_Model_DbTable_Travels();
			$travel = $travel_table->getById($params['travel_id']);
			
			if (!empty($travel['image_url'])) {
				echo $this->_helper->json(array('image_url' => $travel['image_url']));
			}
			
			echo $this->_helper->json(array('error' => 'failed to obtain'));
		}
		
		echo $this->_helper->json(array('error' => 'empty params'));
		
	}
	
	
	
	/*
	* Задает главную картинку для вывода в списках
	**/
	public function setmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['travel_id']) && !empty($params['middle_url'])) {
			
			$data = array(
				'image_url' => $params['middle_url'],
			);
			
			$travel_table = new Api_Model_DbTable_Travels();
			$travel_table->update($data, 'id = '.$params['travel_id']);
			
			echo $this->_helper->json($params['travel_id']);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}

	
	
	
	/*
	 * Сохраняет текст для картинки
	 */
	public function saveimagedataAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['travel_id']) && !empty($params['image_url'])) {
			
			$data = array(
				'image_idx' => $params['image_idx'],
				'image_txt' => !empty($params['image_txt']) ? $params['image_txt'] : ''
			);
			
			$travelimages_table = new Api_Model_DbTable_Travelimages();
			$travelimages_table->update($data, 'tr_id = '.$params['travel_id'].' and url = \''.$params['image_url'].'\'');
			
			echo $this->_helper->json(array('success' => 1));
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
	
	/*
	 * Записывает картинки мест
	 */
	public function addtrimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['travel_id']) && !empty($params['url']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Travelimages();

			$data = array(
				'tr_id' => $params['travel_id'],
				'name' => $params['name'],
				'url' => $params['url'],
				'middle_url' => !empty($params['middle_url']) ? $params['middle_url'] : '',
				'thumbnail_url' => !empty($params['thumbnail_url']) ? $params['thumbnail_url'] : '',
				'delete_url' => !empty($params['delete_url']) ? $params['delete_url'] : ''
			);
			$tmp_table->insert($data);
			
			echo $this->_helper->json($data);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
	
	public function addplacetotravelAction(){
		
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();
		if(!empty($get['ajax']) && !empty($get['place']) && isset($get['travel'])){
			
			$travelplace_table = new Api_Model_DbTable_Travelplace();
			
			$data = array(
				't_id' => $get['travel'],
				'p_id' => $get['place']
			);
			
			$result = $travelplace_table->add($data);
			if(!empty($result)){
				
				echo $this->_helper->json(array($result));
				exit();
			}
		}
		echo $this->_helper->json(array(false));
	}
	
	
	
	public function travelfavAction(){
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		
		if(!empty($user)){
			
			if(!empty($get['ajax']) && !empty($get['travel'])){
			
				$travelFav_table = new Api_Model_DbTable_TravelFav();
	
				$result = $travelFav_table->check($get['travel'], $user['id']);
				
				if(empty($result)){
					$data = array(
						't_id' => $get['travel'],
						'u_id' => $user['id']
					);

					$result = $travelFav_table->add($data);
					if(!empty($result)){
						
						/*$oCache = Zend_Registry::getInstance()->oCache;
						// *******Сохраниение****** //
						// Подключаемся к модели
						$ratinger = new Application_Model_MemcacheCounter();
						// Передаем в метод айдишник записи - это должно быть что то типа user_23 или travel_88 или place_43
						$res = $ratinger->increment('travel_'.$get['travel']);*/
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
	
	public function travelunfavAction(){
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		
		if(!empty($user)){
			
			if(!empty($get['ajax']) && !empty($get['travel'])){
			
				$travelFav_table = new Api_Model_DbTable_TravelFav();
	
				$result = $travelFav_table->check($get['travel'], $user['id']);
				
				if(!empty($result)){
					$data = array(
						't_id' => $get['travel'],
						'u_id' => $user['id']
					);

					$result = $travelFav_table->deleteByData($data);
					if(!empty($result)){
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
	public function travellikeAction(){
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		
		if(!empty($user)){
			
			if(!empty($get['ajax']) && !empty($get['travel'])){
			
				$travelLike_table = new Api_Model_DbTable_TravelLike();
	
				$result = $travelLike_table->check($get['travel'], $user['id']);
				
				if(empty($result)){
					$data = array(
						't_id' => $get['travel'],
						'u_id' => $user['id']
					);

					$result = $travelLike_table->add($data);
					if(!empty($result)){
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
	public function travelunlikeAction(){
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		
		if(!empty($user)){
			
			if(!empty($get['ajax']) && !empty($get['travel'])){
			
				$travelLike_table = new Api_Model_DbTable_TravelLike();
	
				$result = $travelLike_table->check($get['travel'], $user['id']);
				
				if(!empty($result)){
					$data = array(
						't_id' => $get['travel'],
						'u_id' => $user['id']
					);

					$result = $travelLike_table->deleteByData($data);
					if(!empty($result)){
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
}

?>
