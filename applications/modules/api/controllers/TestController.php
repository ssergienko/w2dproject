<?php

/*
 * Тестовый контроллер. По его подобию делать контроллеры для доступа к базе через javascript
 * 
 * @author sergey.s.sergienko@gmail.com
 */

class Api_TestController extends Zend_Controller_Action {	
	
	public function preDispatch()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	/*
	 * 
	 */
	public function indexAction() {
		
		// Принимаем параметры
		$peremennaya = $this->_getParam('peremennaya');
		$itd = $this->_getParam('itd');
		
		// Тестовый массив
		$array = array(
					'fiealds' => array(
						'itd' => $itd,
						'peremennaya' => '!!!!!!!!!!!!!'.$peremennaya.'!!!!!!!!!!!!!!!!'
					)
				);
		
		 echo $this->_helper->json($array);
		
	}	

}

?>
