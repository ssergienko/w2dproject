<?php
/**
*  @filesource
*  Класс IndexController для модуля
* 
*  
*  @author Sergey S. Sergienko
*/

class Api_StorageController extends Zend_Controller_Action
{  
	
	// @todo вынести в bootstrap
	public function init () {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	/*
	 * 
	 */
    public function indexAction()
    {

    }
	
	
	
	/*
	 * Удаляет записи из places_images
	 */
    public function deleteplAction()
    {

		$params = $this->getRequest()->getParams();
		
		if (!empty($params['place_id']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Placeimages();
			$tmp_table->delete('pl_id = '.$params['place_id'].' and delete_url = \''.$params['delete_url']."'");
			
			echo json_encode(array('success' => 'tmp row deleted'));
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
	}


    /*
	 * Удаляет записи из city_images
	 */
    public function deletecityAction()
    {

        $params = $this->getRequest()->getParams();

        if (!empty($params['city_id']) && !empty($params['delete_url'])) {

            $tmp_table = new Api_Model_DbTable_Cityimages();
            $tmp_table->delete('cy_id = '.$params['city_id'].' and delete_url = \''.$params['delete_url']."'");

            echo json_encode(array('success' => 'tmp row deleted'));

        } else {
            echo json_encode(array('error' => 'empty params'));
        }

    }

	
	/*
	 * Удаляет записи из places_images
	 */
    public function deletetrAction()
    {

		$params = $this->getRequest()->getParams();
		
		if (!empty($params['travel_id']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Travelimages();
			$tmp_table->delete('tr_id = '.$params['travel_id'].' and delete_url = \''.$params['delete_url']."'");
			
			echo json_encode(array('success' => 'tmp row deleted'));
			
		} else {
			echo json_encode(array('error' => 'empty params'));
		}
		
	}
	
	
	
}
