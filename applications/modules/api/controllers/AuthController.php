<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_AuthController extends Zend_Controller_Action {

	public function preDispatch() {
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	/*
	 * Получает главную картинку места
	 */
	public function nativeAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		$auth = new Zend_Session_Namespace('auth');
		
		if (!empty($params['email']) && !empty($params['pass'])) {
			
			$users_table = new Api_Model_DbTable_Users();
			$user = $users_table->getByEmailAndPass($params['email'], $params['pass']);
			
			if (!empty($user)) {
				// Если логин подтвержден
				//if ($users_table->checkConfirm($user['id'])) {
					$auth->uid = $user['id'];
					echo $this->_helper->json(array('success' => $user['id']));
				//} else {
				//	echo $this->_helper->json(array('error' => 'not confirmed'));
				//}
			} else {
				echo $this->_helper->json(array('error' => 'auth failed'));
			}
			
		} else {		
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
	
	/*
	 * Отправка напоминания пароля
	 */
	public function sendpasswdAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		if (!empty($params['email'])) {
			$users_table = new Api_Model_DbTable_Users();
			$user = $users_table->getByEmail($params['email']);
			if (!empty($user->pass)) {
				
				// Отправляем письмо для подтверждения
				$mailer = new Application_Model_Email();
				$mailer->sendPassword($user);
				
				echo $this->_helper->json(array('success' => $user->uemail));
				
			} else {
				echo $this->_helper->json(array('error' => 'Пароль для пользователя с таким email еще не был создан'));
			}
		} else {
			echo $this->_helper->json(array('error' => 'Пользователь с таким email не существует'));
		}
		
	}
	
	
	
}

?>
