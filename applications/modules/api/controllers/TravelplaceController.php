<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_TravelplaceController extends Zend_Controller_Action {

	
	
	public function preDispatch() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	
	
	public function changedayAction() {
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();

		if (!empty($params['ajax']) && !empty($params['travel']) && !empty($params['place']) && isset($params['day']) ) {
			
			$travelplace_table = new Api_Model_DbTable_Travelplace();

			$result = $travelplace_table->chooseday($params['travel'],$params['place'],$params['day']);
			if($result === true){
				// Пытаемся получить пользователя
				$auth = new Application_Model_Auth();
				$user = $auth->check();
				
				
				// Подключаем мемкэш
				$memcache = Zend_Registry::getInstance()->oCache;
				$memcache->remove("travel_".$params['travel']."_plan");
				$memcache->remove("travel_".$params['travel']."_plan_user_".$user['id']);
				
				echo $this->_helper->json(array('success' => $result));
				exit();
			}
			echo $this->_helper->json(array('error' => 'result'));
			exit();
			
		} else {
			echo $this->_helper->json(array('error' => 'ajax'));
		}
	}
}

?>
