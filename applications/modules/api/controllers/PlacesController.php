<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_PlacesController extends Zend_Controller_Action {

	
	
	public function preDispatch() {
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	
	
	/*
	 * Получает главную картинку места
	 */
	public function getmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['place_id'])) {
			$place_table = new Api_Model_DbTable_Places();
			$place = $place_table->getById($params['place_id']);
			
			if (!empty($place['image_url'])) {
				echo $this->_helper->json(array('image_url' => $place['image_url']));
			}
			
			echo $this->_helper->json(array('error' => 'failed to obtain'));
		}
		
		echo $this->_helper->json(array('error' => 'empty params'));
		
	}
	
	
	
	/*
	* Задает главную картинку для вывода в списках
	**/
	public function setmainimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['place_id']) && !empty($params['middle_url'])) {
			
			$data = array(
				'image_url' => $params['middle_url'],
			);
			
			$place_table = new Api_Model_DbTable_Places();
			$place_table->update($data, 'id = '.$params['place_id']);
			
			echo $this->_helper->json($params['place_id']);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}

	
	
	/*
	 * Сохраняет текст для картинки
	 */
	public function saveimagetxtAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['place_id']) && !empty($params['image_url'])) {
			
			$data = array(
				'image_txt' => !empty($params['image_txt']) ? $params['image_txt'] : '',
				'idx' => !empty($params['idx']) ? $params['idx'] : 0
			);
			
			$placeimages_table = new Api_Model_DbTable_Placeimages();
			$placeimages_table->update($data, 'pl_id = '.$params['place_id'].' and url = \''.$params['image_url'].'\'');
			
			echo $this->_helper->json(array('success' => 1));
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
	
	
	/*
	 * Записывает картинки мест
	 */
	public function addplimageAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if (!empty($params['place_id']) && !empty($params['url']) && !empty($params['delete_url'])) {
		
			$tmp_table = new Api_Model_DbTable_Placeimages();

			$data = array(
				'pl_id' => $params['place_id'],
				'name' => $params['name'],
				'url' => $params['url'],
				'middle_url' => !empty($params['middle_url']) ? $params['middle_url'] : '',
				'thumbnail_url' => !empty($params['thumbnail_url']) ? $params['thumbnail_url'] : '',
				'delete_url' => !empty($params['delete_url']) ? $params['delete_url'] : ''
			);
			$tmp_table->insert($data);
			
			echo $this->_helper->json($data);
			
		} else {
			echo $this->_helper->json(array('error' => 'empty params'));
		}
		
	}
	
	
		
	/*
	 * ПОлучает список городов для автокомплита
	 */
	public function getcityAction () {
		
		$parts = array();
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		// Сначала выбираем города которые уже есть на сайте
		$city_table = new Api_Model_DbTable_City();
		$result = $city_table->searchByPhrase($params['q']);
		
		if (!empty($result)) {				
			foreach ($result as $city) {
				$parts[] = $city['title'];
			}			
		}
			
		// Если мненьше 5ти результатов
		if (empty($result) || count($result) < 5) {
			
			// ДОбавляем недостающие
			$regions_table = new Api_Model_DbTable_Regions();
			$result2 = $regions_table->searchByPhrase($params['q'], 5-count($result));
			
			if (!empty($result2)) {				
				foreach ($result2 as $city) {
					if (!in_array($city['name'], $parts)) {
						$parts[] = $city['name'];
					}
				}			
			}
		}
		
		if (!empty($parts)) {
			echo implode("\r\n", $parts);
			exit();
		}
		
		echo 'empty';
		
	}
	
	
	
	public function addtotravelAction(){
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		if(!empty($params['place']) && isset($params['travel'])){
			
			$travelplace_table = new Api_Model_DbTable_Travelplace();
			
			if(empty($params['travel'])){
				$result = $travelplace_table->addPlaceToMyTravel($params['travel'],$params['place'],$params['title']);
			}
			else{
				$result = $travelplace_table->addPlaceToMyTravel($params['travel'],$params['place']);
			}
			
			if(!empty($result)) {
				echo $this->_helper->json(array('success' => $result));
				exit();
			}
			
		}
		echo $this->_helper->json(array('error' => 'ajax'));
	}
	
	
	public function placefavAction(){
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		if(!empty($user)){
			
			if(!empty($params['ajax']) && !empty($params['place'])){
		
				$placeFav_table = new Api_Model_DbTable_PlaceFav();
	
				$result = $placeFav_table->check($params['place'], $user['id']);
					
				if(empty($result)){
					
					$data = array(
						'p_id' => $params['place'],
						'u_id' => $user['id']
					);

					$result = $placeFav_table->add($data);
					
					if(!empty($result)){
					
						// Получаем данные по месту
						$place_table = new Api_Model_DbTable_Places();
						$result = $place_table->getById($params['place']);
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
	
	
	//public function placeUnFavAction(){
	public function placeunfavAction(){
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		if(!empty($user)){
			
			if(!empty($params['ajax']) && !empty($params['place'])){
		
				$placeFav_table = new Api_Model_DbTable_PlaceFav();
	
				$result = $placeFav_table->check($params['place'], $user['id']);
					
				if(!empty($result)){
					
					$data = array(
						'p_id' => $params['place'],
						'u_id' => $user['id']
					);

					$result = $placeFav_table->deleteByData($data);
					
					if(!empty($result)){
					
						// Получаем данные по месту
						$place_table = new Api_Model_DbTable_Places();
						$result = $place_table->getById($params['place']);
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	public function placelikeAction(){
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		if(!empty($user)){
			
			if(!empty($params['ajax']) && !empty($params['place'])){
		
				$placeLike_table = new Api_Model_DbTable_PlaceLike();
	
				$result = $placeLike_table->check($params['place'], $user['id']);
					
				if(empty($result)){
					
					$data = array(
						'p_id' => $params['place'],
						'u_id' => $user['id']
					);

					$result = $placeLike_table->add($data);
					if(!empty($result)){
						
						// Получаем данные по месту
						$place_table = new Api_Model_DbTable_Places();
						$result = $place_table->getById($params['place']);
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));		
	}
	
	
	
	
	
	//public function placeUnFavAction(){
	public function placeunlikeAction(){
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();
		
		$auth = new Application_Model_Auth();
		$user = $auth->check();
		
		if(!empty($user)){
			
			if(!empty($params['ajax']) && !empty($params['place'])){
		
				$placeLike_table = new Api_Model_DbTable_PlaceLike();
	
				$result = $placeLike_table->check($params['place'], $user['id']);
					
				if(!empty($result)){
					
					$data = array(
						'p_id' => $params['place'],
						'u_id' => $user['id']
					);

					$result = $placeLike_table->deleteByData($data);
					if(!empty($result)){
						
						echo $this->_helper->json(array('success' => $result));
						exit();
					}
					echo $this->_helper->json(array('error' => 'result'));
					exit();
				}
				echo $this->_helper->json(array('success' => 'already'));
				exit();
			}
			echo $this->_helper->json(array('error' => 'ajax'));
			exit();
		}		
		echo $this->_helper->json(array('error' => 'auth'));
	}
	
	
	
	/*
	 * 
	 * Получает места по координатам. 
	 * По умолчанию 60 миль.
	 * 
	 */
	public function getbycoordsAction() {

		$params = $this->getRequest()->getParams();
		
		$places_table = new Application_Model_DbTable_Places();
		$places = $places_table->getPlacesByCoords($params['lat'], $params['lng']);
		
		// Взято 1 градус = 60 миль
		if (!empty($places)) {
			return $this->_helper->json($places);
		}		
		echo $this->_helper->json(array('error' => 'do not have places'));
		
	}
	
	
	
	/*
	 * 
	 * Получает места по айдишникам. 
	 * По умолчанию 60 миль.
	 * 
	 */
	public function getbyidAction() {

		$params = $this->getRequest()->getParams();
		
		$places_table = new Application_Model_DbTable_Places();
		$places = $places_table->getById($params['place_id']);
		
		// Взято 1 градус = 60 миль
		if (!empty($places)) {
			return $this->_helper->json($places);
		}		
		echo $this->_helper->json(array('error' => 'do not have places'));
		
	}
	
	
	
}

?>
