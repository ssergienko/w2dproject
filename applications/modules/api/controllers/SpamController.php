<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_SpamController extends Zend_Controller_Action {

	public function preDispatch() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);		
	}

	/*
	 * Сохранение номера
	 */
	public function sendAction() {
		
		// Вытаскиваем конфиг из реестра
		$config = Zend_Registry::getInstance()->config;
		$email_message = $config->emails->message;
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();

		$error = array();
		
		// Проверяем целостность данных
		// Если не задан тип отправителя
		if(empty($params['type'])) $error[] = "Выберите отправителя(ей)";
		
		// Если тип отправителя - адресаты, проверяем каждый Email на валидность
		if($params['type'] == 'email'){
			if(empty($params['email_list'])){
				$error[] = "Укажите хотя бы одного отправителя";
			}
			else{
				$email_list = array();
				$email_list_error = 0;
				
				foreach($params['email_list'] as $list){
					if(preg_match("/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i",$list)){
						$arr = array(
							'name' => $list,
							'email' => $list,
						);
						$email_list[] = $arr;						
					}
					else{
						$email_list_error = 1;
					}
				}
				
				if($email_list_error == 1){
					$error[] = "Ошибка валидности email(s)";
				}				
			}
		}
		
		// Если не задан заголовое письма
		if(empty($params['email_title'])) $error[] = "Введите заголовок письма";
		
		// Если прошли проверку на целостность данных
		if(empty($error)){
			// Выбираем пользователей для получения по типу отправителя
			switch($params['type']){
				case 'all':
					$table = new Application_Model_DbTable_Users();
					$result = $table->getList();
					if(!empty($result)){
						foreach($result as $row){
							$arr = array(
								'name' => $row['uname'],
								'email' => $row['email'],
							);
							$email_list[] = $arr;
						}
					}
					break;
				case 'promo':
					$email_list = array();
					
					$table = new Odmin_Model_DbTable_UserList();
					$result = $table->getList();
					if(!empty($result)){
						foreach($result as $row){
							$arr = array(
								'name' => $row['email'],
								'email' => $row['email'],
							);
							$email_list[] = $arr;							
						}
					}					
					break;
				default:
					$email_list = !empty($email_list)?$email_list:array();
			}
			
			// Если не пустой список получателей
			if(!empty($email_list)) {
				
				// Отправляем промо код
				if($params['typeEmail'] == 'promoUnicId') {
					
					$promo_table = new Odmin_Model_DbTable_Promo();
					
					$message = new Odmin_Model_Message();
					$message_error = 0;
					
					foreach($email_list as $list) {
						
						$title = preg_replace('/%username%/',$list['name'],$params['email_title']);
						
						$promo_data = $promo_table->checkForEmail($list['email']);
						
						if(!empty($promo_data)) {
							
							if(empty($promo_data['unic_id'])) {
								$promo_data['unic_id'] = $promo_table->setSingleKey($list['email']);
							}
							
							if(!empty($promo_data['unic_id'])) {
								
								// Достаем шаблон
								$html_view = new Zend_View();
								$html_view->setScriptPath(APPLICATION_PATH . '/modules/odmin/views/emails/');

								// Подставляем данные в шаблон
								$html_view->assign('unic_id', $promo_data['unic_id']);
								if(!empty($params['email_body'])){
									$html_view->assign('html', $params['email_body']);
								}
								if(!empty($params['email_footer'])){
									$html_view->assign('footer', $params['email_footer']);
								}	

								// Рендерим шаблон
								$html = $html_view->render('promo.phtml');

								$data = array(
									'email_title' => $title,
									'email_body' => $html,						
									'from' => $email_message,
									'to' => $list['email'],
								);
								$result = $message->send($data);
								if(!$result){
									$message_error = 1;
								}
							}
						}
					}
					
				} 
				// Простая отправка
				else {
					
					// Достаем шаблон
					$html_view = new Zend_View();
					$html_view->setScriptPath(APPLICATION_PATH . '/modules/odmin/views/emails/');
				
					// Подставляем данные в шаблон
					
					if(!empty($params['email_body'])){
						$html_view->assign('html', $params['email_body']);
					}
					if(!empty($params['email_footer'])){
						$html_view->assign('footer', $params['email_footer']);
					}				

					// Рендерим шаблон
					$html = $html_view->render('index.phtml');
					
					$message = new Odmin_Model_Message();
					$message_error = 0;

					foreach($email_list as $list) {

						$title = preg_replace('/%username%/',$list['name'],$params['email_title']);

						$data = array(
							'email_title' => $title,
							'email_body' => $html,						
							'from' => $email_message,
							'to' => $list['email'],
						);
						$result = $message->send($data);
						if(!$result){
							$message_error = 1;
						}					
					}
				}
				
				if($message_error == 1){
					$error[] = "Не всем пользователям было отправлено сообщение";					
				}				
			}
		}
		if(empty($error)){
			echo $this->_helper->json(array('success'=>'Соообщение успешно отправлено'));
		}
		else{
			echo $this->_helper->json(array('error'=>$error));
		}	
	}
}

?>
