<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author mr.debugg
 */
class Api_HotelsController extends Zend_Controller_Action {

	public function preDispatch() {
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
	}

	
	
	public function savelogoAction () {
		
		// Обрабатка GET данных
		$params = $this->getRequest()->getParams();		
		$options = array('image_url' => $params['logo_url']);		
		
		$hotel_obj = new Api_Model_DbTable_Hotels();		
		$res = $hotel_obj->edit($params['id'], $options);
		
		echo true;
	}
	
	
	
	/*
	 * Сохранение номера
	 */
	public function roomsaveAction() {
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();

		if (!empty($get['ajax']) && !empty($get['id']) && !empty($get['title']) && isset($get['long_text']) &&  isset($get['price'])) {
			$hotelprice_table = new Api_Model_DbTable_Hotelprice();
			
			$data = array(
				'title' => $get['title'],
				'long_text' => $get['long_text'],
				'price' => $get['price']
			);
			
			$result = $hotelprice_table->edit($get['id'], $data);
			if(!empty($result)){
				echo 1;
				//echo $this->_helper->json(array(1));
			}
			
		} else {
			//echo $this->_helper->json(array());
		}
	}

	/*
	 * Удаление номера
	 */
	public function roomdeleteAction() {
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();

		if (!empty($get['ajax']) && !empty($get['id'])) {
			$hotelprice_table = new Api_Model_DbTable_Hotelprice();
			
			$result = $hotelprice_table->delete($get['id']);
			if(!empty($result)){
				echo 1;
				//echo $this->_helper->json(array(1));
			}			
			
		} else {
			//echo $this->_helper->json(array());
		}
	}
	
	/*
	 * Сохранение номера
	 */
	public function roomaddAction() {
		// Обрабатка GET данных
		$get = $this->getRequest()->getParams();

		if (!empty($get['ajax']) && !empty($get['h_id']) && !empty($get['title']) && isset($get['long_text']) &&  isset($get['price'])) {
			$hotelprice_table = new Api_Model_DbTable_Hotelprice();
			
			$data = array(
				'h_id' => $get['h_id'],
				'title' => $get['title'],
				'long_text' => $get['long_text'],
				'price' => $get['price']
			);
			
			$result = $hotelprice_table->add($data);
			if(!empty($result)){
				echo $result;
				//echo $this->_helper->json(array(1));
			}
			
		} else {
			//echo $this->_helper->json(array());
		}
	}
}

?>
