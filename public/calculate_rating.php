<?php

header('Content-type: text/html; charset=utf-8');
date_default_timezone_set("Europe/Moscow");

define('RATING_OPERATION', 'calculate rating');

if(!empty($_SERVER['ProgramFiles'])) {
	// Define path to application directory
	defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../applications'));

	// Define application environment
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	// Ensure library/ is on include_path
	set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library'),
		realpath(APPLICATION_PATH . '/../applications/plugins')
		//,get_include_path()
	)));
} else {
	define('CRON_START', TRUE);
	
	// Define path to application directory
	defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', '/var/www/devel.way2day.ru/applications');
		//|| define('APPLICATION_PATH', '../applications');

	// Define application environment
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	// Ensure library/ is on include_path
	set_include_path(implode(PATH_SEPARATOR, array(
		'/var/www/devel.way2day.ru/library',
		'/var/www/devel.way2day.ru/applications/plugins'		
	)));
}

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->session = new Zend_Session_Namespace();

$application->bootstrap();//->run();

/*
 * ======================================================================================================
 * ======================= CALCULATE RATING =============================================================
 * ======================================================================================================
 */

$places = array();		
$travels = array();
$city = array();

$placeFav_table = new Application_Model_DbTable_PlaceFav();
$travelFav_table = new Application_Model_DbTable_TravelFav();		
$travelplace_table = new Application_Model_DbTable_Travelplace();

$places_table = new Application_Model_DbTable_Places();
$travels_table = new Application_Model_DbTable_Travels();
$city_table = new Application_Model_DbTable_City();

// Добавление мест в избранные
$placeFav = $placeFav_table->getList();		
if(!empty($placeFav)){
	foreach($placeFav as $fav){
		if(isset($places[$fav['p_id']]))$places[$fav['p_id']]++;
		else $places[$fav['p_id']] = 1;
	}
}	

// Добавление поездок в избранные
$travelFav = $travelFav_table->getList();	

if(!empty($travelFav)){
	foreach($travelFav as $fav){
		if(isset($travels[$fav['t_id']])) {
			$travels[$fav['t_id']]++;			
		} else { 
			$travels[$fav['t_id']] = 1;			
		}
	}
}

// Добавление мест в поездки
$travelplace = $travelplace_table->getList();
if(!empty($travelplace)){
	foreach($travelplace as $tp){
		if(!empty($places[$tp['p_id']])){
			$places[$tp['p_id']]++;						
		}				
	}			
	foreach($travelplace as $tp){
		if(!empty($places[$tp['p_id']])){
			if(!empty($travels[$tp['t_id']])) $travels[$tp['t_id']] +=	$places[$tp['p_id']];
			else $travels[$tp['t_id']] =	$places[$tp['p_id']];
		}				
	}
}

// Рейтинг для городов
foreach($places as $key=>$value){
	$result = $places_table->getCity($key);
	if(!empty($result)){
		if(!empty($city[$result])) $city[$result] += $value;
		else $city[$result] = $value;
	}
}

// Обновление рейтинга
if(!empty($places)){
	foreach($places as $key=>$value){
		$places_table->updateRating($key, $value, 'refresh');
	}
}
if(!empty($travels)){
	foreach($travels as $key=>$value){
		$travels_table->updateRating($key, $value, 'refresh');
	}
}
if(!empty($city)){
	foreach($city as $key=>$value){
		$city_table->updateRating($key, $value, 'refresh');
	}
}


// Очистка memcache
$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect to memcached server");

$allSlabs = $memcache->getExtendedStats('slabs');
foreach($allSlabs as $server => $slabs){
	foreach($slabs AS $slabId => $slabMeta){
		$cdump = $memcache->getExtendedStats('cachedump',(int)$slabId);
		foreach($cdump AS $keys => $arrVal){
			if ($arrVal) {
				foreach($arrVal AS $k => $v){
					if(preg_match('/_rating_(\d*)/',$k)){
						$memcache->delete($k);
					}
				}
			}
		}
	}
}

// Открываем LOG файл для записи
$fileName = "log_calculaterating.txt";
if(!empty($_SERVER['REMOTE_ADDR'])) {
	$remout_ip = $_SERVER['REMOTE_ADDR'];
	
} elseif(!empty($_SERVER['SSH_CLIENT'])) {
	$remout_ip = $_SERVER['SSH_CLIENT'];	
	
}
if(!empty($remout_ip)) {
	preg_match('/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $remout_ip, $match);
	$subnet = $match[1];
	if ($subnet != '127.0.0' && $subnet != '172.16.0') {
		$fileName = "/var/www/devel.way2day.ru/public/log_calculaterating.txt";
		
	}
}
	
$fh = fopen($fileName, 'a+');

fwrite($fh, "\n Calculate Rating ".date("Y-m-d H:i:s")."\n====================================\n");
$array = array('places', 'travels', 'citys');
foreach($array as $row) {
	switch($row) {
		case 'places': $resourse = $places; break;
		case 'travels': $resourse = $travels; break;
		case 'citys': $resourse = $city; break;
		default: $resourse = array(); break;
	}
	if(!empty($resourse)) {
		$title = "\n==== $row ====\n";
		fwrite($fh, $title);
		echo $title;
		
		foreach($resourse as $key=>$value) {
			$text = "id [$key] => rating [$value]\n";
			fwrite($fh, $text);
			echo $text;
		}
		echo "\n";
	}
}
fclose($fh);
