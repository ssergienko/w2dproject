<?php
/* 
 * Отправщик изображений в хранилище
 */

header('Content-type: text/plain; charset=utf-8');

if(!isset ($_FILES['file']))  {	
	echo 'File is empty';
	exit();
}

// Get file element
$file = $_FILES['file'];

// If file not found
if (empty($file)) {

	// Check lenght exceeds (2M)
	if( $_SERVER['CONTENT_LENGTH'] > 2*1024*1024 ) {
		echo 'The uploaded file exceeds the maximum allowed size for posts';
		exit();
	}

	// Invalid request parameters: file
	echo 'Invalid request parameters: file';
	exit();

}

// Check error
switch (@$file['error']) {

	// All OK, do nothing here
	case UPLOAD_ERR_OK:
		break;

	// Max size
	case UPLOAD_ERR_INI_SIZE:
	case UPLOAD_ERR_FORM_SIZE:
		echo 'The uploaded file exceeds the maximum allowed size';
		exit();
		break;

	// Upload canceled
	case UPLOAD_ERR_PARTIAL:
		echo 'The uploaded file was only partially uploaded';
		exit();
		break;

	// No file
	case UPLOAD_ERR_NO_FILE:
		echo 'No data was uploaded';
		exit();
		break;

	// No TMP dir
	case UPLOAD_ERR_NO_TMP_DIR:
		echo 'Internal server error: missing the folder for temporary files';
		exit();
		break;

	// No rights
	case UPLOAD_ERR_CANT_WRITE:
		echo 'Internal server error: failed to write file to disk';
		exit();
		break;

	// Wrong extension
	case UPLOAD_ERR_EXTENSION:
		echo 'Internal server error: file upload stopped by extension';
		exit();
		break;

	// Unknown error
	default:
		echo 'Unknown error';
		exit();
		break;
}

// Get params
$params = array(
	'type' => isset($_POST['type']) ? $_POST['type'] : 'image',
	'resize' => isset($_POST['resize']) && $_POST['resize'] == 1 ? 1 : 0,
	'x' => isset($_POST['x']) && (int)$_POST['x'] > 1 ? (int)$_POST['x'] : 150,
	'y' => isset($_POST['y']) && (int)$_POST['y'] > 1 ? (int)$_POST['y'] : 150,
	'force' => isset($_POST['force']) && $_POST['force'] == 1 ? 1 : 0
);

// Check type
switch($params['type']) {
	
	case 'image':
	case 'file':		
		// do nothing
		break;
	
	// Unknown type
	default:
		echo 'Unknown type';
		exit();
		break;
	
}

// Prepare request data
$data = array(
	'file' => base64_encode(file_get_contents($file['tmp_name'])),
	'type' => $params['type'],
	'resize' => $params['resize'],
	'x' => $params['x'],
	'y' => $params['y'],
	'force' => $params['force']
);

$xml_file_path = 'http://storage.way2day.ru/upload/file.xml';

// Make request
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $xml_file_path);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$content = curl_exec($ch);
curl_close($ch);

$xml = @simplexml_load_string($content);

if( @(string)$xml->error ) {
	echo (string)$xml->error;
	exit();
}

echo (string)$xml->url;