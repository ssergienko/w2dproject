<?php

header('Content-type: text/html; charset=utf-8');
date_default_timezone_set("Europe/Moscow");

define('RATING_OPERATION', 'calculate rating');

if(!empty($_SERVER['ProgramFiles'])) {
	// Define path to application directory
	defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../applications'));

	// Define application environment
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	// Ensure library/ is on include_path
	set_include_path(implode(PATH_SEPARATOR, array(
		realpath(APPLICATION_PATH . '/../library'),
		realpath(APPLICATION_PATH . '/../applications/plugins')
		//,get_include_path()
	)));
} else {
	define('CRON_START', TRUE);
	
	// Define path to application directory
	defined('APPLICATION_PATH')
		|| define('APPLICATION_PATH', '/var/www/devel.way2day.ru/applications');
		//|| define('APPLICATION_PATH', '../applications');

	// Define application environment
	defined('APPLICATION_ENV')
		|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

	// Ensure library/ is on include_path
	set_include_path(implode(PATH_SEPARATOR, array(
		'/var/www/devel.way2day.ru/library',
		'/var/www/devel.way2day.ru/applications/plugins'		
	)));
}

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->session = new Zend_Session_Namespace();

$application->bootstrap();//->run();

/*
 * ======================================================================================================
 * ======================= UPLOAD MEMCACHE ==============================================================
 * ======================================================================================================
 */
// Открываем LOG файл для записи
$fileName = "log_uploadrating.txt";
if(!empty($_SERVER['REMOTE_ADDR'])) {
	$remout_ip = $_SERVER['REMOTE_ADDR'];
	
} elseif(!empty($_SERVER['SSH_CLIENT'])) {
	$remout_ip = $_SERVER['SSH_CLIENT'];	
	
}
if(!empty($remout_ip)) {
	preg_match('/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/', $remout_ip, $match);
	$subnet = $match[1];
	if ($subnet != '127.0.0' && $subnet != '172.16.0') {
		$fileName = "/var/www/devel.way2day.ru/public/log_uploadrating.txt";
		
	}
}
	
$fh = fopen($fileName, 'a+');

fwrite($fh, "\n Upload Rating From Memcache ".date("Y-m-d H:i:s")."\n====================================\n");

$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect to memcached server");

$allSlabs = $memcache->getExtendedStats('slabs');

foreach($allSlabs as $server => $slabs) {
	foreach($slabs AS $slabId => $slabMeta) {
		$cdump = $memcache->getExtendedStats('cachedump',(int)$slabId);
		foreach($cdump AS $keys => $arrVal) {
			if ($arrVal) {
				foreach($arrVal AS $k => $v) {
					$update = 0;
					
					if(preg_match('/travel_rating_(\d*)/',$k,$match)) {
						$table = new Application_Model_DbTable_Travels();
						$id = $match[1];
						$type = 'travel';
						$update = 1;
						
					} elseif(preg_match('/place_rating_(\d*)/',$k,$match)) {
						$table = new Application_Model_DbTable_Places();
						$id = $match[1];
						$type = 'place';
						$update = 1;
						
					} elseif(preg_match('/city_rating_(\d*)/',$k,$match)) {
						$table = new Application_Model_DbTable_City();
						$id = $match[1];
						$type = 'city';
						$update = 1;
						
					}

					if(!empty($update)) {
						if(!empty($id)) {
							// Получаем данные по ID
							$element = $table->getById($id);
							
							if(!empty($element)) {
								$data = $memcache->get($k);
								
								preg_match('/i:((-)?\d*)/',$data[0],$match_data);
								
								// Если нет значения в КЕШЕ
								if(!empty($match_data[1])) {
									$value = $match_data[1];
									$result = $table->updateRating($id, $value);
									
									$znak = !empty($match_data[2])?'-':'+';
									if($result) {
										$text = "type [$type] id [$id] => rating [".$element['rating']."] $znak $value \n";
										
									} else {
										$text = "error update [$k] $znak $value \n";
										
									}
									fwrite($fh, $text);
									echo $text;									
								}
								$memcache->delete($k);
							}
						}
					}
				}
			}
		}
	}		
}
$memcache->flush();

fclose($fh);