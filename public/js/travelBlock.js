/*
 * Тестовый класс
 * Он может называться например myTravelsBlock
 * и использоваться для управления выводом интерактивного блока мои поездки
 *
 * @author sergey.s.sergienko@gmail.com
 *
 * Вызывать так:
 * // Блок
 * $(document).ready( function () {
 *     myTravelsBlock.test();
 * });
 *
 *
 */
travelBlock = function ($) {
	return {
		// Метод класса
		SetTravelPlaceDay: function (travel, place, day) {
			// Формируем данные и передаем их в тест контроллер
			var data = {
				ajax: 1,
				travel: travel,
				place: place,
				day: day
			};

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/travel',
				// Акшн
				'chooseday',
				// Данные
				data,
				// Success
				function (data) {
					if(data){
						location.reload();
					}
				},
				// Error
				function (error) {
					// Сюда попадаем если ошибка
					console.log(error);
				}
				);
		}
	}
}(jQuery);

// Навешиваем обрабочик событий
$(document).ready( function() {
	// Изменить день поездки
	$('#travelpage .info .plan li .choose-day a').click(function(){
		var travel = $(this).attr('travel');
		var place = $(this).attr('place');
		var day = $(this).attr('day');

		travelBlock.SetTravelPlaceDay(travel,place,day);
	});
});
