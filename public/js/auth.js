/**
 * Класс api который работает удаленным апи. Через этот апи организована работа админки
 *
 * @author ssergienko@kodeks.ru
 *
 **/
Auth = function () {

	return {

		/*
		 * Метод для авторизации через социальные сети
		 */
		loginSocial: function(social){
			// Формируем данные и передаем их в тест контроллер
			var data = {
				ajax: 1,
				social: social
			};

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/users',
				// Акшн
				'login',
				// Данные
				data,
				// Success
				function (data) {
					console.log(data);
				},
				// Error
				function (error) {
					// Сюда попадаем если ошибка
					console.log(error);
				}
			);
		},

		initButton: function () {

			var _this = this;

			_this.setLogoutBtnHeandlers();
			_this.setLoginbtnHeandlers();

		},

		setLogoutBtnHeandlers: function () {

			var _this = this;

			$('#logout').click(
				function () {
					VK.Auth.logout(function(){
						refreshPage(false);
						_this.showLoginButton();
					});
				}
			);

		},

		showLogoutButton: function () {

			// Выводим кнопку логау
			$('#logout').show();
			$('#login_vk').hide();

		},

		setLoginbtnHeandlers: function () {

			var _this = this;

			$('#login_vk').click(
				function (event) {

					event.preventDefault();

					VK.Auth.getLoginStatus(
						// Авторизован
						function (response) {
							_this.showLogoutButton();
							_this.setLogoutBtnHeandlers();
						},
						// Не авторизован
						function () {
							_this.loginVk();
						}
					);

				}
			);


		},

		showLoginButton: function () {
			// Выводим кнопку логин
			$('#login_vk').show();
			$('#logout').hide();
		},

		loginVk: function () {

			// Выводим окошко с авторизацией
			VK.Auth.login(function(response) {
				if (response.session) {
						console.log(response);
					/* Пользователь успешно авторизовался */
					if (response.settings) {
						/* Выбранные настройки доступа пользователя, если они были запрошены */
						//console.log(response);
						//console.log('ggg');
						/*Api.send(
							'user', 'get',
							data:
						);*/
					}
				} else {
					/* Пользователь нажал кнопку Отмена в окне авторизации */
					//console.log(2222222);
				}
			});

		},

        initLogout: function () {
            $('#logout_button').unbind().on('click', function (e) {
                e.preventDefault();
                document.cookie = "w2duid=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
                location.href='/auth/logout';
            });
        }

	}

}(jQuery);

$(document).ready(function () {

		VK.init({
			apiId: 2718228
		});

		Auth.initButton();
        Auth.initLogout();

		/*$('.header .auth a').click(function(){
			var social = $(this).attr('class');
			Auth.loginSocial(social);
		});*/
	}

);