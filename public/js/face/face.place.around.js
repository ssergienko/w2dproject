Face.place.around = function ($) {

	return {


		init: function () {

			// Автокомплит городов
			this.setHeandlerToCityForm();

			// Отобразить места на карте
			this.showPlaces([ymaps.geolocation.latitude, ymaps.geolocation.longitude]);

			// Вешаем обработчик на нажатие клавиши enter
			//this.setKeyHeandlerToCityForm();

			// Реакция на перемещение карты
			//this.moveMapHeandlers();

		},



		moveMapHeandlers: function () {
			var _this = this;
			w2dmap.map.events.add('click', function (event) {
				w2dmap.setAroundByCoords(event.get("coordPosition"), function () { _this.showPlaces(event.get("coordPosition")) });
			});
		},




		/*
		 * Обработчик на нажатие кнопки
		 **/
		setKeyHeandlerToCityForm: function () {
			var _this = this;
			$("#city_title").on('keydown', function(e) {
				if (e.which == 13) {
					w2dmap.setAroundByCityName($('#city_title').val(), function () { _this.showPlaces([w2dmap.lat, w2dmap.lng]) });
				}
			});
		},



		setHeandlerToCityForm: function () {

			var _this = this;

			$("#city_title").autocomplete({
				url: "/api/places/getcity",
				remoteDataType: 'jsonp',
				sortResults: false,
				autoFill: true,
				showResult: function(value, data) {
					return '<span style="color:black">' + value + '</span>';
				},
				onItemSelect: function(item) {
					w2dmap.setAroundByCityName($('#city_title').val(), function () { _this.showPlaces([w2dmap.lat, w2dmap.lng]) });
				}
			});

		},



		showPlaces: function (coords) {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'getbycoords',
				// Данные
				{
					lat: coords[0],
					lng: coords[1]
				},
				// Success
				function (places) {
					for(var i=0;i<places.length;i++){
						if (places[i].image_url != 'no-image.png') {
							_this.showPlace(places[i]);
						}
					}
				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},


		showPlacesById: function (place_id) {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'getbyid',
				// Данные
				{
					place_id: place_id
				},
				// Success
				function (place) {
					_this.showPlace1(place);
				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},


		/*
		 * Отмечает место на карте. Ставит метку, добавляет описание.
		 **/
		showPlace: function (place) {

			//console.log(place);

			// Создание метки
			var myPlacemark = new ymaps.Placemark(
				// Координаты метки
				[place.lat, place.lng], {
					/* Свойства метки: - контент значка метки */
					balloonTitle: "<a href='"+HOST+"place/"+place.id+"' class='balloon-link'>"+place.title+"</a>",
					// - контент балуна метки
					balloonContent: "<div class='balloon'>\n\
										<a href='"+HOST+"place/"+place.id+"'>\n\
											"+place.title+"<br />\n\
											<img width='200px' height='150px' src='"+place.image_url+"' /><br />\n\
										</a>\n\
										<span>Расстояние: "+Math.round(place.distance)+"км</span>\n\
									</div>"
				}, {
					/* Опции метки:
					   - флаг перетаскивания метки */
					draggable: false,
                    iconImageHref: place.image_url,
                    iconImageSize: [60, 46]
				}
			);

			// Задание стиля коллекции после ее создания
			myPlacemark.options.set("preset", 'twirl#redDotIcon');

			// Добавление метки на карту
			w2dmap.map.geoObjects.add(myPlacemark);

		},



		/*
		 * Отмечает место на карте. Ставит метку, добавляет описание.
		 * Делает то же что и предыдущая, только не выводит расстояние
		 **/
		showPlace1: function (place) {

			//console.log(place);

			// Создание метки
			var myPlacemark = new ymaps.Placemark(
				// Координаты метки
				[place.lat, place.lng], {
					/* Свойства метки: - контент значка метки */
					balloonTitle: "<a href='"+HOST+"/place/"+place.id+"' class='balloon-link'>"+place.title+"</a>",
					// - контент балуна метки
					balloonContent: "<div class='balloon'>\n\
										<a href='"+HOST+"/place/"+place.id+"'>\n\
											"+place.title+"<br />\n\
											<img width='200px' height='150px' src='"+place.image_url+"' /><br />\n\
										</a>\n\
										<!--<span>Расстояние: "+Math.round(place.distance)+"км</span>-->\n\
									</div>"
				}, {
					/* Опции метки:
					   - флаг перетаскивания метки */
					draggable: false,
					/* - показывать значок метки при открытии балуна */
					hideIconOnBalloonOpen: true
				}
			);

			// Задание стиля коллекции после ее создания
			myPlacemark.options.set("preset", 'twirl#redDotIcon');

			// Добавление метки на карту
			w2dmap.map.geoObjects.add(myPlacemark);

		}


	}

}(jQuery);