Face.travel = function ($) {

	return {
		init: function () {
			this.TravelFav();
			this.TravelUnFav();

			this.TravelLike();
			this.TravelUnLike();

			this.ChangeDay();
		},

		TravelFav: function(){
			$('.travel-fav').live('click',function(){
				var travel_id = $(this).attr('name');

				var travel_h1 = $('h1').text();
				var result = travel_h1.match(/(«.*»)/i);
				var travel_title = result[1];

				if(travel_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/travels',
						// Акшн
						'travelfav',
						// Данные
						{
							ajax: 1,
							travel: travel_id
						},
						// Success
						function (data) {
							console.log(data);

							if(data['success']) {
								var message = "Поездка <a href='/index/mytravels'>"+travel_title+"</a>";

								switch(data['success']){
									case 'already':
										message += " была ранее добавленно в избранные";
										var message_type = 'block';
										break;
									default:
										message += " успешно добавлена в избранные";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth':var message = 'Error Authorized';break;
									case 'ajax':var message = 'Error Response Data';break;
									case 'result':var message = 'Error Response Result';break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.travel-fav').animate({height: 'toggle'});
							setTimeout(function(){
								$('.travel-unFav').animate({height: 'toggle'});
							}, 1000);
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},


		TravelUnFav: function(){
			$('.travel-unFav').live('click',function(){
				var travel_id = $(this).attr('name');

				var travel_h1 = $('h1').text();
				var result = travel_h1.match(/(«.*»)/i);
				var travel_title = result[1];

				if(travel_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/travels',
						// Акшн
						'travelunfav',
						// Данные
						{
							ajax: 1,
							travel: travel_id
						},
						// Success
						function (data) {
							console.log(data);

							if(data['success']) {
								var message = "Поездка "+travel_title+"";

								switch(data['success']){
									case 'already':
										message += " не была добавлена в избранные";
										var message_type = 'block';
										break;
									default:
										message += " успешно удалена из избранных";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth':var message = 'Error Authorized';break;
									case 'ajax':var message = 'Error Response Data';break;
									case 'result':var message = 'Error Response Result';break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.travel-unFav').animate({height: 'toggle'});
							setTimeout(function(){
								$('.travel-fav').animate({height: 'toggle'});
							}, 1000);
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},

		TravelLike: function(){
			$('.travel-like').live('click',function(){
				var travel_id = $(this).attr('name');

				var travel_h1 = $('h1').text();
				var result = travel_h1.match(/(«.*»)/i);
				var travel_title = result[1];

				if(travel_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/travels',
						// Акшн
						'travellike',
						// Данные
						{
							ajax: 1,
							travel: travel_id
						},
						// Success
						function (data) {
							console.log(data);

							if(data['success']) {
								var message = "Поездка <a>"+travel_title+"</a>";

								switch(data['success']){
									case 'already':
										message += " уже вам нравится";
										var message_type = 'block';
										break;
									default:
										message += " вам понравилась";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth':var message = 'Error Authorized';break;
									case 'ajax':var message = 'Error Response Data';break;
									case 'result':var message = 'Error Response Result';break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.travel-like span').html('Уже не нравится');
							$('.travel-like').removeClass('btn-success').addClass('travel-unLike').removeClass('travel-like');
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},


		TravelUnLike: function(){
			$('.travel-unLike').live('click',function(){
				var travel_id = $(this).attr('name');

				var travel_h1 = $('h1').text();
				var result = travel_h1.match(/(«.*»)/i);
				var travel_title = result[1];

				if(travel_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/travels',
						// Акшн
						'travelunlike',
						// Данные
						{
							ajax: 1,
							travel: travel_id
						},
						// Success
						function (data) {
							console.log(data);

							if(data['success']) {
								var message = "Поездка <a>"+travel_title+"</a>";

								switch(data['success']){
									case 'already':
										message += " вам никогда не нравилась";
										var message_type = 'block';
										break;
									default:
										message += " больше вам не нравится";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth':
										var message = 'Error Authorized';
										break;
									case 'ajax':
										var message = 'Error Response Data';
										break;
									case 'result':
										var message = 'Error Response Result';
										break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.travel-unLike span').html('Мне нравится');
							$('.travel-unLike').addClass('btn-success').addClass('travel-like').removeClass('travel-unLike');
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},

		ChangeDay: function () {
			$('.change-day-cl ul.dropdown-menu li a').live('click', function() {

				var _this = this;

				var list = $(_this).closest('.dropdown-menu');
				var travel_id = list.attr('travel');
				var place_id = list.attr('place');
				var day = $(_this).closest('li').attr('day');

				if(travel_id && place_id && day) {
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/travelplace',
						// Акшн
						'changeday',
						// Данные
						{
							ajax: 1,
							travel: travel_id,
							place: place_id,
							day: day
						},
						// Success
						function (data) {
							if(data['success']) {

								// Определяем день поездки
								var place = $(_this).closest('.item.place');
								var day = $(_this).closest('li').attr('day')*1;

								// Меняем заголовок списка выбора дня
								var html = "";
								if(day == 0) {
									var btn_without_day = $('.change-day-cl a.btn-text');
									if (btn_without_day.html() == ' - - - Выберите день поездки - - - ') {
										btn_without_day.removeClass('btn-inverse');
										btn_without_day.addClass('btn-danger');
									}
									html += " - - - Выберите день поездки - - - ";
								} else {
									var btn_without_day = $('.change-day-cl a.btn-text');
									if (btn_without_day.html() != ' - - - Выберите день поездки - - - ') {
										btn_without_day.removeClass('btn-danger');
										btn_without_day.addClass('btn-inverse');
									}
									html += $(_this).html();
								}
								html += "<span class='caret'></span>";
								$(_this).closest('.change-day-cl').find('a.dropdown-toggle').html(html);

								// Меняем список выбора дня
								var day_choose = "";
								if(day != 0) {
									day_choose += "<li day='0'><a>Без дня</a></li>";
									//day_choose.push(0, "Без дня");
								}
								for(var i=1; i <= day+3; i++) {
									if(i==1||i ==4||i==5||i%10==1||i%10==4||i%10==5||(i>9&&i<20)) {
										day_choose += "<li day='"+i+"'><a>"+i+"-ый день поездки</a></li>";
									} else if(i==3||i%10==3) {
										day_choose += "<li day='"+i+"'><a>"+i+"-ий день поездки</a></li>";
									} else {
										day_choose += "<li day='"+i+"'><a>"+i+"-ой день поездки</a></li>";
									}
								}
								$(_this).closest('ul.dropdown-menu').html(day_choose);

								// Если день содержит только 1 место - удаляем
								var day_list = $(place).closest('div.day');
								if(day_list.find('div.item.place').length == 1) {

									day_list.animate({height: 'toggle'});

									setTimeout(function(){
										day_list.remove();
									}, 500);

								} else {

									$(place).animate({height: 'toggle'});
								}

								setTimeout(function (){
									// Удаляем место из дня
									//place = $(place).toggle().remove();
									place = $(place).remove();

									// Если блока с днем нет
									if(!$('div.day[days="'+day+'"]').is('div')) {
										$(place).css('display', 'block');
										var after_day = -1;
										// Пробегаем все дни и находим место вставки
										$.each($('div.list-places div.day'), function(key, value) {
											var current_day = $(value).attr('days')*1;
											if(current_day < day) {
												after_day = current_day;
											}
											var last_day = current_day;
										});

										// Если был сброшен день поездки
										if(day == 0){
											var html = "<div class='day hide' days='"+day+"'><h2>Без дня</h2></div>";
										} else {
											var html = "<div class='day hide' days='"+day+"'><h2>"+day+" день поездки</h2></div>";
										}

										if(after_day < 0) {
											$('div.list-places').prepend(html);
										} else {

											$('div.list-places div.day[days="'+after_day+'"]').after(html);
										}

										// Вставляем место в день
										$('div.list-places div.day[days="'+day+'"] h2').after(place);

										setTimeout(function() {
											$('div.list-places div.day[days="'+day+'"]').animate({height: 'toggle'});
										},500);
									} else {
										$(place).css('display', 'none');
										$('div.list-places div.day[days="'+day+'"]').append(place);
										$(place).animate({height: 'toggle'});
									}
								}, 500);
							}
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		}
	}

}(jQuery);

Face.travel.add = {};