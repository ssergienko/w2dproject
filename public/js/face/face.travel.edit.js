/*
 * Сраница добавления места
 **/
Face.travel.edit = function ($) {

	return {

		user_id: 0,
		travel_id: 0,
		images_arr: new Array(),
		idx_arr: new Array(),

		start: function (user_id, travel_id) {

			var _this = this;

			'use strict';

			// Вешаем обработчики на начало и конец загрузки
			$('#fileupload')
			.bind('fileuploadstarted', function (e) {

				// Глушим стандартный обработчик
				$('#add_place_btn').unbind().click(
					function (e) {
						e.preventDefault();
					}
				);
			})
			.bind('fileuploadstopped', function (e) {
				// Обработчик на кнопку сохранения
				_this.mainSaveButton();
			});

			// Enable iframe cross-domain access via redirect option:
			$('#fileupload').fileupload(
				'option',
				'redirect',
				window.location.href.replace(
					/\/[^\/]*$/,
					'/cors/result.html?%s'
				)
			);

			this.user_id = user_id;
			this.travel_id = travel_id;

			// Кнопка загрузки картинок с компьютера
			this.setUploadBtnHeandler();
			// Вывод уже загруженных картинок
			this.showUploaded();

			// Инициализируем sortable
			this.initSortable();

		},



		/*
		 * Инициализирует плагин для сортировки
		 **/
		initSortable: function () {

			var _this = this;

			$("#sortable").sortable(
				{
					// Действие на отпускание блока мышкой
					beforeStop: function () {
						// Пересчитываем порядок блоков
						_this.resetIdx();
					}
				}
			);
		},



		/*
		 * Пересчитывает атрибт idx у блоков стекстом
		 **/
		resetIdx: function () {

			$('.image_txt').each(
				function (index) {
					$(this).attr('idx-field', index);
				}
			);

		},



		/*
		 * Задает главную картинку для вывода в списках
		 **/
		setMainImage: function () {

			var _this = this;

			$('input[name=main_picture]').change(
				function () {

					Api.send(
						// Контроллер
						'api/travels',
						// Акшн
						'setmainimage',
						// Данные
						{
							travel_id: _this.travel_id,
							middle_url: $(this).val()
						},
						// Success
						function () {

						},
						// Error
						function (error) {console.log(error);}
					);

				}
			);

		},



		/*
		 * Ставит радиобаттон для главной картинки при выводе списка картинок
		 **/
		setMainImageRadio: function () {

			var _this = this;

			Api.send(
				// Контроллер
				'api/travels',
				// Акшн
				'getmainimage',
				// Данные
				{
					travel_id: _this.travel_id
				},
				// Success
				function (data) {
					if (typeof(data.image_url) != 'undefined') {

						$('input[value="'+data.image_url+'"]').attr('checked',true).parent().addClass('current');
						$('#create_travel_form').append('<input type="hidden" name="main_picture" value="'+data.image_url+'" />');

						return 1;
					}
					return 0;
				},
				// Error
				function (error) {
					return 0;
				}
			);

		},



		/*
		 * Показывает уже загруженные картинки
		 **/
		showUploaded: function () {

			var _this = this;

			// Срабатывает при открытии страницы
			$('#fileupload').each(function () {

				var that = this;

				$.getJSON(HOST+'storage/gettrimages',
				{
					user_id: _this.user_id,
					travel_id: _this.travel_id
				},
				function (result) {

					if (result && result.length) {

						$(that).fileupload('option', 'done').call(that, null, {result: result});

						//  Получаем главную картинку
						var res = _this.setMainImageRadio();

						//  Вешаем обработчик на выбор главной картинки
						_this.setMainImage();

						// Обработчик на кнопку сохранения
						_this.mainSaveButton();

					}
				});
			});

		},



		/*
		 * Если нажали на большую желтую кнопку сохранить
		 **/
		mainSaveButton: function () {

			var _this = this;

			$('#add_place_btn').unbind().click(
				function (e) {

					// Ставим курсор в ожидание
					$(document).css('cursor','wait');

					// Глушим стандартный обработчик
					e.preventDefault();

					var count_images = $('.image_txt').length;

					// Сохраняем тексты картинок, индексы и другие доп данные
					_this.saveImagesData();

					// Если еще не нажимали
					if (Face.listener.text_save_listener == '') {
						// Ждем пока все тексты картинок догрузятся, потом субмитим форму
						Face.listener.text_save_listener = setInterval(function() {
							if (_this.images_arr.length == count_images) {
								clearInterval(Face.listener.text_save_listener);
								//$('#create_travel_form').submit();
								location.href = '/travel/'+_this.travel_id;
							}
						}, 300);
					}
				}
			);
		},



		/*
		 * Пробегает по всем картинкам и сохраняет описание для каждой
		 **/
		saveImagesData: function () {

			var _this = this;

			_this.images_arr = new Array();

			$('.image_txt').each(
				function () {
					_this.saveImageData($(this).attr('data-field'), $(this).val(), $(this).attr('idx-field'));
				}
			);

		},



		/*
		 * Сохраняет подпись для картинки
		 *
		 * @params image_url - урл картинки для поиска записи в таблице картинок
		 * @params image_txt - текст картинки
		 * @params image_idx - индекс для сортировки
		 *
		 **/
		saveImageData: function (image_url, image_txt, image_idx) {

			var _this = this;

			// Сохраняем текст для картинки
			Api.send(
				// Контроллер
				'api/travels',
				// Акшн
				'saveimagedata',
				// Данные
				{
					travel_id: _this.travel_id,
					image_url: image_url,
					image_txt: image_txt,
					image_idx: image_idx
				},
				// Success
				function (data) {
					if (data.success == 1) {
						// Сохраняем
						_this.images_arr.push(1);
					}
				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},



		/*
		 * Вешаем обработчик на клик на кнопку добавления картинок
		 *
		 *
		 **/
		setUploadBtnHeandler: function () {

			var _this = this;

			// Срабатывает при выборе картинок
			$('#fileupload').fileupload({
				add: function (e, data) {

					// Сразу загружаем выбранные картинки
					var jqXHR = data.submit()
						// В случае успеха пишем в таблицу place_images запись о том какой пользователь какую картинку добавил при создании места
						.success(function (result, textStatus, jqXHR) {

							if (typeof(result[0].files_url) != 'undefined') {

								Api.send(
									// Контроллер
									'api/travels',
									// Акшн
									'addtrimage',
									// Данные
									{
										travel_id: _this.travel_id,
										name: result[0].name,
										url: result[0].files_url,
										middle_url: result[0].middle_url,
										thumbnail_url: result[0].thumbnail_url,
										delete_url: result[0].delete_url
									},
									// Success
									function (data) {},
									// Error
									function (error) {console.log(error);}
								);
							}

						})
						.complete(function (result, textStatus, jqXHR) {
							//   Вешаем обработчик на выбор главной картинки
							_this.setMainImage();
						})
						.error(function (jqXHR, textStatus, errorThrown) {console.log(jqXHR, textStatus, errorThrown);});
				},
				destroyed: function (e, data) {

					Api.send(
						// Контроллер
						'api/storage',
						// Акшн
						'deletetr',
						// Данные
						{
							travel_id: _this.travel_id,
							delete_url: data.url
						},
						// Success
						function (data) { if (data.success) { console.log(data); } },
						// Error
						function (error) {console.log(error);}
					);

				}

			});

		}

	}

}(jQuery);