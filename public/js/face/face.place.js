Face.place = (function () {

	return {

		init: function () {

			var _this = this;



			// Кнопка редактирования описания
			$('#place-descr-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения описания
			$('#place-descr-save-btn').click(
				function () {
					// Show samething
					_this.saveDescrField();
				}
			);

			// Кнопка редактирования названия
			$('#place-title-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения названия
			$('#place-title-save-btn').click(
				function () {
					// Show samething
					_this.saveTitleField();
				}
			);

			// Кнопка сохранения названия
			$('a.to-auth').click(
				function () {
					// Show samething
					Face.showShadow();
				}
			);


			this.hideAddToTravel();
			this.chooseAddToTravel();
			this.addToTravel();

			this.PlaceFav();
			this.PlaceUnFav();

			this.PlaceLike();
			this.PlaceUnLike();
		},

		/*
		 * Делает блок редактируемым
		 *
		 * @param block object
		 *
		 **/
		doFieldEditable: function (block) {

			if(block.id == 'place-descr-edit-btn') {
				// Делаем редактируемым
				$('#place-descr-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#place-descr-field').css('border-style', 'dotted');
			} else if(block.id == 'place-title-edit-btn') {
				// Делаем редактируемым
				$('#place-title-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#place-title-field').css('border-style', 'dotted');
			}

		},


		/*
		 *  Сохраняет описание
		 **/
		saveDescrField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'savedescr',
				// Данные
				{
					id: $('#place_id').val(),
					descr: $('#place-descr-field').html()
				},
				// Success
				function (data) {

					// Делаем не редактируемым
					$('#place-descr-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#place-descr-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},

		/*
		 *  Сохраняет название
		 **/
		saveTitleField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'savetitle',
				// Данные
				{
					id: $('#place_id').val(),
					title: $('#place-title-field').html()
				},
				// Success
				function (data) {

					// Делаем не редактируемым
					$('#place-title-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#place-title-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},

		hideAddToTravel: function(){
			$('.add-to-travel').click(function(){
				$('.add-to-travel-block').animate({height: 'toggle'});
			});
		},

		chooseAddToTravel: function(){
			$('.add-to-travel-block select[name="t_id"]').change(function(){
				var value = $(this).val();
				var disp = $(this).closest('.add-to-travel-block').find('input[name="t_title"]').css('display');
				if(value == 0 && disp == 'none'){
					$(this).closest('.add-to-travel-block').find('input[name="t_title"]').animate({height: 'toggle'});
				}
				else if(value != 0 && disp != 'none'){
					$(this).closest('.add-to-travel-block').find('input[name="t_title"]').animate({height: 'toggle'});
				}
			});
		},

		addToTravel: function(){
			$('input[name="add-to-travel-button"]').click(function(){
				// Выбираем данные для добавления
				var place_id = $(this).closest('.add-to-travel-block').find('input[name="p_id"]').val();

				var place_h1 = $('h1').text();
				var result = place_h1.match(/(«.*»)/i);
				var place_title = result[1];

				var travel_id = $(this).closest('.add-to-travel-block').find('select[name="t_id"]').val();
				var travel_title = travel_id == '0'
						?$(this).closest('.add-to-travel-block').find('input[name="t_title"]').val()
						:$(this).closest('.add-to-travel-block').find('select[name="t_id"] option:selected').html();

				// Проверяем на пустые данные
				if(travel_id != '0' || travel_title != ''){

					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/places',
						// Акшн
						'addtotravel',
						// Данные
						{
							place: place_id,
							travel: travel_id,
							title: travel_title
						},
						// Success
						function (data) {

							if(data['success']) {
								var message = "Место "+place_title;
								switch(data['success']['status']) {
									case 'already':
										message += ' уже было ранее добавленно в поездку';
										var message_type = 'block';
										break;
									default:
										message += ' успешно добавлено в поездку';
										var message_type = 'success';
										break;
								}
								// Если новая поездка
								if(travel_id != '0' || data['success']['t_id']) {
									//message += " <a href='/travel/"+travel_id+"'>"+travel_title+"</a>";
								//} else if(data['success']['t_id']) {
									message += " <a href='/index/mytravels/travel/"+data['success']['t_id']+"'>"+travel_title+"</a>";
								} else {
									var message_type = 'error';
									message = "Ошибка добавления места в поездку";
								}

							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth': var message = 'Error Authorized'; break;
									case 'ajax': var message = 'Error Response Data'; break;
									case 'result': var message = 'Error Response Result'; break;
								}
							}
							if(message !=""){
								Face.showAlertMessage(message, message_type);
							}
							$('.add-to-travel-block').animate({height: 'toggle'});
						},
						// Error
						function (error) {
							console.log(error);
						}
					);
				}

			});
		},

		PlaceFav: function(){
			$('.place-fav').live('click',function(){
				console.log(1);
				// Выбираем данные для добавления
				var place_id = $(this).attr('name');

				if(place_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/places',
						// Акшн
						'placefav',
						// Данные
						{
							ajax: 1,
							place: place_id
						},
						// Success
						function (data) {

							console.log(data);

							if(typeof data.success != 'undefined') {
								var message = "Место <a href='/index/myplaces'>"+data.success.title+"</a>";
								switch(data['success']){
									case 'already':
										message += " было ранее добавленно в избранные";
										var message_type = 'block';
										break;
									default:
										message += " успешно добавлено в избранные";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth': var message = 'Error Authorized'; break;
									case 'ajax': var message = 'Error Response Data'; break;
									case 'result': var message = 'Error Response Result'; break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.place-fav').animate({height: 'toggle'});
							setTimeout(function(){
								$('.place-unFav').animate({height: 'toggle'});
							}, 1000);

						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},

		PlaceUnFav: function(){
			$('.place-unFav').live('click',function(){
				// Выбираем данные для добавления
				var place_id = $(this).attr('name');

				if(place_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/places',
						// Акшн
						//'placeUnFav',
						'placeunfav',
						// Данные
						{
							ajax: 1,
							place: place_id
						},
						// Success
						function (data) {

							if(typeof data.success != 'undefined') {
								var message = "Место "+data.success.title+"";
								switch(data['success']){
									case 'already':
										message += " не была добавлено в избранные";
										var message_type = 'block';
										break;
									default:
										message += " успешно удалено из избранных";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth': var message = 'Error Authorized'; break;
									case 'ajax': var message = 'Error Response Data'; break;
									case 'result': var message = 'Error Response Result'; break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.place-unFav').animate({height: 'toggle'});
							setTimeout(function(){
								$('.place-fav').animate({height: 'toggle'});
							}, 1000);

						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},

		PlaceLike: function(){
			$('.place-like').live('click',function(){
				console.log(1);
				// Выбираем данные для добавления
				var place_id = $(this).attr('name');

				if(place_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/places',
						// Акшн
						'placelike',
						// Данные
						{
							ajax: 1,
							place: place_id
						},
						// Success
						function (data) {
							console.log(data);

							if(typeof data.success != 'undefined') {
								var message = "Место <a>"+data.success.title+"</a>";
								switch(data['success']){
									case 'already':
										message += " уже вам нравится";
										var message_type = 'block';
										break;
									default:
										message += " вам понравилось";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth': var message = 'Error Authorized'; break;
									case 'ajax': var message = 'Error Response Data'; break;
									case 'result': var message = 'Error Response Result'; break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.place-like span').html('Уже не нравится');
							$('.place-like').removeClass('btn-success').addClass('place-unLike').removeClass('place-like');
						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		},

		PlaceUnLike: function(){
			$('.place-unLike').live('click',function(){
				// Выбираем данные для добавления
				var place_id = $(this).attr('name');

				if(place_id){
					// Формирование запроса к апи
					Api.send(
						// Контроллер
						'api/places',
						// Акшн
						//'placeUnFav',
						'placeunlike',
						// Данные
						{
							ajax: 1,
							place: place_id
						},
						// Success
						function (data) {
							console.log(data);

							if(typeof data.success != 'undefined') {
								var message = "Место <a>"+data.success.title+"</a>";
								switch(data['success']){
									case 'already':
										message += " вам никогда не нравилось";
										var message_type = 'block';
										break;
									default:
										message += " больше вам не нравится";
										var message_type = 'success';
										break;
								}
							} else {
								var message_type = 'error';

								switch(data['error']){
									case 'auth': var message = 'Error Authorized'; break;
									case 'ajax': var message = 'Error Response Data'; break;
									case 'result': var message = 'Error Response Result'; break;
								}
							}

							if(message != ""){
								Face.showAlertMessage(message, message_type);
							}

							$('.place-unLike span').html('Мне нравится');
							$('.place-unLike').addClass('btn-success').addClass('place-like').removeClass('place-unLike');

						},
						// Error
						function (error) {
							console.log(error);
						}
					)
				}
			});
		}
	}

})();