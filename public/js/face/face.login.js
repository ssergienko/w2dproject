Face.login = function ($) {

	return {
		init: function () {
			this.formActions();
		},

		/**
		* Навешивает обработчики на формы.
		*/
		formActions: function () {

			var _this = this;

			$('#email_user').focus(
				function () {
					if ($(this).val() == 'Электронная почта') {
						$(this).val('');
						$(this).css('font-style', 'normal').css('color', 'black');
					}
				}
			);
			$('#email_user').keypress(
				function(event) {

					if ( event.which == 13 ) {
						_this.sendEmail();

						event.preventDefault();
					}
				}
			)

			/*$('#subscribe_user').unbind().click(
				function () {
					_this.sendEmail();
				}
			);*/
		}
	}

}(jQuery);