Face.hotel = function ($) {

	return {
		init: function () {
			$('#date_arrival').datepicker({
				dateFormat:'yy-mm-dd',
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				dayNamesMin: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
				firstDay: 1,
				minDate: new Date()
			});
			$('#date_departure').datepicker({
				dateFormat:'yy-mm-dd',
				monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
				dayNamesMin: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
				firstDay: 1,
				minDate: $('#date_arrival').val()
			});
			$('#date_arrival').change(function() {
				$("#date_departure").datepicker( "option", "minDate", $( this ).val() );
			});

			this.Booking();

			this.mapHeandler();

		},



		Booking: function () {
			$('#booking_room').live('click', function (){
				// Обрабатываем дату заезда и забиваем данные
				var date_arrival = $('#date_arrival').val();
				if(date_arrival) {
					// Определяем формат даты
					var date_checkin = $('#checkin').val(date_arrival);
				}

				// Обрабатываем дату выезда и забиваем данные
				var date_departure = $('#date_departure').val();

				var checkin = new Date(date_arrival);
				var checkout = new Date(date_departure);
				// Определяем разницу в датах заезда и выезда
				var diff = checkout - checkin;

				if (diff > 0) {
					var date_interval =  $('#interval').val(diff / 1000 / 60 / 60 / 24);
				}

				if(date_checkin && date_interval) {
					$('#frm').submit();
				}
			});
		}
	}

}(jQuery);