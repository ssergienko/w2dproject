/*
 * Объект для интерфейса главной страницы
 *
 */
Face.user.place = function ($) {

	return {


		/*
		 * Вешаем обработчики
		 *
		 **/
		init: function () {

			var _this = this;

			// Кнопка редактирования описания
			$('#place-descr-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения описания
			$('#place-descr-save-btn').click(
				function () {
					// Show samething
					_this.saveDescrField();
				}
			);

			// Кнопка редактирования названия
			$('#place-title-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения названия
			$('#place-title-save-btn').click(
				function () {
					// Show samething
					_this.saveTitleField();
				}
			);

			this.hideAddToTravel();
			this.chooseAddToTravel();
			this.addToTravel();

		},


		/*
		 * Делает блок редактируемым
		 *
		 * @param block object
		 *
		 **/
		doFieldEditable: function (block) {

			if(block.id == 'place-descr-edit-btn') {
				// Делаем редактируемым
				$('#place-descr-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#place-descr-field').css('border-style', 'dotted');
			} else if(block.id == 'place-title-edit-btn') {
				// Делаем редактируемым
				$('#place-title-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#place-title-field').css('border-style', 'dotted');
			}

		},



		/*
		 *  Сохраняет описание
		 **/
		saveDescrField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'savedescr',
				// Данные
				{
					id: $('#place_id').val(),
					descr: $('#place-descr-field').html()
				},
				// Success
				function (data) {

					// Делаем не редактируемым
					$('#place-descr-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#place-descr-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},



		/*
		 *  Сохраняет название
		 **/
		saveTitleField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'savetitle',
				// Данные
				{
					id: $('#place_id').val(),
					title: $('#place-title-field').html()
				},
				// Success
				function (data) {

					// Делаем не редактируемым
					$('#place-title-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#place-title-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},


		hideAddToTravel: function(){
			$('.add-to-travel').click(function(){
				$('.add-to-travel-block').animate({height: 'toggle'});
			});
		},

		chooseAddToTravel: function(){
			$('.add-to-travel-block select[name="t_id"]').change(function(){
				var value = $(this).val();
				var disp = $(this).closest('.add-to-travel-block').find('input[name="t_title"]').css('display');
				if(value == 0 && disp == 'none'){
					$(this).closest('.add-to-travel-block').find('input[name="t_title"]').animate({height: 'toggle'});
				}
				else if(value != 0 && disp != 'none'){
					$(this).closest('.add-to-travel-block').find('input[name="t_title"]').animate({height: 'toggle'});
				}
			});
		},

		addToTravel: function(){
			$('input[name="add-to-travel-button"]').click(function(){
				// Проверяем наличие блока для добавления места в свои поездки
				if($(this).closest('.add-to-travel-block').is('div')){
					// Получаем ID пользователя
					var user_id = $(this).closest('.add-to-travel-block').find('input[name="user_id"]').val();

					if(user_id){

						// Выбираем данные для добавления
						var place = $(this).closest('.add-to-travel-block').find('input[name="p_id"]').val();
						var place_title = $('#placepage .title').html();
						var travel = $(this).closest('.add-to-travel-block').find('select[name="t_id"]').val();
						var travel_title = travel == 0
								?$(this).closest('.add-to-travel-block').find('input[name="t_title"]').val()
								:$(this).closest('.add-to-travel-block').find('select[name="t_id"] option:selected').html();

						// Проверяем на пустые данные
						if(travel != '0' || travel_title != ''){

							// Формирование запроса к апи
							Api.send(
								// Контроллер
								'api/places',
								// Акшн
								'addtotravel',
								// Данные
								{
									ajax: 1,
									place: place,
									travel: travel,
									title: travel_title
								},
								// Success
								function (data) {

									if(data[0] != 'false'){
										//window.location.href = '/user/'+user_id+'/travel/'+data[0]['t_id']+'/place/'+data[0]['p_id'];

										$('.add-to-travel-block').animate({height: 'toggle'});
										var html = 'Добавлено место <a href="/user/'+user_id+'/travel/'+data[0]['t_id']+'/place/'+data[0]['p_id']+'">'+place_title+'</a>';
										html += ' в поездку <a href="/user/'+user_id+'/travel/'+data[0]['t_id']+'">'+travel_title+'</a>';
										$('.right-block').append(html);

										$('.add-to-travel-success').toggle();

										setTimeout(function(){
											$('.add-to-travel-success').toggle();
										},1000);
									}
								},
								// Error
								function (error) {
									console.log(error);
								}
							);
						}
					}
				}
			});
		}

	}

}(jQuery);