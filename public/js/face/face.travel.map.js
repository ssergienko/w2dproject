Face.travel.map = function ($) {

	return {

		init: function (coords, center) {

			var _this = this;
			_this.coords = coords;
			_this.center = center;

			ymaps.ready(function () {

				var myMap = new ymaps.Map("map", {
					center: _this.center,
					zoom: 10
				});

				// Создание экземпляра элемента управления
				myMap.controls.add(
				   new ymaps.control.ZoomControl()
				);

				var coords = _this.coords;
				var myCollection = new ymaps.GeoObjectCollection();

				for (var i = 0; i < coords.length; i++) {
					myCollection.add(new ymaps.Placemark(coords[i]));
				}

				myMap.map.geoObjects.add(myCollection);
			});

		}

	}

}(jQuery);