/*
 * Главный объект для интерфейса
 *
 */
var Face = (function () {
    return {

        init: function (){

            $('.hint').tooltip();
            $('.typeahead').typeahead();
            this.doClickable();

            this.foolSizeImages();

        },



        // Делает кликабельными картинки
        doClickable: function () {
            $('.img-container').click(function() {
                location.href = $(this).find('a.place-name').attr('href');
            });

        },



        // Затемняет экран
        showShadow: function () {

            var _this = this;

            $('#all-shadow').toggle().click(
                function () {
                    _this.hideShadow();
                }
            );
        },



        hideShadow: function () {
            $('#all-shadow').hide();
        },




        dontWorkMessage: function () {

            $('.place_stat.unstyled a').unbind().click(
                function(e) {
                    e.preventDefault();
                }
            );

        },

        foolSizeImages: function () {

            // Настройки для карусели
            $('.carousel').carousel('pause');
            // Нажатие на картинку
            $('.carousel .btn-full-screen, .absolut_gallery .close, img.gallery, div.carousel-inner div.item img').click(function() {

                //console.log('#small_'+$(this).parent().attr("id"));

                // Тушим активные
                $('.absolut_gallery .item').removeClass('active');
                // Выбираем активную
                $('#small_'+$(this).parent().attr("id")).addClass('active');
                // Открываем картинку на весь экран
                $('.absolut_gallery').toggleClass('hide');
            });
        },


        showAlertMessage: function(message, message_type) {

            if($('.alert').css('display') != 'none') {
                $('.alert').animate({height: 'toggle'});
                setTimeout(function(){
                    var alert_class = "alert hide alert-"+message_type;
                    $('.alert').attr('class', alert_class);
                    $('.alert strong').html(message);
                    $('.alert').animate({height: 'toggle'});
                },1000);

            } else {
                var alert_class = "alert hide alert-"+message_type;
                $('.alert').attr('class', alert_class);
                $('.alert strong').html(message);
                $('.alert').animate({height: 'toggle'});
            }
        }


    }

})();

Face.index = {};
Face.user = {};
Face.city = {};
Face.search = {};
Face.place = {};
Face.travel = {};
Face.hotel = {};
Face.listener = {};