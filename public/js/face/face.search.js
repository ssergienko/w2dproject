Face.search = function ($) {

	return {
		init: function () {
			this.doClickable();
			this.submitSearch();
		},

		// Submit search
		doClickable: function () {
			$('.the_place').click(function(){
				location.href = $(this).attr('data');
			});

		},

		// Submit search
		submitSearch: function () {
			$('#search-form #search-submit').unbind().click(function(e){

				// Глушим пустой запрос
				if ($('#appendedInputButton').val() == '') {
					$('#appendedInputButton').attr('placeholder', 'Введи хоть что-нибудь');
				} else {
					$('#search-form').submit();
				}
			});

		}
	}

}(jQuery);