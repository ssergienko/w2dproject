/*
 * Объект для интерфейса главной страницы
 *
 */
Face.user = function ($) {

	return {


		/*
		 * Вешаем обработчики
		 *
		 **/
		start: function (user_id) {

			this.user_id = user_id;
			this.setUploadBtnHeandler();

		},


		/*
		 * Делает блок редактируемым
		 *
		 * @param block object
		 *
		 **/
		setUploadBtnHeandler: function (block) {

			var _this = this;

			// Срабатывает при выборе картинок
			$('#fileupload').fileupload({
				add: function (e, data) {

					// Сразу загружаем выбранные картинки
					var jqXHR = data.submit()
						// В случае успеха пишем в таблицу tmp_images запись о том какой пользователь какую картинку добавил при создании места
						.success(function (result, textStatus, jqXHR) {

							Api.send(
								// Контроллер
								'api/users',
								// Акшн
								'saveimage',
								// Данные
								{
									user_id: _this.user_id,
									url: result[0].middle_url
								},
								// Success
								function (data) {
									if (typeof(data.url) != 'undefined') {
										$('#avatarka').attr('src', data.url);
										$('#avatarka_in_header').attr('src', data.url);
									}
								},
								// Error
								function (error) {console.log(error);}
							);

						})
						.complete(function (result, textStatus, jqXHR) {
							// Вешаем обработчик на выбор главной картинки
							//_this.setMainImage();
						})
						.error(function (jqXHR, textStatus, errorThrown) {console.log('ERROR');});
				}
			});

		}

	}

}(jQuery);