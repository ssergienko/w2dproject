/*
 * Объект для страницы списка мест
 *
 */
Face.user.places = (function () {

	return {

		init: function () {

			var _this = this;

			$('.delete-btn').unbind().click(
				function () {

					// Затемняем экран
					Face.showShadow();

				}
			);


			Face.dontWorkMessage();

		}

	}

})();