/*
 * Объект для главной
 *
 */
Face.index = (function () {
    return {

        init: function (){
            this.submitSearch();
            this.doClickable();
            this.placesaround();

            $('.carousel').carousel({
                pause: true,
                interval: false/*,
                interval: 2000*/
            });

        },

        placesaround: function () {
            $('#placesaroud').unbind().click(
                function () {
                    $('#w2d_redirect_uri').val('places/around');
                }
            );
        },

        // Submit search
        doClickable: function () {
            $('.the_place').unbind().click(function(){
                location.href = $(this).attr('data');
            });

        },

        // Submit search
        submitSearch: function () {
            $('#search-form #search-submit').unbind().click(function(e){

                // Глушим пустой запрос
                if ($('#appendedInputButton').val() == '') {
                    $('#appendedInputButton').attr('placeholder', 'Введите хоть что-нибудь :)');
                } else {
                    $('#search-form').submit();
                }
            });

        }
    }

})();