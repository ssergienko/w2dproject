/*
 * Объект для главной
 *
 */
Face.header = (function () {
	return {

		init: function (){
			// Вешаем обработчики на блоки в шапке сайта
			this.setHeaderHeandlers();

			// Вешаем обработчик на кнопку Войти в модальном окне авторизации
			this.setAuthModalHeandler();

			this.remPassHeandler();

		},



		remPassHeandler: function () {

			$('#remember_pass').unbind().click(
				function (e) {

					e.preventDefault();

					// Проверяем поля
					if ($('#input_email').val() == '') {
						$('#email_field').addClass('error');
						$('#email_error_txt').show();
					} else {
						$('#email_field').removeClass('error');
						$('#email_error_txt').hide();

						// Отправляеи пароль на почту
						Api.send(
							// Контроллер
							'api/auth',
							// Акшн
							'sendpasswd',
							// Данные
							{
								email: $('#input_email').val()
							},
							// Success
							function (data) {
								$('#auth_error_txt').html('Пароль отправлен на <strong>'+data.success+'</strong>');
								$('#auth_error_txt').css('color', 'green');
								$('#auth_error_txt').show();
							},
							// Error
							function (data) {
								$('#auth_error_txt').html(data.error).show();
							}
						);

					}

				}
			);

		},



		setAuthModalHeandler: function () {

			$('#login_btn').unbind().click(
				function (e) {

					e.preventDefault();

					// Проверяем поля
					if ($('#input_email').val() == '') {
						$('#email_field').addClass('error');
						$('#email_error_txt').show();
					} else {
						$('#email_field').removeClass('error');
						$('#email_error_txt').hide();
					}

					// Проверяем поля
					if ($('#input_pass').val() == '') {
						$('#password_field').addClass('error');
						$('#pass_error_txt').show();
					} else {
						$('#password_field').removeClass('error');
						$('#pass_error_txt').hide();
					}

					if ($('#input_email').val() != '') {

						// Формирование запроса к апи
						Api.send(
							// Контроллер
							'api/auth',
							// Акшн
							'native',
							// Данные
							{
								email: $('#input_email').val(),
								pass: $('#input_pass').val()
							},
							// Success
							function (data) {
								location.href = $('#w2d_redirect_uri').val();
							},
							// Error
							function (error) {
								if (error.error == 'not confirmed') {
									$('#auth_error_txt').html('Регистрация не была подтверждена.');
								} else {
									$('#auth_error_txt').html('Не верная пара значений логин - пароль.');
								}
								$('#auth_error_txt').show();
							}
						);

					}

				}
			);



            $('#signin_btn').unbind().click(
                function (e) {

                    e.preventDefault();
                    var err = false;

                    // Проверяем поля
                    if ($('#uname').val() == '') {
                        err = true;
                        $('#reg_uname_field').addClass('error');
                        $('#reg_uname_error_txt').show();
                    } else {
                        $('#reg_uname_field').removeClass('error');
                        $('#reg_uname_error_txt').hide();
                    }

                    // Проверяем поля
                    if ($('#uemail').val() == '' || $('#uemail').val().match(/\S+@\S+\.\S+/) == null) {
                        err = true;
                        $('#reg_email_field').addClass('error');
                        $('#reg_email_error_txt').show();
                    } else {
                        $('#reg_email_field').removeClass('error');
                        $('#reg_email_error_txt').hide();
                    }

                    // Проверяем поля
                    if ($('#pass1').val() == '') {
                        err = true;
                        $('#reg_password_field1').addClass('error');
                        $('#reg_pass_error_txt1').show();
                    } else {
                        $('#reg_password_field1').removeClass('error');
                        $('#reg_pass_error_txt1').hide();
                    }

                    if (err !== true && ($('#uemail').val() != '' && $('#pass1').val() != '')) {
                       location.href = location.origin+'/auth/signinative?uname='+$('#uname').val()+'&uemail='+$('#uemail').val()+'&pass1='+$('#pass1').val()+'&redirect_uri='+$('#w2d_redirect_uri').val()+'&submit=true';
                    }

                }
            );



            $('#already_reg').click( function () {
                $('#registration_modal').modal('hide');
            });

            $('#registration_link').click( function () {
                $('#myModal').modal('hide');
            });

		},



		setHeaderHeandlers: function () {

			// Клик на выпадающий список для добавления мест в шапке
			$('#add-place-btn').click(
				function () {

					if ($('#add-place-btn-block').hasClass('open')) {
						$('#add-place-btn-block').removeClass('open');
					} else {
						$('#add-place-btn-block').addClass('open');
						// Если открыта вторая вкладка зкрываем
						if ($('#add-travel-btn-block').hasClass('open')) {
							$('#add-travel-btn-block').removeClass('open');
						}
					}

					return false;

				}
			);

			// Клик на выпадающий список для добавления поездок в шапке
			$('#add-travel-btn').click(
				function () {
					if ($('#add-travel-btn-block').hasClass('open')) {
						$('#add-travel-btn-block').removeClass('open');
					} else {
						$('#add-travel-btn-block').addClass('open');
						// Если открыта вторая вкладка зкрываем
						if ($('#add-place-btn-block').hasClass('open')) {
							$('#add-place-btn-block').removeClass('open');
						}
					}

					return false;

				}
			);
		}

	}

})();