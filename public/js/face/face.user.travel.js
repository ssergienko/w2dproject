/*
 * Объект для интерфейса поездок пользователя
 *
 */
Face.user.travel = function ($) {

	return {


		/*
		 * Вешаем обработчики
		 *
		 **/
		init: function () {

			var _this = this;

			// Кнопка редактирования названия
			$('#travel-title-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения названия
			$('#travel-title-save-btn').click(
				function () {
					// Show samething
					_this.saveTitleField();
				}
			);

			// Кнопка редактирования описания
			$('#travel-descr-edit-btn').click(
				function () {
					// Show samething
					_this.doFieldEditable(this);
				}
			);

			// Кнопка сохранения описания
			$('#travel-descr-save-btn').click(
				function () {
					// Show samething
					_this.saveDescrField();
				}
			);

			this.hideAddPlaceToTravel();
			this.addPlaceToTravel();
		},


		/*
		 * Делает блок редактируемым
		 *
		 * @param block object
		 *
		 **/
		doFieldEditable: function (block) {

			if(block.id == 'travel-descr-edit-btn') {
				// Делаем редактируемым
				$('#travel-descr-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#travel-descr-field').css('border-style', 'dotted');
			} else if(block.id == 'travel-title-edit-btn') {
				// Делаем редактируемым
				$('#travel-title-field').attr('contentEditable','true');
				// Выделяем пунктиром
				$('#travel-title-field').css('border-style', 'dotted');
			}

		},



		/*
		 *  Сохраняет описание
		 **/
		saveDescrField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/places',
				// Акшн
				'savedescr',
				// Данные
				{
					id: $('#place_id').val(),
					descr: $('#place-descr-field').html()
				},
				// Success
				function (data) {

					// Делаем не редактируемым
					$('#place-descr-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#place-descr-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},



		/*
		 *  Сохраняет название
		 **/
		saveTitleField: function () {

			var _this = this;

			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/travels',
				// Акшн
				'savetitle',
				// Данные
				{
					id: $('#travel_id').val(),
					title: $('#travel-title-field').html()
				},
				// Success
				function (data) {
					console.log(data);

					// Делаем не редактируемым
					$('#travel-title-field').attr('contentEditable','false');

					// Выделяем не пунктиром
					$('#travel-title-field').css('border-style', 'none');

				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},

		hideAddPlaceToTravel: function(){
			$('.add-place-to-travel').click(function(){
				$('.add-place-to-travel-block').animate({height: 'toggle'});
			});
		},

		addPlaceToTravel: function(){
			$('input[name="add-place-to-travel-button"]').click(function(){
				// Проверяем наличие блока для добавления места в свои поездки
				if($(this).closest('.add-place-to-travel-block').is('div')){
					// Получаем ID пользователя
					var user_id = $(this).closest('.add-place-to-travel-block').find('input[name="user_id"]').val();

					if(user_id){

						// Выбираем данные для добавления
						var place = $(this).closest('.add-place-to-travel-block').find('select[name="p_id"]').val();
						var place_title = $(this).closest('.add-place-to-travel-block').find('select[name="p_id"] option:selected').html();
						var travel = $(this).closest('.add-place-to-travel-block').find('input[name="t_id"]').val();

						if(place == 0){
							window.location.href = '/user/'+user_id+'/travel/'+travel+'/places/add';
						}
						else{
							// Формирование запроса к апи
							Api.send(
								// Контроллер
								'api/travels',
								// Акшн
								'addplacetotravel',
								// Данные
								{
									ajax: 1,
									place: place,
									travel: travel
								},
								// Success
								function (data) {
									console.log(data);
									window.location.href = '/user/'+user_id+'/travel/'+travel+'/plan';

									/*$('.add-place-to-travel-block').animate({height: 'toggle'});
									var html = 'Добавлено место <a href="/user/'+user_id+'/travel/'+travel+'/place/'+place+'">'+place_title+'</a>';
									$('.right-block').append(html);

									$('.add-place-to-travel-success').toggle();

									setTimeout(function(){
										$('.add-place-to-travel-success').toggle();
									},1000);*/
								},
								// Error
								function (error) {
									console.log(error);
								}
							);
						}

					}
				}
			});
		}

	}

}(jQuery);