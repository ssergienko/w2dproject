/*
 * @info Класс отвечающий за добавление и редактирование места
 * @author sserg
 * @email sergey.s.sergienko@gmail.com
 *
 **/
Face.city.edit = function ($) {

	return {

		user_id: 0,
		city_id: 0,
		lat: 0,
		lng: 0,
		images_arr: new Array(),

		start: function (user_id, city_id) {

			var _this = this;

			'use strict';

			// Вешаем обработчики на начало и конец загрузки
			$('#fileupload')
			.bind('fileuploadstarted', function (e) {

				// Глушим стандартный обработчик
				$('#add_place_btn').unbind().click(
					function (e) {
						e.preventDefault();
					}
				);
			})
			.bind('fileuploadstopped', function (e) {
				// Обработчик на кнопку сохранения
				_this.mainSaveButton();
			});

			// Enable iframe cross-domain access via redirect option:
			$('#fileupload').fileupload(
				'option',
				'redirect',
				window.location.href.replace(
					/\/[^\/]*$/,
					'/cors/result.html?%s'
				)
			);

			this.user_id = user_id;
			this.city_id = city_id;

			// Кнопка загрузки картинок с компьютера
			this.setUploadBtnHeandler();
			// Вывод уже загруженных картинок
			this.showUploaded();

			// Автокомплит городов
			this.setHeandlerToCityForm();

			// Клики по карте
			this.mapHeandler();

			// Обработчик на изменение города
			this.cytyFormArrow();

			// Инициализируем sortable
			this.initSortable();

		},



		/*
		 * Инициализирует плагин для сортироваки
		 **/
		initSortable: function () {

			var _this = this;

			$("#sortable").sortable(
				{
					// Действие на отпускание блока мышкой
					beforeStop: function () {
						// Пересчитываем порядок блоков
						_this.resetIdx();
					}
				}
			);
		},



		/*
		 * Пересчитывает атрибт idx у блоков стекстом
		 **/
		resetIdx: function () {

			$('.image_txt').each(
				function (index) {
					$(this).attr('idx-field', index);
				}
			);

		},



		/*
		 * При изменении города, выводим его на карте
		 **/
		cytyFormArrow: function () {

			var _this = this;

            var $title = $('#title');
            $title.unbind().change(
				function () {

					if ($title.val() != '') {

						// Ищем точку по городу
						var geocoder = ymaps.geocode($title.val(), {results: 1});

						 geocoder.then(
							 function (res) {

								 // Если нашли город
								 if (res.geoObjects.get(0) != null) {

									 var point = res.geoObjects.get(0);
									// Центрирование карты на добавленном объекте
									w2dmap.setCenter(point.geometry.getCoordinates());
									// Выставляем координаты в формах
									_this.setFormValues(point.geometry.getCoordinates()[0], point.geometry.getCoordinates()[1]);

								 }

							 }
						 );
					}
				}
			);
		},



		/*
		 *Тут все для работы с картой
		 **/
		mapHeandler: function () {

			var _this = this;

			// Если есть координаты центруем карту
			if (w2dmap.lat == 0 || w2dmap.lng == 0) {

				// Ищем точку по городу
				var geocoder = ymaps.geocode($('#city_title').val(), {results: 1});

				/* После того, как поиск вернул результат, вызывается callback-функция */
				 geocoder.then(
					 function (res) {

						 // Если нашли город
						 if (res.geoObjects.get(0) != null) {

							 var point = res.geoObjects.get(0);
							// Центрирование карты на добавленном объекте
							w2dmap.setCenter(point.geometry.getCoordinates());
							// Выставляем координаты в формах
							_this.setFormValues(point.geometry.getCoordinates()[0], point.geometry.getCoordinates()[1]);

						 }

					 }
				 );

			}

			// Обработчик клика
			w2dmap.map.events.add("click", function(e) {
				// Выставляем координаты в формах
				_this.setFormValues(e.get("coordPosition")[0], e.get("coordPosition")[1]);
				// Центрирование карты на добавленном объекте
				w2dmap.setCenter(e.get("coordPosition"));
			});

		},


		/*
		* Вставляет данные в формы
		*/
		setFormValues: function (lat, lng) {
			// Заполняем формы
			$('#lat-coords').val(lat);
			// Заполняем формы
			$('#lng-coords').val(lng);
		},



		/*
		* Обработчик на изменение города
		*/
		setHeandlerToCityForm: function () {

			var _this = this;

			$("#city_title").autocomplete({
				url: "/api/places/getcity",
				remoteDataType: 'jsonp',
				sortResults: false,
				showResult: function(value, data) {
					return '<span style="color:black">' + value + '</span>';
				},
				onItemSelect: function(item) {

					setTimeout( function() {

						// Ищем точку по городу
						var geocoder = ymaps.geocode($('#city_title').val(), {results: 1});

						/* После того, как поиск вернул результат, вызывается callback-функция */
						geocoder.then(
							function (res) {

								// Если нашли город
								if (res.geoObjects.get(0) != null) {

									var point = res.geoObjects.get(0);
									// Центрирование карты на добавленном объекте
									w2dmap.setCenter(point.geometry.getCoordinates());
									// Выставляем координаты в формах
									_this.setFormValues(point.geometry.getCoordinates()[0], point.geometry.getCoordinates()[1]);

								 }

							 }
						 );

						// Фокус
						$('#description').focus();

					}, 500);
				}
			});

		},



		/*
		 * Задает главную картинку для вывода в списках
		 **/
		setMainImage: function () {

			var _this = this;

			$('input[name=main_picture]').change(
				function () {

					Api.send(
						// Контроллер
						'api/city',
						// Экшн
						'setmainimage',
						// Данные
						{
							city_id: _this.city_id,
							middle_url: $(this).val()
						},
						// Success
						function () {

						},
						// Error
						function (error) {console.log(error);}
					);

				}
			);

		},



		/*
		 * Ставит радиобаттон для главной картинки при выводе списка картинок
		 **/
		setMainImageRadio: function () {

			var _this = this;

			Api.send(
				// Контроллер
				'api/city',
				// Экшн
				'getmainimage',
				// Данные
				{
					city_id: _this.city_id
				},
				// Success
				function (data) {
					if (typeof(data.image_url) != 'undefined') {
						$('input[value="'+data.image_url+'"]').attr('checked',true).parent().addClass('current');
						$('#create_place_form').append('<input type="hidden" name="main_picture" value="'+data.image_url+'" />');
					}
				},
				// Error
				function (error) {console.log(error);}
			);

		},



		/*
		 * Показывает уже загруженные картинки
		 **/
		showUploaded: function () {

			var _this = this;

			// Срабатывает при открытии страницы
			$('#fileupload').each(function () {

				var that = this;

				$.getJSON(HOST+'storage/getcityimages',
				{
					user_id: _this.user_id,
                    city_id: _this.city_id
				},
				function (result) {

					if (result && result.length) {

						$(that).fileupload('option', 'done').call(that, null, {result: result});

						// Получаем главную картинку
						_this.setMainImageRadio();

						// Вешаем обработчик на выбор главной картинки
						_this.setMainImage();

						// Обработчик на кнопку сохранения
						_this.mainSaveButton();

					}
				});
			});

		},



		/*
		 * Если нажали на большую желтую кнопку сохранить
		 **/
		mainSaveButton: function () {

			var _this = this;

			$('#add_place_btn').unbind().click(
				function (e) {

					// Глушим стандартный обработчик
					e.preventDefault();

					var count_images = $('.image_txt').length;

					// Сохраняем тексты картинок
					_this.saveImageTexts();

					// Если еще не нажимали
					if (Face.listener.text_save_listener == '') {
						// Ждем пока все тексты картинок догрузятся, потом субмитим форму
						Face.listener.text_save_listener = setInterval(function() {
							if (_this.images_arr.length == count_images) {
								clearInterval(Face.listener.text_save_listener);
								$('#create_place_form').submit();
							}
						}, 300);
					}
				}
			);

		},



		/*
		 * Пробегает по всем картинкам и сохраняет описание для каждой
		 **/
		saveImageTexts: function () {

			var _this = this;

			_this.images_arr = new Array();

			$('.image_txt').each(
				function () {
					_this.saveImageText($(this).attr('data-field'), $(this).attr('idx-field'), $(this).val());
				}
			);

		},



		/*
		 * Сохраняет подпись для картинки
		 **/
		saveImageText: function (image_url, idx, image_txt) {

			var _this = this;

			// Сохраняем текст для картинки
			Api.send(
				// Контроллер
				'api/city',
				// Экшн
				'saveimagetxt',
				// Данные
				{
                    city_id: _this.city_id,
					image_url: image_url,
					image_txt: image_txt,
					idx: idx
				},
				// Success
				function (data) {
					if (data.success == 1) {
						// Сохраняем
						_this.images_arr.push(1);
					}
				},
				// Error
				function (error) {
					console.log(error);
				}
			);

		},



		/*
		 * Вешаем обработчик на клик на кнопку добавления картинок
		 *
		 **/
		setUploadBtnHeandler: function () {

			var _this = this;

			// Срабатывает при выборе картинок
			$('#fileupload').fileupload({
				add: function (e, data) {

					// Сразу загружаем выбранные картинки
					var jqXHR = data.submit()
						// В случае успеха пишем в таблицу place_images запись о том какой пользователь какую картинку добавил при создании места
						.success(function (result, textStatus, jqXHR) {

							if (typeof(result[0].files_url) != 'undefined') {

								Api.send(
									// Контроллер
									'api/city',
									// Экшн
									'addcityimage',
									// Данные
									{
                                        city_id: _this.city_id,
										name: result[0].name,
										url: result[0].files_url,
										middle_url: result[0].middle_url,
										thumbnail_url: result[0].thumbnail_url,
										delete_url: result[0].delete_url
									},
									// Success
									function (data) {},
									// Error
									function (error) {console.log(error);}
								);
							}

						})
						.complete(function (result, textStatus, jqXHR) {
							// Вешаем обработчик на выбор главной картинки
							_this.setMainImage();
						})
						.error(function (jqXHR, textStatus, errorThrown) {console.log(jqXHR, textStatus, errorThrown);});
				},
				destroyed: function (e, data) {

					Api.send(
						// Контроллер
						'api/storage',
						// Экшн
						'deletecity',
						// Данные
						{
                            city_id: _this.city_id,
							delete_url: data.url
						},
						// Success
						function (data) { if (data.success) { console.log(data); } },
						// Error
						function (error) {console.log(error);}
					);

				}

			});

		}

	}

}(jQuery);