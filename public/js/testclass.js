
/* 
 * Тестовый класс
 * Он может называться например myTravelsBlock
 * и использоваться для управления выводом интерактивного блока мои поездки
 *
 * @author sergey.s.sergienko@gmail.com
 * 
 * Вызывать так:
 * // Блок 
 * $(document).ready( function () {
 *     myTravelsBlock.test();
 * });
 * 
 * 
 */
myTravelsBlock = function ($) {
	
	return {
		// Свойство класса
		peremennaya: 'fffddggh35',
		
		// Метод класса
		test: function () {
		
			// Формируем данные и передаем их в тест контроллер
			var data = {
				peremennaya: this.peremennaya,
				itd: 'итд'
			};
		
			// Формирование запроса к апи
			Api.send(
				// Контроллер
				'api/test', 
				// Акшн
				'index',
				// Данные
				data,
				// Success
				function (response) {
					// Сюда попадаем если все успешно прошло
					// response - переменная в которую возвращается объект в виде json
					console.log(response);
				},
				// Error
				function (error) {
					// Сюда попадаем если ошибка
					console.log(error);
				}
			);
		
		}

	}
}(jQuery);

