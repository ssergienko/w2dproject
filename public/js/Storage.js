/**
 * Модуль для работы с удаленным файловым хранилищем
 *
 * @author mkechinov
 *
 *
 * @howto:
 * Необходимо вызывать функцию iWidget.storage.upload(form, callback);
 *
 * Где:
 * - form - DOM-объект формы, внутри которой находится поле файла (форма должна содержать только это поле)
 * - callback - функция-обработчик ответа, которая содержит только один параметр - URL полученной картинки
 *
 * Важно:
 * - форма должна иметь: enctype="multipart/form-data"
 * - форма должна иметь: method="post"
 *
 *
 */
Storage = function ($) {

	// iWidget.storage
	return {
	
		upload: function (form, params, callback) {

			// Устанавливаем action формы
			form.action = '/storage.php';

			// Генерируем уникальный идентификатор формы
			var id = 's' + Math.floor(Math.random() * 99999);
			form.setAttribute('target', id);

			// Формируем контейнер для фрейма
			var container = document.createElement('div');
			container.innerHTML = '<iframe style="display:none" src="about:blank" id="' + id + '" name="' + id + '" ></iframe>';
			
			// Удаляем из формы скрытые параметры
			$(form).children('[type=hidden]').remove();

			// Добавляем к форме параметры
			if(params) {
				for(var i in params) {
					if(params.hasOwnProperty(i)) {
						$(form).append('<input type="hidden" name="' + i + '" value="' + params[i] + '">');
					}
				}
			} 

			// Получаем фрейм
			var iframe = container.firstChild;

			/*
			 *  Когда сформировался iframe, получаем его контент,
			 *  из за защиты chrome до contentWindow не так просто дотянуться.
			 *  Контент получили, получаем содержимое body.
			 *  И вот уже содержимое body возвращаем в callback/
			 *  Это и есть наш урл нового файла. Или ошибка.
			 */

			$(iframe).load(
				function() {					

					var content = iframe.contentWindow ? iframe.contentWindow : iframe.contentDocument;
					var body = content.document.body;

					// если убрать это условие, в хроме не будет успевать отрабатывать php скрипт, и будет приходить пустая строка
					if (body.textContent || body.innerText) {
						callback(body.innerText ? body.innerText : body.textContent);
					}
					
				}
			);

			form.appendChild(container);			
			form.submit();
			
		}

	};

}(jQuery);