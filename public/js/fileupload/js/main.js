/*
 * jQuery File Upload Plugin JS Example 6.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, unparam: true, regexp: true */
/*global $, window, document */


$(function () {
	
	// Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
	
	
	
	$('#fileupload').fileupload({
		formData: {},
		dataType: 'json',
		error: function (jqXHR, textStatus, errorThrown) {
			console.log('error', jqXHR, textStatus, errorThrown);
		},
		success: function (data, textStatus, jqXHR) {
			if (typeof(data[0].url) != 'undefined') {
				
				// Вставляем урл на большой файл
				$('#create_place_form').append('<input type="hidden" name="files[]" value=\'{"url":"'+data[0].url+'","middle_url": "'+data[0].middle_url+'","thumbnail_url": "'+data[0].thumbnail_url+'"}\' />');
				
				// Показываем кнопку загрузить все
				$('#upload_all_images_btn').hide();
			}
		}
    }).bind('fileuploadadd', function (e, data) { 
		// Показываем кнопку загрузить все
		$('#upload_all_images_btn').show();
	});
	
	
		
	// Load existing files:
	/*$('#fileupload').each(function () {
		var that = this;
		$.getJSON(this.action, function (result) {			
			if (result && result.length) {
				$(that).fileupload('option', 'done').call(that, null, {result: result});
				// Вставляем урлы на файлы в общую форму
				$('#create_place_form').append('<input type="hidden" name="places[]" value="'+result[0].url+'" />');
				
			}			
		});
	});*/
	
});



/*
 {
		// Переопределяем действие на добавиление картинок
        add: function (e, data) {
			
			$('#fileupload').fileupload('process', {
				// An array of image files that are to be resized:
				files: data,
				process: [
					{
						action: 'load',
						fileTypes: /^image\/(gif|jpeg|png)$/,
						maxFileSize: 20000000 // 20MB
					},
					{
						action: 'resize',
						maxWidth: 40,
						maxHeight: 30,
						minWidth: 800,
						minHeight: 600
					},
					{action: 'save'},
					{action: 'duplicate'},					
					{
						action: 'resize',
						maxWidth: 269,
						maxHeight: 202,
						minWidth: 800,
						minHeight: 600
					},
					{action: 'save'},
					{action: 'duplicate'},
					{
						action: 'resize',
						maxWidth: 760,
						maxHeight: 570,
						minWidth: 800,
						minHeight: 600
					},
					{action: 'save'},
					{action: 'duplicate'},
				]
			}).done(function () {
				// Resized image files have been converted in place
				// and are available in the given files array
				data.submit();
			});
			
		}
		
    }
 **/