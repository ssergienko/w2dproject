<?
$page_name="Великий Новгород";
include ($_SERVER["DOCUMENT_ROOT"]."/inc/header.php"); 
?>
<link rel="stylesheet" href="css/jquery.fileupload-ui.css">
<link rel="stylesheet" href="http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css">
<div class="container main">
	<div class="page">
		<div class="row">
			<div class="span12">
				<ul class="breadcrumb">
					<li><a href="/">Куда поехать?</a></li>
					<li><a href="city.php">Великий Новгород</a></li>
					<li><a href="city.php">Места</a></li>
					<li>Добавить место</li>
				</ul>
				<h1>Добавление места</h1>
			</div>
		</div>
		
		<div class="row">
			<div class="span12 about_city">
			
				<div class="about_city">
					<div class="row">
						<div class="span12">
							
							<form id="fileupload" action="server/php/" method="POST" enctype="multipart/form-data">
							    <div class="row fileupload-buttonbar">
							        <div class="span7">
							            <span class="btn fileinput-button">
							                <i class="icon-picture icon-white"></i>
							                <span>Выбрать фотографии</span>
							                <input type="file" name="files[]" multiple>
							            </span>
							            <button type="submit" class="btn btn-primary start">
							                <i class="icon-upload-alt icon-white"></i>
							                <span>Загрузить фотографии</span>
							            </button>

							        </div>
							        <div class="span5 fileupload-progress fade dark">
							            <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
							                <div class="bar" style="width:0%;"></div>
							            </div>
							            <div class="progress-extended">&nbsp;</div>
							        </div>
							    </div>
							    <div class="fileupload-loading"></div>

							    <script type="text/javascript">
							    	$(document).bind('dragover', function (e) {
							    	    var dropZone = $('#dropzone'),
							    	        timeout = window.dropZoneTimeout;
							    	    if (!timeout) {
							    	        dropZone.addClass('in');
							    	    } else {
							    	        clearTimeout(timeout);
							    	    }
							    	    if (e.target === dropZone[0]) {
							    	        dropZone.addClass('hover');
							    	    } else {
							    	        dropZone.removeClass('hover');
							    	    }
							    	    window.dropZoneTimeout = setTimeout(function () {
							    	        window.dropZoneTimeout = null;
							    	        dropZone.removeClass('in hover');
							    	    }, 100);
							    	});
							    
							    </script>
							    
							    <div id="dropzone" class="fade"><i class="icon-picture icon-white"></i> Перетащите фотографии на это место</div>
							    <table role="presentation" class="table dark table-striped">
							    	<tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
							    
							    	</tbody>
							    </table>
							</form>
													
							<form class="form-horizontal" action="">
								<div class="control-group">
									<label class="control-label" for="input_1">Название места</label>
									<div class="controls">
										<input class="span5" type="text" id="input_1">
										<span class="help-block">Какой-нибудь поясняющий текст</span>
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label" for="input_1">Город</label>
									<div class="controls">										
										<input type="text" class="span5" data-provide="typeahead" data-items="5" data-source='["Москва","Санкт-Петербург","Великий Новогород","Нижний Новгород","Ярославль","Тверь","Владимир","Вологда","Великие Луки","Смоленск","Иваново"]'>
										<span class="help-block">Укажите город рядом с этим местом</span>
									</div>
								</div>
								
								<div class="control-group">
									<label class="control-label" for="inputEmail">Описание места</label>
									<div class="controls">
										<textarea class="span5"></textarea>
									</div>
								</div>
								<div class="control-group">
									<div class="controls">
										<button type="submit" class="btn btn-large btn-warning">Добавить место</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
				
		<div class="row">
			<div class="span12">
				<div id="modal-gallery" class="modal modal-gallery hide fade" data-filter=":odd">
				    <div class="modal-header">
				        <a class="close" data-dismiss="modal">&times;</a>
				        <h3 class="modal-title"></h3>
				    </div>
				    <div class="modal-body"><div class="modal-image"></div></div>
				    <div class="modal-footer">
				        <a class="btn modal-download" target="_blank">
				            <i class="icon-download"></i>
				            <span>Скачать</span>
				        </a>
				        <a class="btn btn-success modal-play modal-slideshow" data-slideshow="5000">
				            <i class="icon-play icon-white"></i>
				            <span>Слайдшоу</span>
				        </a>
				        <a class="btn btn-info modal-prev">
				            <i class="icon-arrow-left icon-white"></i>
				            <span>Предыдущая</span>
				        </a>
				        <a class="btn btn-primary modal-next">
				            <span>Следующая</span>
				            <i class="icon-arrow-right icon-white"></i>
				        </a>
				    </div>
				</div>
				<!-- The template to display files available for upload -->
				<script id="template-upload" type="text/x-tmpl">
				
				{% for (var i=0, file; file=o.files[i]; i++) { %}
					
		
				    <tr class="template-upload fade">
				        <td class="preview"><span class="fade"></span></td>
				        <td class="name"><span>{%=file.name%}</span></td>
				        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
				        {% if (file.error) { %}
				            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
				        {% } else if (o.files.valid && !i) { %}
				            <td>
				                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
				            </td>
				            <td class="start">{% if (!o.options.autoUpload) { %}
				                <button rel="tooltip" title="Загрузить" class="btn btn-primary">
				                    <i class="icon-upload-alt icon-white"></i>
				                </button>
				            {% } %}</td>
				        {% } else { %}
				            <td colspan="2"></td>
				        {% } %}
				        <td class="cancel">{% if (!i) { %}
				            <button class="btn">
				                <i class="icon-ban-circle icon-white"></i>
				            </button>
				        {% } %}</td>
				    </tr>
				{% } %}
				</script>

				<script id="template-download" type="text/x-tmpl">
				{% for (var i=0, file; file=o.files[i]; i++) { %}
				    <tr class="template-download fade">
				        {% if (file.error) { %}
				            <td></td>
				            <td class="name"><span>{%=file.name%}</span></td>
				            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
				            <td class="error" colspan="2"><span class="label label-important">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
				        {% } else { %}
				            <td class="preview">{% if (file.thumbnail_url) { %}
				                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
				            {% } %}</td>
				            <td class="name">
				                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
				            </td>
				            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
				            <td colspan="2"></td>
				        {% } %}
				        <td class="delete">
				            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}">
				                <i class="icon-trash icon-white"></i>
				              
				            </button>
				        
				        </td>
				    </tr>
				{% } %}
				</script>
				<script src="js/vendor/jquery.ui.widget.js"></script>
                <script src="/js/blueimp/tmpl.min.js"></script>
                <script src="/js/blueimp/load-image.min.js"></script>
                <script src="/js/blueimp/canvas-to-blob.min.js"></script>
                <script src="/js/bootstrap.min.js"></script>
                <script src="/js/blueimp/bootstrap-image-gallery.min.js"></script>
				<script src="/js/jquery.iframe-transport.js"></script>
				<script src="/js/jquery.fileupload.js"></script>
				<script src="/js/jquery.fileupload-fp.js"></script>
				<script src="/js/jquery.fileupload-ui.js"></script>
				<script src="/js/locale.js"></script>
				<!--[if gte IE 8]>
                <script src="/js/cors/jquery.xdr-transport.js"></script>
                <![endif]-->
			
			</div>
		</div>

	</div>
</div>

<? include ($_SERVER["DOCUMENT_ROOT"]."/inc/footer.php"); ?>