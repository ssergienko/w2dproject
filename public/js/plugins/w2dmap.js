
/* 
 * Тестовый класс
 * Он может называться например myTravelsBlock
 * и использоваться для управления выводом интерактивного блока мои поездки
 *
 * @author sergey.s.sergienko@gmail.com
 * 
 * Вызывать так:
 * // Блок 
 * $(document).ready( function () {
 *     myTravelsBlock.test();
 * });
 * 
 * 
 */
var w2dmap = function ($) {
	
	return {
		
		lat: 0,
		lng: 0,
		map: {},
		myPlacemark: {},
		balloonContent: '',
		
		/*
		* Инициализация яндекс карт
		* 
		**/
		initmap: function (lat, lng, map_zoom, map_type) {

			var _this = this;
		   
			// Если не пришли координаты, определяем местоположение
			if (lat == '' || lng == '') {
				lat = ymaps.geolocation.latitude;
				lng = ymaps.geolocation.longitude;
			}
		   
			_this.map = new ymaps.Map(
				"ymaps", {
					// Центр карты
					center: [lat, lng],
					// Коэффициент масштабирования
					zoom: map_zoom,
					// Тип карты
					type: map_type,
					behaviors: ["default", "scrollZoom"]
				}
			);

			// Элементы управления
			_this.map.controls
                .add(new ymaps.control.ZoomControl())
                .add(new ymaps.control.MapTools())
                .add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));

			_this.lat = lat;
			_this.lng = lng;

			// Показываем метку
			_this.showMarker(lat, lng);

	   },
	   
	   
	   
	   /*
		* Центрует карту по имени города и отрисовывает места вокруг
		**/
	   setAroundByCityName: function (city_name, callback) {

			var _this = this;

			setTimeout( function() {
				// Ищем точку по городу
				var geocoder = ymaps.geocode(city_name, {results: 1});
				/* После того, как поиск вернул результат, вызывается callback-функция */
				geocoder.then(
					function (res) {
						// Если нашли город
						if (res.geoObjects.get(0) != null) {
							var point = res.geoObjects.get(0);

							// Удаляем все маркеры
							_this.removeAllMarkers();

							// Центрирование карты на добавленном объекте
							_this.setCenter(point.geometry.getCoordinates());

							// Выполняем переданный метод
							callback();
						}
					 }
				 );

			}, 500);
		},
		
		
		
		/*
		* Центрует карту по координатам и отрисовывает места вокруг
		**/
		setAroundByCoords: function (coords, callback) {
		
			var _this = this;
			
			_this.removeAllMarkers();
			
			_this.setCenter(coords);
			
			callback();
		
		},
	   
	   
	   
		/*
		 * Очищает карту от маркеров
		 **/
		removeAllMarkers: function () {
			var _this = this;
			// Найдём на карте геообъект по его id
			_this.map.geoObjects.each(function (geoObject) {
				_this.map.geoObjects.remove(geoObject);
			});
		},
	   
	   
	   
		showMarker: function (lat, lng) {
		   
		   var _this = this;
		   
		   // Создание метки 
			_this.myPlacemark = new ymaps.Placemark(
				// Координаты метки
				[lat, lng], {
					/* Свойства метки:
					   - контент значка метки */
					iconContent: "",
					// - контент балуна метки
					balloonContent: _this.balloonContent
				}, {
					/* Опции метки:
					   - флаг перетаскивания метки */
					draggable: false,
					/* - показывать значок метки при открытии балуна */
					hideIconOnBalloonOpen: true
				}
			);

			// Задание стиля коллекции после ее создания
			_this.myPlacemark.options.set("preset", 'twirl#houseIcon');

			// Добавление метки на карту
			_this.map.geoObjects.add(_this.myPlacemark);
	   },
	   
	   
	   
	   setCenter: function (coords) {
		   
			var _this = this;
			
			_this.map.panTo(
				// Координаты нового центра карты
				[coords[0], coords[1]], {
					/* Опции перемещения:
					   разрешить уменьшать и затем увеличивать зум
					   карты при перемещении между точками 
					*/
					flying: true
				}
			);

			// Удаляем маркер
			_this.map.geoObjects.remove(_this.myPlacemark);

			// Ставим маркер
			_this.showMarker(coords[0], coords[1]);
			
			_this.lat = coords[0];
			_this.lng = coords[1];

	   }
	   
	   

	}
}(jQuery);

